package us.johnchambers.podcast.activity.main

import android.app.*
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.CountDownTimer
import android.os.PersistableBundle
import android.util.AttributeSet
import android.util.Log
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.navigation.NavigationView
import androidx.core.view.GravityCompat
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import org.greenrobot.eventbus.EventBus
import us.johnchambers.podcast.database.DBHelper
import org.greenrobot.eventbus.Subscribe;
import us.johnchambers.podcast.R
import us.johnchambers.podcast.activity.player.PlayerActivity
import java.io.Serializable
import android.widget.RadioButton
import com.crashlytics.android.Crashlytics
import com.google.android.material.switchmaterial.SwitchMaterial
import com.google.firebase.analytics.FirebaseAnalytics
import io.fabric.sdk.android.Fabric
import kotlinx.android.synthetic.main.podcast_option_color.view.*
import kotlinx.android.synthetic.main.podcast_option_playback_limit.view.*
import org.jetbrains.anko.*
import us.johnchambers.podcast.event.*
import us.johnchambers.podcast.misc.*
import java.util.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    lateinit var _fragMgr: FragMgr
    lateinit var tapGuard: TapGuard
    private lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(null) //       savedInstanceState)
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)
        Fabric.with(this, Crashlytics());

        //*** setup DB ***
        DBHelper.init(applicationContext)
        //initTheme()
        setTheme(ColorHelper.getCurrentTheme())
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        ColorHelper.setAppBackground(drawer_layout, this)

        val toggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )

        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
        _fragMgr = FragMgr(this)

        tapGuard = TapGuard(GC.tapGuard.shortTime)
        EventBus.getDefault().register(this)

        if (!updaterIsRunning()) {
            startUpdater()
        }

        doAsync {
            for (tag in GC.tags.tagStrings) {
                if (!DBHelper.tagExists(tag)) {
                    DBHelper.insertTag(tag)
                }
            }
        }
    }

    override fun onCreateView(name: String, context: Context, attrs: AttributeSet): View? {
        return super.onCreateView(name, context, attrs)
    }

    override fun onResume() {
        //GC.update.service.shuttingDown = false
        super.onResume()
    }

    override fun onStop() {
        //GC.update.service.shuttingDown = true
        super.onStop()
    }

    override fun onDestroy() {
        //GC.update.service.shuttingDown = true
        finishAffinity()
        finishAndRemoveTask()
        super.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle?, outPersistentState: PersistableBundle?) {
        //super.onSaveInstanceState(outState, outPersistentState)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            _fragMgr.closeTopFragment()
            //super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_playback_limit -> {
                setPlaybackLimit()
                return true
            }
            R.id.action_wifi -> {
                setWifi()
                return true
            }
            R.id.action_color -> {
                setColor()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        if (tapGuard.tooSoon()) return true
        when (item.itemId) {
            R.id.nav_search -> {
                _fragMgr.openSearchFragment()
            }
            R.id.nav_my_podcasts -> {
                _fragMgr.openMyPodcastsFragment()
            }
            R.id.nav_tags -> {
                EventBus.getDefault().post(EventOpenTagFragment())
            }
            R.id.nav_what_is_new -> {
                EventBus.getDefault().post(EventOpenTagDetailFragment(GC.tags.WHATS_NEW_TEXT))
            }
            R.id.nav_manual_queue_1 -> {
                openManualQueueMenu()
            }

            R.id.nav_update_podcasts -> {
                if (updaterIsRunning()) {
                    longToast("Podcast updater is already running, count to 300 and try again.")
                } else {
                    longToast("Updating podcasts, this may take a few minutes.")
                    startUpdater()
                }
            }
            R.id.nav_about -> {
                openAboutDialog()
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    //*******************************************
    //* event receivers
    //*******************************************

    @Subscribe
    fun onEvent(event: EventToast) {
        longToast(event.message)
    }

    @Subscribe
    fun onEvent(event: EventSnackbar) {
        Snackbar.make(drawer_layout, event.message, event.length())
    }

    @Subscribe
    fun onEvent(event: EventCloseTopFragment) {
        this.onBackPressed()
    }

    @Subscribe
    fun onEvent(event: EventOpenPlayer) {
        if (badWifi()) return
        //Log.d("~~~~~" + System.currentTimeMillis().toString(), "Main Activity Starting event player: episode: " + event.docket.nextEpisode)
        val playerIntent = Intent(this, PlayerActivity::class.java)
        playerIntent.putExtra("docket", event.docket as Serializable)
        startActivity(playerIntent)
    }

    @Subscribe
    fun onEvent(event: EventRemovePodcast) {
        _fragMgr.closePodcastDetailsFragment()
        doAsync {
            DBHelper.removeEntirePodcast(event.pid)
            uiThread {
                EventBus.getDefault().post(EventToast("Podcast has been removed"))
                EventBus.getDefault().post(EventPodcastHasBeenDeleted(event.pid))
            }
        }
    }

    //******************************************************
    //* options menu calls
    //******************************************************

    fun setPlaybackLimit() {
        val builder = AlertDialog.Builder(this)
        val inflater = this.layoutInflater
        var v = inflater.inflate(R.layout.podcast_option_playback_limit, null)
        var limit = DBHelper.getGlobalOptionPlaybackLimit()

        for (i in 0..(v.radio_playback_limit.childCount - 1)) {
            var button = v.radio_playback_limit.getChildAt(i) as RadioButton
            var tag = button.getTag()
            if (tag.toString().equals(limit.toString())) button.isChecked = true
        }

        builder.setView(v)
            .setPositiveButton("done",
                DialogInterface.OnClickListener { dialog, id -> var x = 1
                    var checked = v.radio_playback_limit.checkedRadioButtonId
                    var r = v.find<RadioButton>(checked)
                    var newLimit = r.getTag().toString()
                    var oh = OptionsHelper()
                    oh.setPlaybackLimit(newLimit)
                })
            .setNegativeButton("cancel",
                DialogInterface.OnClickListener { dialog, id ->
                    //getDialog()
                })
        builder.create()
        builder.show()
    }

    fun setColor() {
        val builder = AlertDialog.Builder(this)
        val inflater = this.layoutInflater
        var v = inflater.inflate(R.layout.podcast_option_color, null)
        //var limit = DBHelper.getGlobalOptionPlaybackLimit()

        var currThemeName = ColorHelper.getThemeName()
        for (i in 0..(v.radio_color.childCount - 1)) {
            var button = v.radio_color.getChildAt(i) as RadioButton
            var tag = button.getTag()
            if (tag.toString().equals(currThemeName)) button.isChecked = true
        }

        builder.setView(v)
            .setPositiveButton("done",
                DialogInterface.OnClickListener { dialog, id -> var x = 1
                    var checked = v.radio_color.checkedRadioButtonId
                    var r = v.find<RadioButton>(checked)
                    var newColor = r.getTag().toString()
                    ColorHelper.updateColor(newColor)
                })
            .setNegativeButton("cancel",
                DialogInterface.OnClickListener { dialog, id ->
                    //getDialog()
                })
        builder.create()
        builder.show()
    }

    private fun openAboutDialog() {
        val builder = AlertDialog.Builder(this)
        val inflater = this.layoutInflater
        var v = inflater.inflate(R.layout.about_layout, null)

        builder.setView(v)
            .setPositiveButton("done", DialogInterface.OnClickListener { dialog, id -> var x = 1 })
        builder.create()
        builder.show()
    }

    private fun setWifi() {
        val builder = AlertDialog.Builder(this)
        val inflater = this.layoutInflater
        var v = inflater.inflate(R.layout.podcast_option_wifi, null)
        while (v == null) {
            pauseOneSecond()
        }
        var oh = OptionsHelper()
        var button = v.findViewById(R.id.option_wifi_switch) as SwitchMaterial
        button.setChecked(oh.getWifiOnly())
        builder.setView(v)
            .setPositiveButton("done",
                DialogInterface.OnClickListener { dialog, id -> var x = 1
                    var button = v.findViewById(R.id.option_wifi_switch) as SwitchMaterial
                    var oh = OptionsHelper()
                    oh.setWifiOnly(button.isChecked)
                })
        builder.create()
        builder.show()
    }

    private fun openManualQueueMenu() {

        val array = arrayOf("One","Two","Three","Four","Five","Six","Seven","Eight","Nine","Ten")
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Choose a manual queue to open.")
        builder.setItems(array,{_, which ->
            val selected = array[which]
            when(which) {
                0 -> _fragMgr.openManualQueueFragment(GC.playlist.manual.MANUAL_QUEUE_1)
                1 -> _fragMgr.openManualQueueFragment(GC.playlist.manual.MANUAL_QUEUE_2)
                2 -> _fragMgr.openManualQueueFragment(GC.playlist.manual.MANUAL_QUEUE_3)
                3 -> _fragMgr.openManualQueueFragment(GC.playlist.manual.MANUAL_QUEUE_4)
                4 -> _fragMgr.openManualQueueFragment(GC.playlist.manual.MANUAL_QUEUE_5)
                5 -> _fragMgr.openManualQueueFragment(GC.playlist.manual.MANUAL_QUEUE_6)
                6 -> _fragMgr.openManualQueueFragment(GC.playlist.manual.MANUAL_QUEUE_7)
                7 -> _fragMgr.openManualQueueFragment(GC.playlist.manual.MANUAL_QUEUE_8)
                8 -> _fragMgr.openManualQueueFragment(GC.playlist.manual.MANUAL_QUEUE_9)
                9 -> _fragMgr.openManualQueueFragment(GC.playlist.manual.MANUAL_QUEUE_10)
                else -> _fragMgr.openManualQueueFragment(GC.playlist.manual.MANUAL_QUEUE_1)
            }
        })
        val dialog = builder.create()
        dialog.show()
    }
    //*******************************************************************
    //* end of menu section
    //*******************************************************************

    private fun pauseOneSecond() {
        val timer = object: CountDownTimer(100, 100) {
            override fun onTick(millisUntilFinished: Long) {}
            override fun onFinish() {}
        }
        timer.start()
    }

    private fun badWifi() : Boolean {
        var oh = OptionsHelper()
        if (!oh.getWifiOnly()) return false

        var connManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        var connections = connManager.allNetworks

        for (connection in connections) {
            var info = connManager.getNetworkInfo(connection)

            if ((info.type == ConnectivityManager.TYPE_WIFI) && (info.isConnected)) {
                return false
            }
        }
        alert("You have decided to only play while on Wi-Fi. Tap the tripple-dot menu in the corner to change Wi-Fi settings.",
            "Wi-Fi not found") {
            positiveButton("OK") {}
            negativeButton("") {}
        }.show()

        return true
    }

    private fun setUpdateAlarm() {

        var cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 5) //GC.update.service.UPDATE_HOUR);
        cal.set(Calendar.MINUTE, 11) //GC.update.service.UPDATE_MINUTE);

        var intent = Intent(this, us.johnchambers.podcast.updater.PodcastUpdateBroadcastReceiver::class.java)

        var pendingIntent = PendingIntent.getBroadcast(this, 0,
            intent, PendingIntent.FLAG_CANCEL_CURRENT);

        var alarmManager = getSystemService(ALARM_SERVICE) as AlarmManager
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
            cal.getTimeInMillis(),
            (24*60*60*1000), //GC.update.service.UPDATE_INTERVAL,
            pendingIntent);
    }

    private fun startUpdater() {
        if (updaterIsRunning()) {return}
        var intent = Intent(this, us.johnchambers.podcast.updater.PodcastUpdateBroadcastReceiver::class.java)

        var pendingIntent = PendingIntent.getBroadcast(this, 0,
            intent, PendingIntent.FLAG_CANCEL_CURRENT)

        pendingIntent.send()
    }

    private fun updaterIsRunning():Boolean {

        var am = getSystemService(ACTIVITY_SERVICE) as ActivityManager
        for (svc in am.getRunningServices(Integer.MAX_VALUE))
        {
            if (svc.service.getClassName().toString().equals("us.johnchambers.podcast.updater.PodcastUpdateService")) {
                return true
            }
        }
        return false
    }

}
