package us.johnchambers.podcast.activity.player

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.KeyEvent
import android.view.KeyEvent.KEYCODE_MEDIA_NEXT
import android.view.KeyEvent.KEYCODE_MEDIA_PREVIOUS
import android.view.View
import android.view.WindowManager
import us.johnchambers.podcast.R
import kotlinx.android.synthetic.main.activity_player.*
import kotlinx.android.synthetic.main.exo_playback_control_view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.jetbrains.anko.alert
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.toast
import us.johnchambers.podcast.event.EventEpisodeLimitReached
import us.johnchambers.podcast.event.EventPlayerActivityClosed
import us.johnchambers.podcast.event.EventToast
import us.johnchambers.podcast.misc.OptionsHelper
import us.johnchambers.podcast.objects.docket.Docket
import us.johnchambers.podcast.objects.docket.DocketEmpty

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
class PlayerActivity : AppCompatActivity() {
    private val mHideHandler = Handler()

    private val mHidePart2Runnable = Runnable {
    }

    private val mShowPart2Runnable = Runnable {
        supportActionBar?.show()
    }
    private var mVisible: Boolean = false
    private val mHideRunnable = Runnable { hide() }
    private val mDelayHideTouchListener = View.OnTouchListener { _, _ ->
        if (AUTO_HIDE) {
            delayedHide(AUTO_HIDE_DELAY_MILLIS)
        }
        false
    }

    //** my variables ************************
    lateinit var docket: Docket
    //****************************************

    override fun onCreate(savedInstanceState: Bundle?) {
        try {
            //Log.d("~~~~~" + System.currentTimeMillis().toString(), "Activity: recycled = " + recycled.toString())
        } catch (e: Exception) {
            //Log.d("~~~~~" + System.currentTimeMillis().toString(), "Activity: recycled error = " + e.message)
        }
        savedInstanceState?.putBoolean("recycled", true)
        var intent = this.intent
        try {
            docket = intent.extras["docket"] as Docket

        } catch(e: Exception) {
            toast("Error creating playlist")
            docket = DocketEmpty()
            onBackPressed()
        }

        setContentView(R.layout.activity_player)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        mVisible = true
        toggle() //turn off action bar

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        super.onCreate(savedInstanceState)
    } //** end of onCreate **

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        setupOnClicks()
        PlayerController.attachService(this.applicationContext, player_player_view)
        PlayerController.processPlaylist(docket)
        EventBus.getDefault().register(this)
        var oh = OptionsHelper()
    }



    override fun onPause() {
        super.onPause()
    }

    //*********************************
    //* Termination routines
    //*********************************
    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun onStop() {
        //PlayerController.cleanup()
        super.onStop()
    }

    override fun onDestroy() {
        EventBus.getDefault().post(EventPlayerActivityClosed())
        PlayerController.cleanup()
        finishAffinity()
        finishAndRemoveTask()
        super.onDestroy()
    }
    //*************************************
    //*************************************

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KEYCODE_MEDIA_NEXT) {
            PlayerController.skipFoward()
        }

        if (keyCode == KEYCODE_MEDIA_PREVIOUS) {
            PlayerController.skipBackwards()
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onKeyLongPress(keyCode: Int, event: KeyEvent?): Boolean {
        return super.onKeyLongPress(keyCode, event)
    }

    private fun toggle() {
        if (mVisible) {
            hide()
        } else {
            show()
        }
    }

    private fun hide() {
        // Hide UI first
        supportActionBar?.hide()
        //fullscreen_content_controls.visibility = View.GONE
        mVisible = false

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable)
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY.toLong())
    }

    private fun show() {
        mVisible = true

        // Schedule a runnable to display UI elements after a delay
        mHideHandler.removeCallbacks(mHidePart2Runnable)
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY.toLong())
    }

    /**
     * Schedules a call to hide() in [delayMillis], canceling any
     * previously scheduled calls.
     */
    private fun delayedHide(delayMillis: Int) {
        mHideHandler.removeCallbacks(mHideRunnable)
        mHideHandler.postDelayed(mHideRunnable, delayMillis.toLong())
    }

    companion object {
        /**
         * Whether or not the system UI should be auto-hidden after
         * [AUTO_HIDE_DELAY_MILLIS] milliseconds.
         */
        private val AUTO_HIDE = true

        /**
         * If [AUTO_HIDE] is set, the number of milliseconds to wait after
         * user interaction before hiding the system UI.
         */
        private val AUTO_HIDE_DELAY_MILLIS = 3000

        /**
         * Some older devices needs a small delay between UI widget updates
         * and a change of the status and navigation bar.
         */
        private val UI_ANIMATION_DELAY = 300
    }

    fun setupOnClicks() {

        controls_x_out_button.onClick {
            onBackPressed()
        }

        exo_prev.onClick {
            PlayerController.gotoBeginningOfEpisode()
        }

        exo_next_custom.onClick {
            PlayerController.gotoEndOfEpisode()
        }

        player_time_bar.onClick {
        }
    }

    //*****************************
    //* events
    //*****************************

    @Subscribe
    fun onEvent(event: EventEpisodeLimitReached) {
        alert("Do you want to keep playing?") {
            positiveButton("Yes") {
                PlayerController.continuePlaying()
            }
            negativeButton("No") {
                //close activity
            }
        }.show()
    }
}
