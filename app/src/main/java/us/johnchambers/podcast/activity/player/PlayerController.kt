package us.johnchambers.podcast.activity.player

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import com.google.android.exoplayer2.ui.PlayerView
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import us.johnchambers.podcast.event.*
import us.johnchambers.podcast.misc.OptionsHelper
import us.johnchambers.podcast.objects.docket.Docket
import us.johnchambers.podcast.playlist.Playlist
import us.johnchambers.podcast.playlist.PlaylistFactory

object PlayerController {

    var service: PlayerService? = null
    var serviceBound = false
    lateinit var serviceIntent: Intent
    lateinit var playlist: Playlist
    private var episodesLeft = 0

    init {
        EventBus.getDefault().register(this)
        resetEpisodesLeft()
    }

    private fun resetEpisodesLeft() {
        var oh = OptionsHelper()
        episodesLeft = oh.getPlaybackLimit()
    }

    public fun attachService(context: Context, playerView: PlayerView) {
        if (service == null) {
            //Log.d("~~~~~" + System.currentTimeMillis().toString(),"PlayerController: service was null")
            initService(context, playerView)
        }
        else {
            //Log.d("~~~~~" + System.currentTimeMillis().toString(),"PlayerController: service existed")
            attachPlayerToView(context, playerView)
        }
    }

    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(componentName: ComponentName, iBinder: IBinder) {
            val myBinder = iBinder as PlayerService.MyBinder
            service = myBinder.getService()
            serviceBound = true
        }
        override fun onServiceDisconnected(componentName: ComponentName) {
            serviceBound = false
        }
    }

    private fun initService(context: Context, playerView: PlayerView) {
        //Log.d("~~~~~" + System.currentTimeMillis().toString(),"PlayerController: binding service")
        serviceIntent =  Intent(context, PlayerService::class.java)
        context.bindService(serviceIntent, serviceConnection, Context.BIND_AUTO_CREATE)

        doAsync {
            while (!serviceBound) {
                Thread.sleep(1000)
            }
            uiThread {
                finishInitOfService(context, playerView)
            }
        }
    }

    private fun finishInitOfService(context: Context, playerView: PlayerView) {
        service?.configureService()
        attachPlayerToView(context, playerView)
    }

    private fun attachPlayerToView(context: Context, view: PlayerView) {
        service?.attachPlayerToView(context, view)
    }

    //****************************************************
    //* playing section
    //****************************************************
    fun processPlaylist(docket: Docket) {
        var oh = OptionsHelper()
        episodesLeft = oh.getPlaybackLimit()

        playlist = PlaylistFactory.getPlaylist(docket)

        doAsync {
            while (!serviceBound) {
                Thread.sleep(1000)
            }
            uiThread {
                doAsync {
                    var playerStarted = service?.playerReady()
                    while (!playerStarted!!) {
                        Thread.sleep(1000)
                        playerStarted = service?.playerReady()
                    }
                    uiThread {
                        playNextEpisode()
                    }
                }
            }
        }
    }

    fun continuePlaying() {
        var oh = OptionsHelper()
        episodesLeft = oh.getPlaybackLimit()
        playNextEpisode()
    }

    //plays the next episode in playlist
    private fun playNextEpisode() {
        if (playlist.next()) {
            playlist.setNowPlaying()
           var eid = playlist.getCurrEpisodeEid()
            var episodeToPlay = playlist.getCurrEpisodeUrl()
            var playPoint = playlist.getPlayPointAsLong()
            var speed: Float = playlist.getSpeed()
            service?.play(episodeToPlay, playlist.getLogoUrl(), playPoint, speed, playlist.getEpisodeName(), playlist.getPodcastName(), eid)
        } else {
            playlist.clearNowPlaying()
            service?.playCompleteMessage()
        }
    }

    //*********************************************
    //* player controls actions
    //*********************************************

    fun gotoBeginningOfEpisode() {
        service?.gotoBeginningOfEpisode()
    }

    fun gotoEndOfEpisode() {
        service?.gotoEndOfEpisode()
    }

    fun skipFoward() {
        service?.skipForward()
    }

    fun skipBackwards() {
        service?.skipBackward()
    }

    fun cleanup() {
        service?.cleanup()
    }

    //************************************
    //* EventBus Subscriptions
    //************************************
    @Subscribe
    fun onEvent(event: EventEpisodeFinished) {
        if (--episodesLeft <= 0) {
            EventBus.getDefault().post(EventEpisodeLimitReached())
        } else {
            playNextEpisode()
        }
    }

    @Subscribe
    fun onEvent(event: EventUpdateTime) {
        playlist.updateEpisodeTime(event.playPoint, event.length, event.eid)
     }

    @Subscribe
    fun onEvent(event: EventResetEpisodesLeft) {
        resetEpisodesLeft()
    }

}