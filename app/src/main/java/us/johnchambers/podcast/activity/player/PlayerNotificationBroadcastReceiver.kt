package us.johnchambers.podcast.activity.player

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import org.greenrobot.eventbus.EventBus
import us.johnchambers.podcast.event.*
import us.johnchambers.podcast.misc.GC

class PlayerNotificationBroadcastReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {

        val action = intent.action
        //EventBus.getDefault().post("### " + action.toString())

        if (action == GC.player.REWIND) {
            EventBus.getDefault().post(EventPlayerRewind())
        }

        if (action == GC.player.PLAY_PAUSE) {
            EventBus.getDefault().post(EventPlayerPlayPause())
        }
        if (action == GC.player.FORWARD) {
            EventBus.getDefault().post(EventPlayerForward())
        }
    }
}
