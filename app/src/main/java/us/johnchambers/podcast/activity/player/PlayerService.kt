package us.johnchambers.podcast.activity.player

import android.app.*
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Binder
import android.os.IBinder
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import org.greenrobot.eventbus.EventBus
import us.johnchambers.podcast.R
import android.widget.RemoteViews
import android.os.Build
import android.os.CountDownTimer
import android.support.v4.media.session.MediaSessionCompat
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.google.android.exoplayer2.*
import kotlinx.android.synthetic.main.exo_playback_control_view.view.*
import org.greenrobot.eventbus.Subscribe
import us.johnchambers.podcast.event.*
import us.johnchambers.podcast.misc.GC
import us.johnchambers.podcast.misc.OptionsHelper
import com.google.android.exoplayer2.ext.mediasession.MediaSessionConnector
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import us.johnchambers.podcast.misc.GraphicsHelper
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import us.johnchambers.podcast.database.DBHelper

class PlayerService : Service(), Player.EventListener {

    private val binder = MyBinder()
    private lateinit var trackSelector: DefaultTrackSelector
    private lateinit var loadControl:  DefaultLoadControl
    private lateinit var renderersFactory: DefaultRenderersFactory
    private lateinit var player: ExoPlayer
    private lateinit var context: Context
    private val optionsHelper =  OptionsHelper()
    private lateinit var view: PlayerView
    private var playingLastEpisodeNotice = false
    private val serviceId = 59247
    lateinit var mediaSession: MediaSessionCompat
    lateinit var mediaSessionConnector: MediaSessionConnector
    lateinit var customView: RemoteViews
    var errorCount = 0
    var errorMax = 5

    lateinit var phoneStateListener : PhoneStateListener
    lateinit var telephoneManager : TelephonyManager
    var playerPlayingStateForPhone = true
    var currentEpisodeEid: String = ""

    override fun onBind(intent: Intent): IBinder {
        //Log.d("~~~~~" + System.currentTimeMillis().toString(), epoch.toString() + "-PlayerService: binding")
        return binder
    }

    override fun onCreate() {
        setPhoneListener();
        super.onCreate()
    }

    override fun onDestroy() {
        try {
            EventBus.getDefault().unregister(this)
            stopForeground(true)
            val nm = applicationContext.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            nm.cancel(serviceId)
            nm.cancelAll()
            stopSelf()
        } catch (e: Exception) {

        }
        super.onDestroy()
    }

    //***********************
    //* methods
    //***********************

    fun getEpisodeTime() : Map<String, Long>{
        var m = mutableMapOf("position" to -1.toLong() , "duration" to 0.toLong())
        //EventBus.getDefault().post(EventToast(m.get("position").toString() + ":" + m.get("duration").toString()))
        if ((player.duration > 0) and (player.contentPosition > 0)) {
            m.set("position", player.contentPosition)
            m.set("duration",player.duration)
        }
        return m
    }

    fun stop() {
        try {
            player.playWhenReady = false
        } catch(e: Exception) {}
    }

    fun restart() {
        try {
            player.playWhenReady = true
        } catch(e: Exception) {}
    }

    fun playerReady() : Boolean {
        try {
            player.playWhenReady = false
            return true
        } catch(e: java.lang.Exception) {
            return false
        }
    }

    fun configureService() {
        //Log.d("~~~~~" + System.currentTimeMillis().toString(), epoch.toString() + "-PlayerService: initing player")
        makeForegroundService()
        EventBus.getDefault().register(this)
    }

    fun attachPlayerToView(context: Context, view: PlayerView) {
        this.view = view
        this.context = context
        trackSelector = DefaultTrackSelector()
        loadControl = DefaultLoadControl()
        renderersFactory = DefaultRenderersFactory(context)

        player = ExoPlayerFactory.newSimpleInstance(context,
            renderersFactory, trackSelector, loadControl)

        try {
            player.removeListener(this)
        } catch (e: Exception) {
            //do nothing as no listener was attached
        }
        player.addListener(this)

        this.view.player = player
        setMediaReceiver()
    }

    fun prepareAndPlay(url: String, logoUrl: String, playPoint: Long, speed: Float = 1.0f, title: String = "", podcastName: String = "") {
        player.playWhenReady = false

        var mediaSource = ExtractorMediaSource(Uri.parse(url),
            createDataSourceFactory(),
            DefaultExtractorsFactory(),
            null,
            null);

        player.prepare(mediaSource)

        //todo add correct notification (show playing or not playing)
        if (title.equals("")) {
            this.view.player_control_episode_title.visibility = View.GONE
        } else {
            this.view.player_control_episode_title.visibility = View.VISIBLE
            this.view.player_control_episode_title.text = title
        }

        var pbParms = PlaybackParameters(speed, 1.0f)
        //todo fix parameters
        //player.playbackParameters = pbParms
        player.setPlaybackParameters(pbParms)

        view.setFastForwardIncrementMs(optionsHelper.getFastForwardMilliSeconds(""))
        view.setRewindIncrementMs(optionsHelper.getRewindMilliSeconds(""))

        setLogo(logoUrl, title, podcastName)
        if (playPoint > 0) player.seekTo(playPoint)
        player.playWhenReady = true

    }

    private fun createDataSourceFactory() : DefaultDataSourceFactory {
        var userAgent = Util.getUserAgent(context, "jcpodcast-exoplayer-agent");

        // Default parameters, except allowCrossProtocolRedirects is true
        var httpDataSourceFactory = DefaultHttpDataSourceFactory(
            userAgent,
            null,
            DefaultHttpDataSource.DEFAULT_CONNECT_TIMEOUT_MILLIS,
            DefaultHttpDataSource.DEFAULT_READ_TIMEOUT_MILLIS,
            true /* allowCrossProtocolRedirects */
        );

        var dataSourceFactory = DefaultDataSourceFactory(
            context,
            null,
            httpDataSourceFactory
        );

        return dataSourceFactory;
    }

    fun play(url: String, logoUrl: String, playPoint: Long, speed: Float = 1.0f,
             title: String = "", podcastName: String = "", eid: String = "") {
        currentEpisodeEid = eid
        errorCount = 0
        playingLastEpisodeNotice = false
        prepareAndPlay(url, logoUrl, playPoint, speed, title, podcastName)
    }

    fun playCompleteMessage() {
        currentEpisodeEid = ""
        playingLastEpisodeNotice = true
        view.defaultArtwork = getDrawable(R.drawable.the_end)
        //Log.d("~~~~~" + System.currentTimeMillis().toString(), epoch.toString() + "-PlayerService: playCompleteMessage calling prepareAndPlay")
        prepareAndPlay("file:///android_asset/ZZINTERNAL_playlist_complete.mp3",
            "",
            -1,
            1.0.toFloat(),
            "The End", "Playlist Complete")
    }

    private fun setLogo(logoUrl: String, title: String, podcastName: String) {
        try { //in case logo bitmap is too large
            var graphicHelper = GraphicsHelper()
            if (graphicHelper.isBadImageLink(logoUrl)) {
                var art = graphicHelper.getBigPodcastImage(podcastName)
                view.defaultArtwork = BitmapDrawable(resources, art)
            } else {
                Glide.with(context)
                    .asBitmap()
                    .error(R.drawable.unable_to_get_image)
                    .load(logoUrl)
                    .thumbnail(0.3f)
                    .listener(object : RequestListener<Bitmap> {
                        override fun onLoadFailed(
                            e: GlideException?,
                            model: Any?,
                            target: Target<Bitmap>?,
                            isFirstResource: Boolean
                        ): Boolean {
                            System.out.println("here's your exception")
                            view.defaultArtwork = BitmapDrawable(resources, graphicHelper.getBigPodcastImage(podcastName))
                            return true
                        }

                        override fun onResourceReady(
                            resource: Bitmap?,
                            model: Any?,
                            target: Target<Bitmap>?,
                            dataSource: DataSource?,
                            isFirstResource: Boolean
                        ): Boolean {
                            System.out.println("here's your bitmap")
                            view.defaultArtwork = BitmapDrawable(resources, resource)
                            return true
                        }
                    })
                    .preload()
            }
        } catch (e: Exception) {
            view.defaultArtwork = resources.getDrawable(R.drawable.unable_to_get_image, null)
        }
    }

    //********************************************
    //* Notification
    //********************************************
    private fun makeForegroundService() {
        val theNo = getPlayerNotification(
            "",
            "",
            ""
        )
        startForeground(serviceId, theNo)
    }

    private fun eliminateNotification() {
        try {
            val nm = applicationContext.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            nm.cancel(serviceId)
            nm.cancelAll()}
        catch(e:Exception) {}
    }

    private fun updateNotification() {
        val theNo = getPlayerNotification(
            "",
            "",
            ""
        )
        var nm = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        nm.notify(serviceId, theNo);
    }

    private fun getPlayerNotification(
        button2: String,
        title: String,
        contextText: String
    ): Notification {

        // create channel
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                "us.johnchambers.player.notification",
                "Diffcast",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            channel.description = "A Different Podcast App"
            channel.enableLights(false)
            channel.enableVibration(false)
            val notificationManager = getSystemService(NotificationManager::class.java)
            notificationManager.createNotificationChannel(channel)
        }

        val notif: Notification.Builder
        notif = Notification.Builder(applicationContext)
        notif.setSmallIcon(R.mipmap.ic_launcher)
        notif.setContentTitle(title)
        notif.setContentText(contextText)
        notif.setAutoCancel(true)
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notif.setChannelId("us.johnchambers.player.notification")
        }

        customView = RemoteViews(packageName, R.layout.controls_notification)

        notif.setContent(customView)

        //******** rewind button
        customView.setImageViewResource(R.id.notification_rewind_button, R.drawable.ic_rewind_dark)
        val yesReceiveRewind = Intent(this, PlayerNotificationBroadcastReceiver::class.java)
        yesReceiveRewind.action = GC.player.REWIND
        val pendingIntentYesRewind = PendingIntent.getBroadcast(this, 12345, yesReceiveRewind, PendingIntent.FLAG_UPDATE_CURRENT)
        customView.setOnClickPendingIntent(R.id.notification_rewind_button, pendingIntentYesRewind)

        //********* play pause button **************
        try {
            if (player.playWhenReady) {
                customView.setImageViewResource(R.id.notification_play_pause_button, R.drawable.ic_pause_dark)
            } else {
                customView.setImageViewResource(R.id.notification_play_pause_button, R.drawable.ic_play_dark)
            }
        } catch (e: java.lang.Exception) {
            customView.setImageViewResource(R.id.notification_play_pause_button, R.drawable.ic_pause_dark)
        }
        val yesReceivePlayPause = Intent(this, PlayerNotificationBroadcastReceiver::class.java)
        yesReceivePlayPause.action = GC.player.PLAY_PAUSE
        val pendingIntentYesPlayPause = PendingIntent.getBroadcast(this, 12345, yesReceivePlayPause, PendingIntent.FLAG_UPDATE_CURRENT)
        customView.setOnClickPendingIntent(R.id.notification_play_pause_button, pendingIntentYesPlayPause)

        //************ forward button *********************
        customView.setImageViewResource(R.id.notification_forward_button, R.drawable.ic_forward_dark)
        val yesReceiveForward = Intent(this, PlayerNotificationBroadcastReceiver::class.java)
        yesReceiveForward.action = GC.player.FORWARD
        val pendingIntentYesForward = PendingIntent.getBroadcast(this, 12345,yesReceiveForward, PendingIntent.FLAG_UPDATE_CURRENT)
        customView.setOnClickPendingIntent(R.id.notification_forward_button, pendingIntentYesForward)

        return notif.build()

    }
    //***************************************************
    //* end of notification
    //***************************************************

    //***************************************
    //* classes
    //***************************************
    inner class MyBinder : Binder() {
        fun getService() : PlayerService {
            return this@PlayerService
        }
    }

    //******************************************
    //* EventBus subscriptions
    //******************************************
    @Subscribe
    fun onEvent(event: EventPlayerRewind) {
        player.seekTo(player.getContentPosition() - optionsHelper.getRewindMilliSeconds(""));
    }

    @Subscribe
    fun onEvent(event: EventPlayerPause) {
    }

    @Subscribe
    fun onEvent(event: EventPlayerPlay) {
    }

    @Subscribe
    fun onEvent(event: EventPlayerPlayPause) {
         player.playWhenReady = !(player.playWhenReady == true)

        //set notification view

    }

    @Subscribe
    fun onEvent(event: EventPlayerForward) {
        player.seekTo(player.getContentPosition() + optionsHelper.getFastForwardMilliSeconds(""));
    }

    @Subscribe
    fun onEvent(event: EventStopPlayerInService) {
        this.stop()
    }

    //*****************************************
    //* Player.EventListener overrides
    //****************************************

    override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {

        if (player.videoComponent == null) {
            return
        }
        updateNotification()

        //todo update video time in database
        if (!playingLastEpisodeNotice) {
            if ((currentEpisodeEid != "") and (player.duration > 0) and (player.contentPosition > 0)) {
                DBHelper.updateTimerOnEpisodeRow(player.contentPosition, player.duration, currentEpisodeEid)
                EventBus.getDefault().post(EventUpdateTime(player.contentPosition, player.duration, currentEpisodeEid))
                EventBus.getDefault().post(EventUpdateRowInDisplayByEid(currentEpisodeEid))
            }
        }

        if (!playingLastEpisodeNotice) {
            if (player.duration > 0) {
                if (playbackState == Player.STATE_ENDED &&
                    playWhenReady == true &&
                    (player.contentPosition >= player.duration)
                ) {
                    player.playWhenReady = false
                    EventBus.getDefault().post(EventEpisodeFinished())
                }
            }
        }
    }

    override fun onPlayerError(error: ExoPlaybackException) {
        if (++errorCount > errorMax) {
            this.stop()
            this.cleanup()
            view.defaultArtwork = resources.getDrawable(R.drawable.unable_to_get_image, null)
            play("file:///android_asset/problem_playing_the_episode_with_tone.mp3", "", -1)
        } else {
            this.stop()
            EventBus.getDefault().post(EventToast(error.message.toString(), true))
            pauseOneSecond()
            this.restart()
        }
    }

    private fun pauseOneSecond() {
        val timer = object: CountDownTimer(100, 100) {
            override fun onTick(millisUntilFinished: Long) {}
            override fun onFinish() {}
        }
        timer.start()
    }

    //overridden to solve echo problem when quick seeking
    override fun onSeekProcessed() {
        this.stop()
        player.playWhenReady = true
        //todo should this be overridden?
        //super.onSeekProcessed()
    }

    fun cleanup() {
        player.stop()
        player.release()
        try {EventBus.getDefault().unregister(this)} catch(e:Exception) {}
        try {stopForeground(true)} catch(e:Exception) {}
        eliminateNotification()
        try {stopSelf()} catch(e:Exception) {}
        this.stop()
    }

    //**********************************
    //* player manipulation
    //**********************************

    fun gotoBeginningOfEpisode() {
        player.seekTo(0)
    }

    fun gotoEndOfEpisode() {
        player.seekTo(player.duration)
    }

    fun skipForward() {
        player.seekTo(player.getContentPosition() + optionsHelper.getFastForwardMilliSeconds(""));
    }

    fun skipBackward() {
        player.seekTo(player.getContentPosition() - optionsHelper.getRewindMilliSeconds(""));
    }

    fun setMediaReceiver() {
        val mediaButtonReceiver = ComponentName(context, packageName) //RemoteControlReceiver::class.java)

        mediaSession = MediaSessionCompat(context, "jc.podcast.player",
            mediaButtonReceiver, null)

        mediaSession.setCallback(object: MediaSessionCompat.Callback() {
            override fun onMediaButtonEvent(mediaButtonEvent: Intent) : Boolean {
                EventBus.getDefault().post("@@@ " + mediaButtonEvent.action.toString())
                return super.onMediaButtonEvent(mediaButtonEvent);
            }
        })

        mediaSessionConnector = MediaSessionConnector(mediaSession)
        //mediaSessionConnector.setPlayer(player, null)
        mediaSessionConnector.setPlayer(player)


        mediaSession.setActive(true);
    }

    private fun setPhoneListener() {

        phoneStateListener = object: PhoneStateListener() {

            override fun onCallStateChanged(state: Int, phoneNumber: String?) {
                super.onCallStateChanged(state, phoneNumber)
            }
        }
        telephoneManager =  getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        telephoneManager.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE)
    }

}
