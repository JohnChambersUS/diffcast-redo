package us.johnchambers.podcast.database

import android.content.Context
import androidx.room.Room
import org.greenrobot.eventbus.EventBus
import us.johnchambers.podcast.database.objects.SearchPodcastTag
import us.johnchambers.podcast.database.table_definitions.*
import us.johnchambers.podcast.event.EventTagListHasBeenUpdated
import us.johnchambers.podcast.event.EventToast
import us.johnchambers.podcast.misc.GC

object DBHelper {

    lateinit var _db: PodcastDatabase

    fun init(context: Context) {
        _db = Room.databaseBuilder(
            context,
            PodcastDatabase::class.java, "podcast-db")
            .allowMainThreadQueries()
            .fallbackToDestructiveMigration()
            .build()
    }

    fun getCurrentTheme() : String {
        var rv = _db.dao().getGlobalOption(GC.theme.dbKey)
        if (rv != null) {
            return rv.v
        }
        else {
            var newRow = OptionsGlobalRow()
            newRow.k = GC.theme.dbKey
            newRow.v = GC.theme.forest.light.name
            _db.dao().insertGlobalOptionRow(newRow)
            return newRow.v
        }
    }

    fun podcastExists(pid: String) : Boolean {
        var count = _db.dao().doesPodcastExist(pid)
        return (count > 0)
    }

    fun episdeExists(eid: String) : Boolean {
        var count = _db.dao().doesEpisodeExist(eid)
        return (count > 0)
    }

    fun getPodcastRowByPid(pid: String) : PodcastRow{
        var list = _db.dao().getPodcastRowByPid(pid)
        if (list.size == 0) return PodcastRow()
        return list.get(0)
    }

    fun getPodcasts() : MutableList<PodcastRow> {
        return _db.dao().getPodcasts()
    }

    fun insertPodcastRow(row: PodcastRow) : Boolean {
        try {
            _db.dao().insertPodcastRow(row)
            //add podcast to what's new if type if podcast
            if (row.mode.equals(GC.playbackModes.PODCAST)) {
                var ptr = PodcastTagRow()
                ptr.pid = row.pid
                ptr.tag = GC.tags.WHATS_NEW_TEXT
                addPodcastTagRow(ptr)
            }
            return true
        } catch (e: Exception) {
            EventBus.getDefault().post(EventToast("Unable to add podcast: " + e.message))
            return false
        }
    }

    fun updatePodcastRow(row: PodcastRow) : Boolean {
        try {
            _db.dao().updatePodcastRow(row)
            return true
        } catch (e: Exception) {
            EventBus.getDefault().post(EventToast("Unable to update podcast: " + e.message))
            return false
        }
    }


    fun insertEpisodeRow(row: EpisodeRow) : Boolean {
        try {
            _db.dao().insertEpisodeRow(row)
            return true
        } catch (e: Exception) {
            return false
        }
    }

    fun getEpisodeRowsByPid(pid: String) : MutableList<EpisodeRow> {
        var list = _db.dao().getEpisodeRowsByPid(pid)
        return list
    }

    fun getEpisodeRowByEpisodeId(eid: String) : EpisodeRow {
        var row = _db.dao().getEpisodeRowByEpisodeId(eid)
        if (row == null) {
            return EpisodeRow()
        } else {
            return row
        }
    }

    fun updateTimerOnEpisodeRow(playPoint: Long, duration: Long, episodeId: String) {
        var pp = playPoint.toString()
        var len = duration.toString()
        _db.dao().updateTimesOnEpisode(pp, len, episodeId)
    }

    fun removeEntirePodcast(pid: String) {
        try {
            //remove manual playlist rows
            var rows = _db.dao().getAllPlaylistRows()
            for (row in rows) {
                var er = _db.dao().getEpisodeRowByEpisodeId(row.eid)
                if (pid.equals(er.pid)) {
                    _db.dao().removePlaylistRow(row)
                }
            }
            _db.dao().deleteAllEpisodeRowsByPid(pid)
            _db.dao().deletePodcastRowByPid(pid)
            _db.dao().removeFromNowPlaying(pid)
            _db.dao().untagPodcast(pid)
        } catch (e: Exception){
            org.greenrobot.eventbus.EventBus.getDefault().post(EventToast("Failure removing podcast: " + e.message))
        }
    }

    fun getSpeedForPodcastAsString(pid: String) : String {
        var row = _db.dao().getPodcastSpeed(pid)
        if (row == null) {
            return "global"
        } else {
            return row.value.toString()
        }
    }

    fun setSpeedForPodcastFromString(pid: String, speed: String) {
        var row = OptionsPodcastRow()
        row.pid = pid
        row.key = "speed"
        row.value = speed
        _db.dao().insertOptionsPodcastRow(row)
    }

    fun getGlobalOptionPlaybackLimit() : String {
        var limit =  _db.dao().getGlobalOptionPlaybackLimit()
        if (limit == null) {
            return "10"
        }
        else {
            return limit.v
        }
    }

    fun upsertGlobalOptionsRow(row: OptionsGlobalRow) {
        _db.dao().insertGlobalOptionRow(row)
    }

    fun getGlobalOptionsRow(key: String) : OptionsGlobalRow {
        return _db.dao().getGlobalOption(key)
    }

    fun setNowPlaying(playlistName: String, eid: String) {
        clearNowPlaying()
        var np = NowPlayingRow()
        np.name = playlistName
        np.eid = eid
        try {
            _db.dao().insertNowPlayingRow(np)
        } catch (e:Exception) {
            //Log.d("~~~~~", e.message)
        }

    }

    fun clearNowPlaying() {
        _db.dao().clearNowPlaying()
    }

    fun getNowPlayingRow() : NowPlayingRow {
        var row = _db.dao().getNowPlayingRow()
        if (row == null) return NowPlayingRow()
        if (row.isEmpty()) return NowPlayingRow()
        return row
    }

    fun deleteEpisodeByRow(row: EpisodeRow) {
        _db.dao().deleteEpisodeByRow(row)
    }

    fun insertTag(tag: String) {
        var row = TagRow()
        row.tag = tag
        _db.dao().insertTagRow(row)
    }

    fun deleteTag(tag: String) {
        for (currTag in GC.tags.tagStrings) {
            if (currTag.equals(tag)) {
                EventBus.getDefault().post(EventToast("Cannot remove " + tag))
                return
            }
        }
        var row = TagRow()
        row.tag = tag
        _db.dao().deleteTagRow(row)

    }

    fun getAllTags() : MutableList<String> {
        return _db.dao().getAllTags()
    }

    fun removeTag(tag: String) {
        //remove from now playing
        _db.dao().removeFromNowPlaying(tag)
        //remove from playlist
        _db.dao().removeFromPlaylists(tag)
        //remove from tag table
        _db.dao().removeFromTags(tag)
        //event bus notify that tag table has changed
        EventBus.getDefault().post(EventTagListHasBeenUpdated())
    }

    fun getPodcastsWithTag(tag: String) : MutableList<PodcastRow> {
        return _db.dao().getPodcastsWithTag(GC.playbackModes.PODCAST)
    }

    fun getPodcastsWithTag2(tag: String) : MutableList<SearchPodcastTag> {
        return _db.dao().getPodcastsWithTag2(GC.playbackModes.PODCAST, tag)
    }

    fun addPodcastTagRow(ptr: PodcastTagRow) {
        _db.dao().insertPodcastTagRow(ptr)
    }

    fun removePodcastTagRow(ptr: PodcastTagRow) {
            _db.dao().deletePodcastTagRow(ptr)
    }

    fun getPodcastEpisodesByTag(tag: String) : MutableList<EpisodeRow> {
        return _db.dao().getPodcastEpisodesByTag(GC.playbackModes.PODCAST, tag)
    }

    fun getTopPodcastEpisodesByTag(tag: String) : MutableList<EpisodeRow> {
        return _db.dao().getTopPodcastEpisodesByTag(GC.playbackModes.PODCAST, tag)
    }

    fun tagExists(tag: String) : Boolean {
        var count = _db.dao().doesTagExist(tag)
        return (count > 0)
    }

    fun getPlaylistByName(name: String) : MutableList<PlaylistRow> {
        return _db.dao().getPlaylistByName(name)
    }

    fun addManualPlaylistRow(row: PlaylistRow) {
        addManualPlaylistRow(row.eid, row.name)
    }

    fun addManualPlaylistRow(eid: String, queueName: String) {
        if (_db.dao().doesPlaylistItemExist(queueName, eid) > 0) return
        var row = PlaylistRow()
        row.eid = eid
        row.name = queueName
        _db.dao().insertPlaylistRow(row)
    }

    fun removePlaylistRow(row: PlaylistRow) {
        _db.dao().removePlaylistRow(row)
    }

    fun clearManualPlaylist(queueName: String) {
        _db.dao().clearPlaylistByName(queueName)
    }

}