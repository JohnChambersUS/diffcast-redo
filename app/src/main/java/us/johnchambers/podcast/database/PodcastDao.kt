package us.johnchambers.podcast.database

import androidx.room.*
import androidx.room.OnConflictStrategy.*
import us.johnchambers.podcast.database.objects.SearchPodcastTag
import us.johnchambers.podcast.database.table_definitions.*

@Dao
interface PodcastDao {


    //*******************************
    //* options global
    //*******************************

    @Query("select * from options_global limit 1")
    fun getCurrentTheme() : OptionsGlobalRow

    @Query("select * from options_global where k = :key limit 1")
    fun getGlobalOption(key: String) : OptionsGlobalRow

    @Query("select * from options_global where k = 'playback_limit' limit 1")
    fun getGlobalOptionPlaybackLimit() : OptionsGlobalRow

    @Insert(onConflict = REPLACE)
    fun insertGlobalOptionRow(row: OptionsGlobalRow)

    //***********************************
    //* podcast options
    //***********************************

    @Query("select * from options_podcast where pid = :pid and key = 'speed' limit 1")
    fun getPodcastSpeed(pid: String) : OptionsPodcastRow

    @Insert(onConflict = REPLACE)
    fun insertOptionsPodcastRow(row: OptionsPodcastRow)


    //******************************************
    //* podcast row
    //******************************************
    @Query("select count(*) from podcasts where pid = :pid")
    fun doesPodcastExist(pid: String) : Int

    @Query("select * from podcasts where pid = :pid")
    fun getPodcastRowByPid(pid: String) : List<PodcastRow>

    @Insert(onConflict = REPLACE)
    fun insertPodcastRow(row: PodcastRow)

    @Update
    fun updatePodcastRow(row: PodcastRow)

    @Query("select * from podcasts order by sort_name")
    fun getPodcasts() : MutableList<PodcastRow>

    @Query("select * from podcasts where mode = :mode order by sort_name")
    fun getPodcastsWithTag(mode: String) : MutableList<PodcastRow>


    @Query("delete from podcasts where pid = :pid")
    fun deletePodcastRowByPid(pid: String)

    @Query("select podcasts.pid, name, sort_name, logo_url, tag from podcasts LEFT join (select * from podcast_tag where tag = :tag) p2 on podcasts.pid = p2.pid where mode = :mode order by sort_name;")
    fun getPodcastsWithTag2(mode: String, tag: String) : MutableList<SearchPodcastTag>

    //*******************************
    //* episode row
    //*******************************
    @Insert(onConflict = REPLACE)
    fun insertEpisodeRow(row: EpisodeRow)

    @Query("select * from episodes where pid = :pid order by publication_date desc, id desc")
    fun getEpisodeRowsByPid(pid: String) : MutableList<EpisodeRow>

    @Query("select * from episodes where pid = :pid")
    fun getEpisodeRowsByPidUnsorted(pid: String) : MutableList<EpisodeRow>

    @Query("update episodes set play_point = :playPoint, length = :length where eid = :eid")
    fun updateTimesOnEpisode(playPoint: String, length: String, eid: String)

    @Query("select * from episodes where eid = :eid limit 1")
    fun getEpisodeRowByEpisodeId(eid: String) : EpisodeRow

    @Query("select count(*) from episodes where eid = :eid")
    fun doesEpisodeExist(eid: String) : Int

    @Query("delete from episodes where pid = :pid")
    fun deleteAllEpisodeRowsByPid(pid: String)

    @Delete
    fun deleteEpisodeByRow(row: EpisodeRow)

    @Query("select * from episodes e where e.pid in (select pid from podcast_tag where tag = :tag) and e.pid in (select pid from podcasts where mode = :mode) order by e.publication_date DESC LIMIT 99")
    fun getPodcastEpisodesByTag(mode: String, tag: String) : MutableList<EpisodeRow>

    @Query("select * from episodes e where e.pid in (select pid from podcast_tag where tag = :tag) and e.pid in (select pid from podcasts where mode = :mode) group by pid having max(publication_date) order by e.publication_date DESC")
    fun getTopPodcastEpisodesByTag(mode: String, tag: String) : MutableList<EpisodeRow>


    //****************************************
    //* Now Playing Table
    //****************************************
    @Query("delete from now_playing")
    fun clearNowPlaying()

    @Insert(onConflict = REPLACE)
    fun insertNowPlayingRow(row: NowPlayingRow)

    @Query("select * from now_playing limit 1")
    fun getNowPlayingRow() : NowPlayingRow

    @Query("delete from now_playing where name = :pid")
    fun removeFromNowPlaying(pid: String)

    //*****************************************
    //* tag table
    //*****************************************
    @Insert(onConflict = REPLACE)
    fun insertTagRow(row: TagRow)

    @Delete
    fun deleteTagRow(row: TagRow)

    @Query("delete from tags where tag = :tag")
    fun removeFromTags(tag: String)

    @Query("Select tag from tags order by tag ASC")
    fun getAllTags() : MutableList<String>

    @Query("Select count(*) from tags where tag = :tag")
    fun doesTagExist(tag: String) : Int


    //*********************************************
    //* playlist table
    //*********************************************
    @Query("delete from playlists where name = :pid")
    fun removeFromPlaylists(pid: String)

    //****************************************************
    //* podcast tag table
    //****************************************************
    @Insert(onConflict = IGNORE)
    fun insertPodcastTagRow(row: PodcastTagRow)

    @Delete
    fun deletePodcastTagRow(row: PodcastTagRow)

    @Query("delete from podcast_tag where pid = :pid")
    fun untagPodcast(pid: String)

    //***************************************************
    //* playlist table
    //***************************************************
    @Query("select * from playlists where name = :name order by id asc")
    fun getPlaylistByName(name: String) : MutableList<PlaylistRow>

    @Query("select * from playlists")
    fun getAllPlaylistRows() : MutableList<PlaylistRow>

    @Insert(onConflict = ABORT)
    fun insertPlaylistRow(row: PlaylistRow)

    @Delete
    fun removePlaylistRow(row: PlaylistRow)

    @Query("delete from playlists where name = :name")
    fun clearPlaylistByName(name: String)

    @Query("select count(*) from playlists where name = :name and eid = :eid")
    fun doesPlaylistItemExist(name: String, eid: String) : Int


}