package us.johnchambers.podcast.database

import androidx.room.Database
import androidx.room.RoomDatabase
import us.johnchambers.podcast.database.table_definitions.*

@Database(entities = arrayOf(PodcastRow::class,
    EpisodeRow::class,
    NowPlayingRow::class,
    PodcastTagRow::class,
    TagRow::class,
    PlaylistRow::class,
    OptionsGlobalRow::class,
    OptionsPodcastRow::class),
    version = 205, exportSchema = true)
abstract class PodcastDatabase : RoomDatabase() {
    abstract fun dao() : PodcastDao
}