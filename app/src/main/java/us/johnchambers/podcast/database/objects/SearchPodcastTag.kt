package us.johnchambers.podcast.database.objects

class SearchPodcastTag (
    var pid: String,
    var name: String,
    var sort_name: String,
    var logo_url: String,
    var tag: String? = "dummy"
){
}