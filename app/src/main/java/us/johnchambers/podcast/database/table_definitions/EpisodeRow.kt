package us.johnchambers.podcast.database.table_definitions

import android.util.Log
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import org.greenrobot.eventbus.EventBus
import us.johnchambers.podcast.event.EventToast
import java.io.Serializable
import java.lang.Exception

@Entity(tableName = "episodes",
    indices = arrayOf(Index("eid", unique = true), Index("pid", unique = false)) //,
    //foreignKeys = [ForeignKey(entity = PodcastRow::class,
    //                            parentColumns = ["pid"],
    //                            childColumns = ["pid"],
    //                            onDelete = ForeignKey.CASCADE)])
) //added after removed above
data class EpisodeRow(
    @PrimaryKey(autoGenerate = true) var id: Int?,
    var eid: String, //episode id
    var pid: String, //podcast id
    var title: String,
    var summary: String,
    var audio_url: String,
    var publication_date: String, //date field holding as string
    var length: String, //long holding as string
    var play_point: String, //long holding as string
    var downloaded_url: String, //path to episode if downloaded
    var skip: Int //boolean holding as int
) : Serializable {

    constructor():this(null,"","","","","",
        "","0","-1",  "", 0)

    fun isEmpty(): Boolean {
        if (eid == "")
            return true
        else
            return false
    }

    fun isSkipped() : Boolean {
        return (skip == 1)
    }

    fun skipEpisode() {
        skip = 1
    }

    fun unskipEpisode() {
        skip = 0
    }

    fun setToBeginning() {
        setPlayPointFromLong((-1).toLong())
        setLengthFromLong((0).toLong())
    }

    fun markAsPlayed() {
        setPlayPointFromLong(1000.toLong())
        setLengthFromLong(1.toLong())
    }

    fun isComplete() : Boolean {
        var pp = getPlayPointAsLong()
        var len = getLengthAsLong()
        if ((pp == 0.toLong()) && (len == 0.toLong())) return false
        return (getPlayPointAsLong() >= getLengthAsLong())
    }

    fun isInProgress() : Boolean {
        return (hasStarted() && (isComplete() == false))
    }
    
    fun isInProgressEligable() : Boolean {
        return (isNotSkipped() && isInProgress())
    }

    fun hasStarted() : Boolean {
        return (getPlayPointAsLong() > (0).toLong())
    }

    fun touched() : Boolean {
        return (hasStarted() || isComplete())
    }

    fun touchedPlayable() : Boolean {
        return (touched() && isNotSkipped())
    }

    fun isNotSkipped() : Boolean {
        return !isSkipped()
    }
    
    fun isPlayable() : Boolean {
        return (isNotSkipped() and (isComplete() == false))
    }

    fun getPlayPointAsLong() : Long {
        if (play_point.equals("")) return -1
        try {
            var rv = play_point.toLong()
            return rv
        } catch(e: Exception) {
            return -1
        }
    }

    fun setPlayPointFromLong(value: Long) {
        try {
            var longValue = value.toString()
            play_point = longValue
        } catch(e: Exception) {
            play_point = "-1"
        }
    }

    fun setLengthFromLong(value: Long) {
        try {
            var longValue = value.toString()
            length = longValue
        } catch(e: Exception) {
            length = "0"
        }
    }

    fun getLengthAsLong() : Long {
        if (length.equals("")) return 0
        try {
            return length.toLong()
        } catch(e: Exception) {
            return 0
        }
    }

}