package us.johnchambers.podcast.database.table_definitions

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "now_playing",
    indices = arrayOf(
        Index("eid", unique = false),Index("name", unique = false)) //,
    //foreignKeys = arrayOf(
    //    ForeignKey(entity = EpisodeRow::class,
    //        parentColumns = ["eid"],
    //        childColumns = ["eid"],
    //        onDelete = ForeignKey.CASCADE)
    //)
)
data class NowPlayingRow(
    @PrimaryKey(autoGenerate = true) var id: Int?,
    var name: String,
    var eid: String
) {
    constructor():this(null,"","")

    fun isEmpty() : Boolean {
        return (name.equals(""))
    }
}