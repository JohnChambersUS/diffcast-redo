package us.johnchambers.podcast.database.table_definitions

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "options_global")
data class OptionsGlobalRow (
    @PrimaryKey var k: String,
    var v: String
){
    constructor():this("", "")

}