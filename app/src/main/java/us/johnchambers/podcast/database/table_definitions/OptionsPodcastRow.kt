package us.johnchambers.podcast.database.table_definitions

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index

@Entity(tableName = "options_podcast",
    primaryKeys = arrayOf("pid", "key"),
    indices = arrayOf(Index("pid", unique = false)) //,
    //foreignKeys = arrayOf(
    //    ForeignKey(entity = PodcastRow::class,
    //        parentColumns = ["pid"],
    //        childColumns = ["pid"],
    //        onDelete = ForeignKey.CASCADE)
    //)
)
data class OptionsPodcastRow(
    var pid: String,
    var key: String,
    var value: String
) {
    constructor():this("","","")
}