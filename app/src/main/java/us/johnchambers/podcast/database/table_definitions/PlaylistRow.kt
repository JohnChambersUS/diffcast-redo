package us.johnchambers.podcast.database.table_definitions

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "playlists",
    indices = arrayOf(
        Index("eid", unique = false),Index("name", unique = false)) //,
    //foreignKeys = arrayOf(
    //    ForeignKey(entity = EpisodeRow::class,
    //        parentColumns = ["eid"],
    //        childColumns = ["eid"])
    //)
)
data class PlaylistRow(
    @PrimaryKey(autoGenerate = true) var id: Int?,
    var name: String,
    var eid: String,
    var now_playing: Int //bool holding as int
) {
    constructor():this(null,"","",0)
}