package us.johnchambers.podcast.database.table_definitions

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "podcasts",
    indices = arrayOf(Index("sort_name", unique = false)))
data class PodcastRow(
    @PrimaryKey var pid: String, //podcast id
    var name: String,
    var sort_name: String,
    var desc: String,
    var feed_url: String,
    var mode: String,
    var category: String,
    var stored_logo_path: String,
    var logo_url: String,
    var updateIt: Int = 1

) : Serializable {
    constructor():this("","","","","","",
        "","", "")

    fun isEmpty() : Boolean {
        return name.equals("")
    }

    fun setAsUpdatable(status: Boolean) {
        if (status) {
            updateIt = 1
        } else {
            updateIt = 0
        }
    }

    fun isUpdatable() : Boolean {
        if (updateIt > 0) {
            return true
        } else {
            return false
        }
    }
}