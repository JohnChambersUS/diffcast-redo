package us.johnchambers.podcast.database.table_definitions

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index

@Entity(tableName = "podcast_tag",
    primaryKeys = arrayOf("pid", "tag"),
    indices = arrayOf(Index("pid", unique = false), Index("tag", unique = false)) //,
    //foreignKeys = arrayOf(
    //    ForeignKey(entity = PodcastRow::class,
    //        parentColumns = ["pid"],
    //        childColumns = ["pid"],
    //        onDelete = ForeignKey.CASCADE),
    //    ForeignKey(entity = TagRow::class,
    //        parentColumns = ["tag"],
    //        childColumns = ["tag"],
    //        onDelete = ForeignKey.CASCADE)
    //)
)
data class PodcastTagRow(
    var pid: String,
    var tag: String
) {
    constructor():this("","")
}