package us.johnchambers.podcast.database.table_definitions

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "tags",
    indices = arrayOf(Index("tag", unique = true)))
data class TagRow(
    @PrimaryKey var tag: String
) {
    constructor():this("")
}