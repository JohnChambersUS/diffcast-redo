package us.johnchambers.podcast.event

import com.google.android.material.snackbar.Snackbar

class EventSnackbar(var message: String, var long: Boolean = false) {
    fun length(): Int {
        return if (long) Snackbar.LENGTH_LONG else Snackbar.LENGTH_SHORT
    }
}