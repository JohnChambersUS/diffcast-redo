package us.johnchambers.podcast.event

import android.widget.Toast

class EventToast(var message: String, var long: Boolean = false) {
    fun length(): Int {
        return if (long) Toast.LENGTH_LONG else Toast.LENGTH_SHORT
    }
}