package us.johnchambers.podcast.fragment

import android.view.View
import androidx.fragment.app.Fragment
import us.johnchambers.podcast.misc.GC

abstract class BaseFragment : Fragment() {

    open protected val fragmentType = GC.fragmentType.root
    protected val typeOfSearch = "search"
    protected val typeOfSubscribe = "subscribe"
    protected val typeOfTest = "test"
    protected val typeOfDummy = "dummy"
    protected val typeOfGlobalOptions = "global options"
    protected val typeOfMyPodcasts = "my podcasts"
    protected val typeOfPodcastDetails = "podcast details"
    protected val typeOfTag = "tag"
    protected val typeOfTagDetail = "tag detail"
    protected val typeOfTagSelection = "tag selected"
    protected val typeOfManualQueue = "manual queue"

    abstract val identity: String
    lateinit var fragView: View

    fun makeGone() {
        //fragView.visibility = View.GONE
    }

    fun makeVisible() {
        fragView.visibility = View.VISIBLE
    }

    fun getType() : String {
        return fragmentType
    }

    fun isRoot() : Boolean {
        return fragmentType.equals(GC.fragmentType.root)
    }

    fun isBranch() : Boolean {
        return fragmentType.equals(GC.fragmentType.branch)
    }

    fun isLeaf(): Boolean {
        return fragmentType.equals(GC.fragmentType.leaf)
    }

    fun isSearchFragment(): Boolean {
       return identity.equals(typeOfSearch)
    }

    fun isSubscribeFragment(): Boolean {
        return identity.equals(typeOfSubscribe)
    }

    fun isTestFragment(): Boolean {
        return identity.equals(typeOfTest)
    }

    fun isDummyFragment(): Boolean {
        return identity.equals(typeOfDummy)
    }

    fun isGlobalOptionsFragment(): Boolean {
        return identity.equals(typeOfGlobalOptions)
    }

    fun isMyPodcastsFragment(): Boolean {
        return identity.equals(typeOfGlobalOptions)
    }

    fun isPodcastDetailsFragment(): Boolean {
        return identity.equals(typeOfPodcastDetails)
    }

    fun isTagFragment(): Boolean {
        return identity.equals(typeOfTag)
    }

    fun isTagDetailFragment(): Boolean {
        return identity.equals(typeOfTagDetail)
    }

    fun isTagSelectionFragment(): Boolean {
        return identity.equals(typeOfTagSelection)
    }

    fun isManualQueueFragment(): Boolean {
        return identity.equals(typeOfManualQueue)
    }

}