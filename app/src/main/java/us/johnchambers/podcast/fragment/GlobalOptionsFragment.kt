package us.johnchambers.podcast.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import us.johnchambers.podcast.R
import us.johnchambers.podcast.misc.GC

class GlobalOptionsFragment : BaseFragment() {

    override val identity =  typeOfGlobalOptions
    override val fragmentType = GC.fragmentType.leaf

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_global_options, container, false)
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            GlobalOptionsFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}
