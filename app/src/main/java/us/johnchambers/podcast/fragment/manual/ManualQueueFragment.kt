package us.johnchambers.podcast.fragment.manual

import android.content.Context
import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.ItemTouchHelper
import kotlinx.android.synthetic.main.fragment_manual_queue.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import us.johnchambers.podcast.R
import us.johnchambers.podcast.database.DBHelper
import us.johnchambers.podcast.database.table_definitions.PlaylistRow
import us.johnchambers.podcast.event.*
import us.johnchambers.podcast.fragment.BaseFragment
import us.johnchambers.podcast.misc.GC

class ManualQueueFragment : BaseFragment() {

    override val identity = typeOfManualQueue
    override val fragmentType = GC.fragmentType.root
    private lateinit var episodes: MutableList<PlaylistRow>
    private var queueName = "Dummy"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manual_queue, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        EventBus.getDefault().register(this)
        fillBottomMenu()
        fillEpisodeRows()
    }

    override fun onDestroy() {
        EventBus.getDefault().unregister(this)
        super.onDestroy()
    }

    companion object {
        @JvmStatic
        fun newInstance(manualQueueName: String) =
            ManualQueueFragment().apply {
                arguments = Bundle().apply {
                }
            queueName = manualQueueName
            }
    }

    private fun fillEpisodeRows() {
        fragment_manual_queue_name_header.text = queueName

        episodes = DBHelper.getPlaylistByName(queueName)
        toggleEmtpyMessage(episodes.isEmpty())

        manual_queue_episodes_recycler_view.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        manual_queue_episodes_recycler_view.setHasFixedSize(false)
        manual_queue_episodes_recycler_view.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        manual_queue_episodes_recycler_view.adapter = ManualQueueFragmentBodyAdapter(activity as Context, episodes, queueName)

        setItemTouchHelper()
    }

    private fun toggleEmtpyMessage(empty: Boolean) {
        while (manual_queue_episodes_recycler_view == null) {
            pauseOneSecond()
        }
        while (manual_queue_empty_message == null) {
            pauseOneSecond()
        }
        if (empty) {
            manual_queue_empty_message.visibility = View.VISIBLE
            manual_queue_empty_message.text = queueName + manual_queue_empty_message.text.toString()
            manual_queue_episodes_recycler_view.visibility = View.GONE
        } else {
            manual_queue_empty_message.visibility = View.GONE
            manual_queue_episodes_recycler_view.visibility = View.VISIBLE
        }
    }

    private fun pauseOneSecond() {
        EventBus.getDefault().post("pause one second manual")
        val timer = object: CountDownTimer(100, 100) {
            override fun onTick(millisUntilFinished: Long) {}
            override fun onFinish() {}
        }
        timer.start()
    }

    private fun fillBottomMenu() {

        var menuList = listOf("zero", "one", "two", "three")
        //fill adapter
        while (manual_queue_menu_recycler_view == null) {
            pauseOneSecond()
        }
        manual_queue_menu_recycler_view.layoutManager = LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false)
        manual_queue_menu_recycler_view.setHasFixedSize(false)
        manual_queue_menu_recycler_view.adapter = ManualQueueFragmentMenuAdapter(activity as Context, menuList, queueName)
        manual_queue_menu_recycler_view.addOnScrollListener(object: RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                try {
                    if (!manual_queue_menu_recycler_view.canScrollHorizontally(1)) {
                        manual_queue_right_arrow.visibility = View.GONE
                    } else {
                        manual_queue_right_arrow.visibility = View.VISIBLE
                    }
                    if (!manual_queue_menu_recycler_view.canScrollHorizontally(-1)) {
                        manual_queue_left_arrow.visibility = View.GONE
                    } else {
                        manual_queue_left_arrow.visibility = View.VISIBLE
                    }
                } catch(e: Exception) {}

            }
        })
    }

    //*******************************************
    //* event receivers
    //*******************************************

    @Subscribe
    fun onEvent(event: EventPlayerActivityClosed) {
        //episodes = DBHelper.getPlaylistByName(queueName)
        //manual_queue_episodes_recycler_view.adapter?.notifyDataSetChanged()
    }

    @Subscribe
    fun onEvent(event: EventUpdateManualDisplay) {
        episodes = DBHelper.getPlaylistByName(queueName)
        manual_queue_episodes_recycler_view.adapter?.notifyDataSetChanged()
        toggleEmtpyMessage(episodes.isEmpty())
    }

    @Subscribe
    fun onEvent(event: EventUpdateRowInDisplay) {
        episodes = DBHelper.getPlaylistByName(queueName)
        manual_queue_episodes_recycler_view.adapter?.notifyDataSetChanged()
        toggleEmtpyMessage(episodes.isEmpty())
    }

    @Subscribe
    fun onEvent(event: EventScrollManualQueue) {
        if (event.bottom) {
            var ad = manual_queue_episodes_recycler_view.adapter
            var x = ad?.itemCount
            x = x!! - 1
            manual_queue_episodes_recycler_view.scrollToPosition(x)
        } else {
            manual_queue_episodes_recycler_view.scrollToPosition(0)
        }
    }

    //*****************************************************************
    //* touch adapter for movement
    //*****************************************************************
    fun setItemTouchHelper() {

        val simpleCallback = object : ItemTouchHelper.SimpleCallback(
            ItemTouchHelper.UP or ItemTouchHelper.DOWN,
            ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {

            var dragFrom = -1
            var dragTo = -1

            override fun onMove(recyclerView: RecyclerView, source: RecyclerView.ViewHolder,
                                target: RecyclerView.ViewHolder): Boolean {

                if(dragFrom == -1) {
                    dragFrom =  source.adapterPosition;
                }
                dragTo = target.adapterPosition;

                return false
            }

            override fun clearView(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder) {
                super.clearView(recyclerView, viewHolder)

                if (dragFrom !== -1 && dragTo !== -1 && dragFrom !== dragTo) {
                    //episodes .moveItem(dragFrom, dragTo)
                    var item = episodes.get(dragFrom)
                    episodes.removeAt(dragFrom)
                    episodes.add(dragTo, item)

                    //_viewAdapter.notifyDataSetChanged()
                    manual_queue_episodes_recycler_view.adapter?.notifyDataSetChanged()
                    //todo update database
                    DBHelper.clearManualPlaylist(queueName)
                    for (row in episodes) {
                        DBHelper.addManualPlaylistRow(row)
                    }
                }
                dragTo = -1
                dragFrom = dragTo
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
            }

            override fun getSwipeDirs(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder): Int {
                return 0
            }
        }

        val itemTouchHelper = ItemTouchHelper(simpleCallback)
        itemTouchHelper.attachToRecyclerView(manual_queue_episodes_recycler_view)
    }
}
