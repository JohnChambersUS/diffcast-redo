package us.johnchambers.podcast.fragment.manual

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.LayerDrawable
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import kotlinx.android.synthetic.main.row_manual_queue_body.view.*
import org.greenrobot.eventbus.EventBus
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.selector
import org.jetbrains.anko.uiThread
import org.jetbrains.anko.windowManager
import us.johnchambers.podcast.R
import us.johnchambers.podcast.database.DBHelper
import us.johnchambers.podcast.database.table_definitions.EpisodeRow
import us.johnchambers.podcast.database.table_definitions.PlaylistRow
import us.johnchambers.podcast.event.EventOpenPlayer
import us.johnchambers.podcast.event.EventUpdateManualDisplay
import us.johnchambers.podcast.misc.*
import us.johnchambers.podcast.objects.docket.DocketPlaylist

class ManualQueueFragmentBodyAdapter(val context: Context, var items: MutableList<PlaylistRow>, val queueName: String) :
    RecyclerView.Adapter<ManualQueueFragmentBodyAdapter.Holder>() {

    var tapGuard = TapGuard(GC.tapGuard.shortTime)
    var fetchedImages = mutableMapOf<String, Bitmap>()

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(LayoutInflater.from(context).inflate(R.layout.row_manual_queue_body, parent, false))
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.header.visibility = View.GONE
        var currEpisodeRow = DBHelper.getEpisodeRowByEpisodeId(items.get(position).eid)
        holder.name.text = currEpisodeRow.title
        holder.publishDate.text = currEpisodeRow.publication_date.formatPodcastDate()
        ColorHelper.setSearchIcon(holder.button)
        var podcast = DBHelper.getPodcastRowByPid(currEpisodeRow.pid)

        if (holder.graphicHelper.isBadImageLink(podcast.logo_url)) {
            var art = holder.graphicHelper.getSmallPodcastImage(podcast.name)
            holder.image.setImageBitmap(art)
        } else {
            if (fetchedImages.containsKey(podcast.logo_url)) {
                holder.image.setImageBitmap(fetchedImages.get(podcast.logo_url))
            } else {
                Glide.with(context)
                    .asBitmap()
                    .error(R.drawable.unable_to_get_image)
                    .load(podcast.logo_url)
                    .into(object : CustomTarget<Bitmap>() {
                        override fun onResourceReady(
                            resource: Bitmap,
                            transition: com.bumptech.glide.request.transition.Transition<in Bitmap>?
                        ) {
                            holder.image.setImageBitmap(resource)
                            fetchedImages.set(podcast.logo_url, resource)
                        }

                        override fun onLoadCleared(placeholder: Drawable?) {
                        }
                    })
            }
        }

        holder.row.onClick {
        }

        setProgress(holder.row, items.get(position))

        holder.button.onClick {
            if (tapGuard.tooSoon()) return@onClick
            var actions = listOf("Play this episode", "Skip episode", "Reset to beginning", "Remove") //removed mark as complete
            var episodeInfo = DBHelper.getEpisodeRowByEpisodeId(items.get(position).eid)
            if (episodeInfo.isSkipped()) {
                actions = listOf("Play this episode", "Unskip episode", "Reset to beginning", "Remove")
            }
            context.selector("", actions, { dialogInterface, choice ->
                when(choice) {
                    0 -> {
                        if (!tapGuard.tooSoon()) {
                            episodeInfo.unskipEpisode()
                            if (episodeInfo.isComplete()) episodeInfo.setToBeginning()
                            DBHelper.insertEpisodeRow(episodeInfo)
                            var d = DocketPlaylist(queueName, episodeInfo.eid)
                            EventBus.getDefault().post(EventOpenPlayer(d))
                        }
                    }
                    1 -> {
                        if (episodeInfo.isSkipped()) {
                            episodeInfo.unskipEpisode()
                        } else {
                            episodeInfo.skipEpisode()
                        }
                        doAsync {
                            DBHelper.insertEpisodeRow(episodeInfo)
                            uiThread {
                                setProgress(holder.row, episodeInfo)
                            }
                        }
                    }

                    2 -> {
                        doAsync {
                            episodeInfo.setToBeginning()
                            DBHelper.insertEpisodeRow(episodeInfo)
                            uiThread {
                                setProgress(holder.row, episodeInfo)
                            }
                        }
                    }
                    3 -> {
                        if (!tapGuard.tooSoon()) {
                            DBHelper.removePlaylistRow(items.get(position))
                            items.removeAt(position)
                            EventBus.getDefault().post(EventUpdateManualDisplay())
                        }
                    }
                }
            })
        }
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        val headerName = view.row_manual_queue_name_header
        val header = view.row_manual_queue_name_header
        val image = view.row_manual_queue_logo_image
        val publishDate =  view.row_manual_queue_publish_date
        val name = view.row_manual_queue_episode_name
        var button = view.row_manual_queue_menu_image
        val row = view
        val graphicHelper = GraphicsHelper()
    }

    private fun setProgress(view: View, row: PlaylistRow) {
        var episode = DBHelper.getEpisodeRowByEpisodeId(row.eid)
        setProgress(view, episode)
    }

    private fun setProgress(view: View, episode: EpisodeRow) {


        var left = GradientDrawable()
        left.setShape(GradientDrawable.RECTANGLE);

        var v = TypedValue()
        if (episode.isSkipped()) {
            v.data = ColorHelper.getSkippedDarkColor()
        } else {
            v.data = ColorHelper.getProgressColor()
        }
        var cid = v.data
        left.setColor(cid)

        var right = GradientDrawable()
        right.setShape(GradientDrawable.RECTANGLE);

        var v2 = TypedValue()
        if (episode.isSkipped()) {
            v2.data = ColorHelper.getSkippedLightColor()
        } else {
            context.theme.resolveAttribute(android.R.attr.windowBackground, v2, true)
        }

        var cid2 = v2.data
        right.setColor(cid2);

        var ar = arrayOf(left, right)
        var layer = LayerDrawable(ar)

        var displayMetrics = DisplayMetrics()
        context.windowManager.defaultDisplay.getMetrics(displayMetrics)
        var width = displayMetrics.widthPixels

        var twentyWidth = (width * .2).toInt()
        width = width - twentyWidth

        var playPoint = episode.getPlayPointAsLong().toFloat()
        var length = episode.getLengthAsLong().toFloat()

        var ratio = 0f
        if (playPoint >= 0f) {
            if (playPoint >= length) {
                ratio = 1f
            } else {
                ratio = playPoint / length
            }
        }
        var size = Math.round(width * ratio).toInt()
        size = size + twentyWidth
        layer.setLayerInset(1, size, 0, 0, 0);
        view.setBackground(layer);

    } //end of setProgress



}