package us.johnchambers.podcast.fragment.manual

import android.content.Context
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.row_manual_queue_fragment_menu.view.*
import org.greenrobot.eventbus.EventBus
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.windowManager
import us.johnchambers.podcast.R
import us.johnchambers.podcast.database.DBHelper
import us.johnchambers.podcast.event.*
import us.johnchambers.podcast.misc.GC
import us.johnchambers.podcast.misc.TapGuard
import us.johnchambers.podcast.objects.docket.DocketPlaylist

class ManualQueueFragmentMenuAdapter(val context: Context, val items: List<String>, val queueName: String) :
    RecyclerView.Adapter<ManualQueueFragmentMenuAdapter.Holder>() {

    var tapGuard = TapGuard(GC.tapGuard.shortTime)

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(LayoutInflater.from(context).inflate(R.layout.row_manual_queue_fragment_menu, parent, false))
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        var width = getWidth()
        holder.row.maxWidth = width
        holder.row.minWidth = width
        when (position) {
            0 -> {
                holder.image.setImageResource(R.drawable.ic_menu_play_arrow_white_24dp)
                holder.desc.text = "PLAY"
                holder.image.onClick {
                    if (!tapGuard.tooSoon()) {
                        var d = DocketPlaylist(queueName, true)
                        EventBus.getDefault().post(EventOpenPlayer(d))
                    }
                }
            }
            1 -> {
                holder.image.setImageResource(R.drawable.ic_menu_clear_white_24dp)
                holder.desc.text = "CLEAR"
                holder.image.onClick {
                    if (!tapGuard.tooSoon()) {
                        DBHelper.clearManualPlaylist(queueName)
                        EventBus.getDefault().post(EventUpdateManualDisplay())
                    }
                }
            }
            2 -> {
                holder.image.setImageResource(R.drawable.ic_menu_bottom_white_24dp)
                holder.desc.text = "BOTTOM"
                holder.image.onClick {
                    if (!tapGuard.tooSoon()) {
                        EventBus.getDefault().post(EventScrollManualQueue(bottom = true))
                    }
                }
            }
            3 -> {
                holder.image.setImageResource(R.drawable.ic_menu_top_white_24dp)
                holder.desc.text = "TOP"
                holder.image.onClick {
                    if (!tapGuard.tooSoon()) {
                        EventBus.getDefault().post(EventScrollManualQueue(bottom = false))
                    }
                }
            }
        }
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        val image = view.row_manual_queue_image
        val desc = view.row_manual_queue_description
        val row = view.row_manual_queue_fragment_menu

    }

    private fun getWidth() : Int {
        var displayMetrics = DisplayMetrics()
        context.windowManager.defaultDisplay.getMetrics(displayMetrics)
        var width = displayMetrics.widthPixels
        var returnWidth = (width / 3).toInt()
        return returnWidth
    }

}