package us.johnchambers.podcast.fragment.my_podcasts


import android.content.Context
import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_my_podcasts.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import us.johnchambers.podcast.R
import us.johnchambers.podcast.database.DBHelper
import us.johnchambers.podcast.database.table_definitions.PodcastRow
import us.johnchambers.podcast.event.EventPodcastHasBeenDeleted
import us.johnchambers.podcast.event.EventRemovePodcast
import us.johnchambers.podcast.fragment.BaseFragment
import us.johnchambers.podcast.misc.GC

class MyPodcastsFragment : BaseFragment() {
    override val identity =  typeOfMyPodcasts
    override val fragmentType = GC.fragmentType.root
    private lateinit var podcastList: MutableList<PodcastRow>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
        EventBus.getDefault().register(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        fragView = inflater.inflate(R.layout.fragment_my_podcasts, container, false)
        return fragView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        displayPodcastRows()
    }

    companion object {
        @JvmStatic
        fun newInstance() = MyPodcastsFragment().apply {
        }

    }

    //*************************************************
    //* fragment specific functions
    //*************************************************

    //* do database call to get rows
    private fun displayPodcastRows() {
        podcastList = DBHelper.getPodcasts()
        if (podcastList.size == 0) {
            myPodcastsEmptyMessage.visibility = View.VISIBLE
        }
        //fill adapter
        while (my_podcasts_recycler_view == null) {
            pauseOneSecond()
        }
        my_podcasts_recycler_view.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        my_podcasts_recycler_view.setHasFixedSize(true)
        my_podcasts_recycler_view.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        my_podcasts_recycler_view.adapter = MyPodcastsFragmentAdapter(activity as Context, podcastList)
    }

    private fun pauseOneSecond() {
        val timer = object: CountDownTimer(100, 100) {
            override fun onTick(millisUntilFinished: Long) {}
            override fun onFinish() {}
        }
        timer.start()
    }

    @Subscribe
    fun onEvent(event: EventPodcastHasBeenDeleted) {
        if (podcastList.size == 0) {

            return
        }
        var pid = event.pid
        var i = -1
        while (++i < podcastList.size) {
            if (podcastList.get(i).pid.equals(pid)) break
        }
        if (i < podcastList.size) {
            podcastList.removeAt(i)
            my_podcasts_recycler_view.removeViewAt(i)
            my_podcasts_recycler_view.adapter?.notifyItemRemoved(i)
            my_podcasts_recycler_view.adapter?.notifyItemRangeChanged(i, podcastList.size)
            my_podcasts_recycler_view.adapter?.notifyDataSetChanged()
        }

        if (podcastList.size > 0)
            myPodcastsEmptyMessage.visibility = View.GONE
        else
            myPodcastsEmptyMessage.visibility = View.VISIBLE
    }

}
