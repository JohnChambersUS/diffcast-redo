package us.johnchambers.podcast.fragment.my_podcasts

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.row_my_podcasts.view.*
import org.greenrobot.eventbus.EventBus
import org.jetbrains.anko.sdk27.coroutines.onClick
import us.johnchambers.podcast.R
import us.johnchambers.podcast.database.table_definitions.EpisodeRow
import us.johnchambers.podcast.database.table_definitions.PodcastRow
import us.johnchambers.podcast.event.EventOpenPodcastDetailsFragment
import us.johnchambers.podcast.misc.ColorHelper
import us.johnchambers.podcast.misc.GC
import us.johnchambers.podcast.misc.GraphicsHelper
import us.johnchambers.podcast.misc.TapGuard

class MyPodcastsFragmentAdapter(val context: Context, var items: MutableList<PodcastRow>) :
    RecyclerView.Adapter<MyPodcastsFragmentAdapter.Holder>() {

    var tapGuard = TapGuard(GC.tapGuard.shortTime)

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyPodcastsFragmentAdapter.Holder {
        return Holder(LayoutInflater.from(context).inflate(R.layout.row_my_podcasts, parent, false))
    }

    override fun onBindViewHolder(holder: Holder, position: Int, payloads: MutableList<Any>) {
        if (payloads.size > 0) {
            val xlist = payloads.get(0) as MutableList<EpisodeRow>
            val row = xlist.get(position) as PodcastRow
            items.set(position, row)
        }
        super.onBindViewHolder(holder, position, payloads)
    }

    override fun onBindViewHolder(holder: MyPodcastsFragmentAdapter.Holder, position: Int) {
        holder.name.text = items[position].name
        var awu = items.get(position).logo_url
        if (holder.graphicHelper.isBadImageLink(awu)) {
            var art = holder.graphicHelper.getSmallPodcastImage(items.get(position).name)
            holder.image.setImageBitmap(art)
        } else {
            Glide.with(context)
                .applyDefaultRequestOptions(
                    RequestOptions()
                        .error(R.drawable.unable_to_get_image)
                )
                .load(items.get(position).logo_url)
                .into(holder.image)
        }
        if (items.get(position).mode != GC.playbackModes.PODCAST) {
            holder.bookCorner.visibility = View.VISIBLE
        }
        ColorHelper.setSubscribePlayButtonIcon(holder.button)
        holder.row.onClick {
            if (!tapGuard.tooSoon())
                //EventBus.getDefault().post((EventOpenSubscribeFragment(items.get(position))))
                EventBus.getDefault().post(EventOpenPodcastDetailsFragment(items.get(position).pid))
        }
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        val name = view.my_podcasts_name
        val image = view.my_podcasts_image
        val button = view.my_podcasts_action_button
        val bookCorner = view.my_podcasts_book_fold
        val row = view
        val graphicHelper = GraphicsHelper()
    }


}