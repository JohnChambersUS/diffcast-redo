package us.johnchambers.podcast.fragment.podcast_details


import android.content.Context
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_podcast_details.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.jetbrains.anko.alert
import org.jetbrains.anko.appcompat.v7.Appcompat
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import us.johnchambers.podcast.R
import us.johnchambers.podcast.database.DBHelper
import us.johnchambers.podcast.database.table_definitions.EpisodeRow
import us.johnchambers.podcast.database.table_definitions.PodcastRow
import us.johnchambers.podcast.event.*
import us.johnchambers.podcast.fragment.BaseFragment
import us.johnchambers.podcast.misc.GC
import us.johnchambers.podcast.misc.setHeight
import us.johnchambers.podcast.objects.docket.DocketPodcast

class PodcastDetailsFragment : BaseFragment() {
    override val identity = typeOfPodcastDetails
    override val fragmentType = GC.fragmentType.branch
    private lateinit var pid : String
    private lateinit var episodes: MutableList<EpisodeRow>
    private lateinit var podcastRow: PodcastRow

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_podcast_details, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        EventBus.getDefault().register(this)
        fillBottomMenu()
        fillHeaderView()
        fillEpisodeRows()
    }

    override fun onDestroy() {
        EventBus.getDefault().unregister(this)
        super.onDestroy()
    }

    companion object {
        @JvmStatic
        fun newInstance(podcastId: String) =
            PodcastDetailsFragment().apply {
                arguments = Bundle().apply {
                }
                this.pid = podcastId
            }
    }

    private fun fillEpisodeRows() {
        episodes = DBHelper.getEpisodeRowsByPid(pid)
        podcastRow = DBHelper.getPodcastRowByPid(pid)
        while (podcast_details_episodes_recycler_view == null) {
            pauseOneSecond()
        }
        podcast_details_episodes_recycler_view.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        podcast_details_episodes_recycler_view.setHasFixedSize(true)
        podcast_details_episodes_recycler_view.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        podcast_details_episodes_recycler_view.adapter = PodcastDetailsFragmentEpisodeAdapter(activity as Context, episodes, podcastRow )
    }

    private fun pauseOneSecond() {
        EventBus.getDefault().post("pause one second podcast")
        val timer = object: CountDownTimer(100, 100) {
            override fun onTick(millisUntilFinished: Long) {}
            override fun onFinish() {}
        }
        timer.start()
    }

    private fun fillHeaderView() {

    }

    private fun fillBottomMenu() {
        var menuList = listOf(pid, "one", "two", "three", "four", "five")
        //fill adapter
        while (podcast_details_menu_recycler_view == null) {
            pauseOneSecond()
        }
        podcast_details_menu_recycler_view.layoutManager = LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false)
        podcast_details_menu_recycler_view.setHasFixedSize(true)
        podcast_details_menu_recycler_view.adapter = PodcastDetailsFragmentMenuAdapter(activity as Context, menuList)
        podcast_details_menu_recycler_view.addOnScrollListener(object:RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                try {
                    if (!podcast_details_menu_recycler_view.canScrollHorizontally(1)) {
                        podcast_details_right_arrow.visibility = View.GONE
                    } else {
                        podcast_details_right_arrow.visibility = View.VISIBLE
                    }
                    if (!podcast_details_menu_recycler_view.canScrollHorizontally(-1)) {
                        podcast_details_left_arrow.visibility = View.GONE
                    } else {
                        podcast_details_left_arrow.visibility = View.VISIBLE
                    }
                } catch(e: Exception) {}

            }
        })
    }

    //*******************************************
    //* event receivers
    //*******************************************
    @Subscribe
    fun onEvent(event: EventDetailsFragmentMenuPlayButton) {
        try {
            //Log.d("~~~~~" + System.currentTimeMillis().toString(), "PodcastDetailsFragment: Throwing event open: blank episode")
            var docket = DocketPodcast(pid, "")
            EventBus.getDefault().post(EventOpenPlayer(docket))
        } catch (e: Exception) {

        }
    }

    @Subscribe
    fun onEvent(event: EventScrollDetailFragmentToBottom) {
        try {
            var ad = podcast_details_episodes_recycler_view.adapter
            var x = ad?.itemCount
            x = x!! - 1
            podcast_details_episodes_recycler_view.scrollToPosition(x)
        } catch (e: java.lang.Exception) {}
    }

    @Subscribe
    fun onEvent(event: EventScrollDetailFragmentToTop) {
        try {
            var ad = podcast_details_episodes_recycler_view.adapter
            podcast_details_episodes_recycler_view.scrollToPosition(0)
        } catch (e: Exception) {}
    }

    @Subscribe
    fun onEvent(event: EventShowPodcastDescription) {
        try {
            var p = DBHelper.getPodcastRowByPid(pid)
            var title = p.name
            var desc = p.desc
            context?.alert(Appcompat, title + "\n\n" + desc)?.show()
        } catch (e: Exception) {}
    }

    @Subscribe
    fun onEvent(event: EventUpdateRowInDisplay) {
        doAsync {
            episodes.set(event.rowNumber, DBHelper.getEpisodeRowByEpisodeId(episodes.get(event.rowNumber).eid))
            uiThread {
                podcast_details_episodes_recycler_view.adapter?.notifyItemChanged(event.rowNumber, episodes)
            }
        }
    }

    @Subscribe
    fun onEvent(event: EventPlayerActivityClosed) {
    }

}
