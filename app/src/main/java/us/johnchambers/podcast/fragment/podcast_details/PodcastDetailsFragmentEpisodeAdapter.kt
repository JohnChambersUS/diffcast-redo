package us.johnchambers.podcast.fragment.podcast_details

import android.app.AlertDialog
import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.LayerDrawable
import android.util.DisplayMetrics
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.row_podcast_details_episode.view.*
import org.greenrobot.eventbus.EventBus
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.*
import us.johnchambers.podcast.R
import us.johnchambers.podcast.database.DBHelper
import us.johnchambers.podcast.database.table_definitions.EpisodeRow
import us.johnchambers.podcast.database.table_definitions.PodcastRow
import us.johnchambers.podcast.event.EventOpenPlayer
import us.johnchambers.podcast.misc.*
import us.johnchambers.podcast.objects.docket.DocketPodcast

class PodcastDetailsFragmentEpisodeAdapter(val context: Context, var items: MutableList<EpisodeRow>, val podcastRow: PodcastRow) :
    RecyclerView.Adapter<PodcastDetailsFragmentEpisodeAdapter.Holder>() {

    var tapGuard = TapGuard(GC.tapGuard.shortTime)

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PodcastDetailsFragmentEpisodeAdapter.Holder {
        return Holder(LayoutInflater.from(context).inflate(R.layout.row_podcast_details_episode, parent, false))
    }

    override fun onBindViewHolder(holder: Holder, position: Int, payloads: MutableList<Any>) {
        if (payloads.size > 0) {

            val xlist = payloads.get(0) as MutableList<EpisodeRow>
            val row = xlist.get(position) as EpisodeRow
            items.set(position, row)
        }
        super.onBindViewHolder(holder, position, payloads)
    }

    override fun onBindViewHolder(holder: PodcastDetailsFragmentEpisodeAdapter.Holder, position: Int) {
        if (position == 0) {
            if (holder.graphicHelper.isBadImageLink(podcastRow.logo_url)) {
                var art = holder.graphicHelper.getBigPodcastImage(podcastRow.name)
                holder.headerImage.setImageBitmap(art)
            } else {
                Glide.with(context)
                    .applyDefaultRequestOptions(
                        RequestOptions()
                            .error(R.drawable.unable_to_get_image)
                    )
                    .load(podcastRow.logo_url)
                    .into(holder.headerImage)
            }
            holder.headerImage.visibility = View.VISIBLE
            holder.guideline.setBeginWithPercentage(0.5f)
        } else {
            holder.headerImage.visibility = View.GONE
            holder.guideline.setGuidelineBegin(0)
        }
        holder.date.text = items.get(position).publication_date.formatPodcastDate()
        holder.name.text = items.get(position).title
        holder.menuIcon.onClick {
            var actions = listOf("Play this episode", "Skip episode", "Reset to beginning", "Add to manual queue") //removed mark as complete
            val skipped = items.get(position).isSkipped()
            if (skipped) {
                actions = listOf("Play this episode", "Unskip episode", "Reset to beginning", "Add to manual queue")
            }
            context.selector("", actions, { dialogInterface, choice ->
                when(choice) {
                    0 -> {
                        var e = items.get(position) as EpisodeRow
                        var d = DocketPodcast(e.pid, e.eid)
                        //var episodes = DBHelper.getPlaylistByName(GC.playlist.MANUAL)
                        EventBus.getDefault().post(EventOpenPlayer(d))
                    }
                    1 -> {
                        if (skipped) {
                            items.get(position).unskipEpisode()
                        } else {
                            items.get(position).skipEpisode()
                        }
                        doAsync {
                            DBHelper.insertEpisodeRow(items.get(position))
                            uiThread {
                                setProgress(holder.rowView, items.get(position))
                            }
                        }
                    }
                    2 -> {
                        doAsync {
                            items.get(position).setToBeginning()
                            DBHelper.insertEpisodeRow(items.get(position))
                            uiThread {
                                setProgress(holder.rowView, items.get(position))
                            }
                        }
                    }
                    3 -> {
                        openManualQueueMenu(items.get(position).eid)
                    }
                }
            })
        }
        setProgress(holder.rowView, items.get(position))
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        val date = view.row_podcast_detail_episode_row_publish_date
        var name = view.row_podcast_detail_episode_name
        var menuIcon = view.row_podcast_detail_episode_menu_image
        var headerImage = view.podcast_details_header_image_x
        var guideline = view.row_podcast_detail_episode_bottom_of_header_guideline
        var rowView = view
        val graphicHelper = GraphicsHelper()
    }

    private fun setProgress(view: View, episode: EpisodeRow) {
        var left = GradientDrawable()
        left.setShape(GradientDrawable.RECTANGLE);

        var v = TypedValue()
        if (episode.isSkipped()) {
            v.data = ColorHelper.getSkippedDarkColor()
        } else {
            v.data = ColorHelper.getProgressColor()
        }
        var cid = v.data
        left.setColor(cid)

        var right = GradientDrawable()
        right.setShape(GradientDrawable.RECTANGLE);

        var v2 = TypedValue()
        if (episode.isSkipped()) {
            v2.data = ColorHelper.getSkippedLightColor()
        } else {
            context.theme.resolveAttribute(android.R.attr.windowBackground, v2, true)
        }

        var cid2 = v2.data
        right.setColor(cid2);

        var ar = arrayOf(left, right)
        var layer = LayerDrawable(ar)

        var displayMetrics = DisplayMetrics()
        context.windowManager.defaultDisplay.getMetrics(displayMetrics)
        var width = displayMetrics.widthPixels

        var playPoint = episode.getPlayPointAsLong().toFloat()
        var length = episode.getLengthAsLong().toFloat()

        var ratio = 0f
        if (playPoint >= 0f) {
            if (playPoint >= length) {
                ratio = 1f
            } else {
                ratio = playPoint / length
            }
        }
        var size = Math.round(width * ratio).toInt()
        layer.setLayerInset(1, size, 0, 0, 0);
        view.setBackground(layer);

    } //end of setProgress

    private fun openManualQueueMenu(eid: String) {

        val array = arrayOf("One","Two","Three","Four","Five","Six","Seven","Eight","Nine","Ten")
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Add to which manual queue?")
        builder.setItems(array,{_, which ->
            val selected = array[which]
            when(which) {
                0 -> DBHelper.addManualPlaylistRow(eid, GC.playlist.manual.MANUAL_QUEUE_1)
                1 -> DBHelper.addManualPlaylistRow(eid, GC.playlist.manual.MANUAL_QUEUE_2)
                2 -> DBHelper.addManualPlaylistRow(eid, GC.playlist.manual.MANUAL_QUEUE_3)
                3 -> DBHelper.addManualPlaylistRow(eid, GC.playlist.manual.MANUAL_QUEUE_4)
                4 -> DBHelper.addManualPlaylistRow(eid, GC.playlist.manual.MANUAL_QUEUE_5)
                5 -> DBHelper.addManualPlaylistRow(eid, GC.playlist.manual.MANUAL_QUEUE_6)
                6 -> DBHelper.addManualPlaylistRow(eid, GC.playlist.manual.MANUAL_QUEUE_7)
                7 -> DBHelper.addManualPlaylistRow(eid, GC.playlist.manual.MANUAL_QUEUE_8)
                8 -> DBHelper.addManualPlaylistRow(eid, GC.playlist.manual.MANUAL_QUEUE_9)
                9 -> DBHelper.addManualPlaylistRow(eid, GC.playlist.manual.MANUAL_QUEUE_10)
                else -> DBHelper.addManualPlaylistRow(eid, GC.playlist.manual.MANUAL_QUEUE_1)
            }
        })
        val dialog = builder.create()
        dialog.show()
    }


}