package us.johnchambers.podcast.fragment.podcast_details

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.row_podcast_details_header.view.*
import us.johnchambers.podcast.R
import us.johnchambers.podcast.database.table_definitions.PodcastRow

class PodcastDetailsFragmentHeaderAdapter(val context: Context, val items: List<PodcastRow?>) :
    RecyclerView.Adapter<PodcastDetailsFragmentHeaderAdapter.Holder>() {

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PodcastDetailsFragmentHeaderAdapter.Holder {
        return Holder(LayoutInflater.from(context).inflate(R.layout.row_podcast_details_header, parent, false))
    }

    override fun onBindViewHolder(holder: PodcastDetailsFragmentHeaderAdapter.Holder, position: Int) {
        if (position == 0) {
            Glide.with(context)
                .applyDefaultRequestOptions(RequestOptions()
                    .error(R.drawable.unable_to_get_image))
                .load(items.get(0)?.logo_url)
                .into(holder.image)
            holder.image.visibility = View.VISIBLE
            holder.title.visibility = View.GONE
            holder.desc.visibility = View.GONE
        }
        else {
            holder.title.text = items.get(0)?.name
            holder.desc.text = items.get(0)?.desc
            holder.image.visibility = View.GONE
            holder.title.visibility = View.VISIBLE
            holder.desc.visibility = View.VISIBLE
        }
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        val image = view.row_podcast_detail_header_image
        val title = view.row_podcast_detail_title_textview
        val desc = view.row_podcast_detail_desc_textview
    }

}
