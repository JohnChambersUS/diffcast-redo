package us.johnchambers.podcast.fragment.podcast_details

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.podcast_option_speed.view.*
import kotlinx.android.synthetic.main.row_podcast_details_fragment_menu.view.*
import kotlinx.android.synthetic.main.row_search.view.*
import kotlinx.android.synthetic.main.row_search.view.image
import org.greenrobot.eventbus.EventBus
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk27.coroutines.onClick
import us.johnchambers.podcast.R
import us.johnchambers.podcast.database.DBHelper
import us.johnchambers.podcast.event.*
import us.johnchambers.podcast.misc.GC
import us.johnchambers.podcast.misc.OptionsHelper
import us.johnchambers.podcast.misc.TapGuard

class PodcastDetailsFragmentMenuAdapter(val context: Context, val items: List<String>) :
    RecyclerView.Adapter<PodcastDetailsFragmentMenuAdapter.Holder>() {

    var tapGuard = TapGuard(GC.tapGuard.shortTime)

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(LayoutInflater.from(context).inflate(R.layout.row_podcast_details_fragment_menu, parent, false))
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        var width = getWidth()
        holder.row.maxWidth = width
        holder.row.minWidth = width
        when (position) {
            0 -> {
                holder.image.setImageResource(R.drawable.ic_menu_play_arrow_white_24dp)
                holder.desc.text = "PLAY"
                holder.row.onClick {
                    if (!tapGuard.tooSoon()) {
                        EventBus.getDefault().post(EventDetailsFragmentMenuPlayButton())
                    }
                }
            }
            1 -> {
                holder.image.setImageResource(R.drawable.ic_info_white_speed)
                holder.desc.text = "SPEED"
                //todo update when tags added
                holder.row.onClick {
                    if (!tapGuard.tooSoon()) {
                        displayOptionsSpeed()
                    }
                }
            }
            2 -> {
                holder.image.setImageResource(R.drawable.ic_menu_bottom_white_24dp)
                holder.desc.text = "BOTTOM"
                holder.row.onClick {
                    if (!tapGuard.tooSoon()) {
                        EventBus.getDefault().post(EventScrollDetailFragmentToBottom())
                    }
                }
            }
            3 -> {
                holder.image.setImageResource(R.drawable.ic_menu_top_white_24dp)
                holder.desc.text = "TOP"
                holder.row.onClick {
                    if (!tapGuard.tooSoon()) {
                        EventBus.getDefault().post(EventScrollDetailFragmentToTop())
                    }
                }
            }
            4 -> {
                holder.image.setImageResource(R.drawable.ic_menu_info_white_24dp)
                holder.desc.text = "INFO"
                //todo update when tags added
                holder.row.onClick {
                    if (!tapGuard.tooSoon()) {
                        //EventBus.getDefault().post(EventShowPodcastDescription())
                        try {
                            var p = DBHelper.getPodcastRowByPid(items.get(0))
                            var title = p.name
                            var desc = p.desc
                            context?.alert(desc, title)?.show()
                        } catch (e: Exception) {}
                    }
                }
            }
            5 -> {
                holder.image.setImageResource(R.drawable.ic_menu_remove_white_24dp)
                holder.desc.text = "REMOVE"
                //todo update when tags added
                holder.row.onClick {
                    if (!tapGuard.tooSoon()) {
                        context.alert("Are you sure you want to remove this podcast?") {
                            positiveButton("Yes") {
                                EventBus.getDefault().post(EventRemovePodcast(items.get(0)))
                            }
                            negativeButton("No") {}
                        }.show()
                    }
                }
            }
        }
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        val image = view.image
        val desc = view.description
        val row = view.row_podcast_fragment_menu
    }

    private fun getWidth() : Int {
        var displayMetrics = DisplayMetrics()
        context.windowManager.defaultDisplay.getMetrics(displayMetrics)
        var width = displayMetrics.widthPixels
        var returnWidth = (width / 3).toInt()
        return returnWidth
    }

    private fun displayOptionsSpeed() {
        val builder = AlertDialog.Builder(context)
        val inflater = context.layoutInflater
        var v = inflater.inflate(R.layout.podcast_option_speed, null)
        var oh = OptionsHelper()
        var speed = oh.getSpeedForPodcastAsString(items.get(0))

        for (i in 0..(v.radio_speed.childCount - 1)) {
            var button = v.radio_speed.getChildAt(i) as RadioButton
            var tag = button.getTag()
            if (tag.toString().equals(speed.toString())) button.isChecked = true
        }

        builder.setView(v)
            .setPositiveButton("done",
                DialogInterface.OnClickListener { dialog, id ->
                    var checked = v.radio_speed.checkedRadioButtonId
                    var r = v.find<RadioButton>(checked)
                    var speed = r.getTag()
                    oh.setSpeedForPodcast(items.get(0), speed.toString())
                })
            .setNegativeButton("cancel",
                DialogInterface.OnClickListener { dialog, id ->
                    //getDialog()
                })
        builder.create()
        builder.show()
    }

}