package us.johnchambers.podcast.fragment.search


import android.content.Context
import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.RadioButton
import android.widget.RadioGroup
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.android.synthetic.main.fragment_search.view.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.sdk27.coroutines.onEditorAction
import org.jetbrains.anko.support.v4.toast
import org.jetbrains.anko.uiThread
import us.johnchambers.podcast.R
import us.johnchambers.podcast.fragment.BaseFragment
import java.lang.Exception
import java.net.URL
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import org.jetbrains.anko.inputMethodManager
import us.johnchambers.podcast.misc.hideKeyboard
import us.johnchambers.podcast.objects.PodcastInfo
import us.johnchambers.podcast.objects.search_result.ItunesSearchResult
import androidx.recyclerview.widget.DividerItemDecoration
import org.greenrobot.eventbus.EventBus
import org.jetbrains.anko.find
import us.johnchambers.podcast.event.EventOpenSubscribeFragment
import us.johnchambers.podcast.event.EventToast
import us.johnchambers.podcast.misc.ColorHelper
import us.johnchambers.podcast.objects.search_result.RssSearchResult

class SearchFragment : BaseFragment() {

    override val identity = typeOfSearch
    val iTunesSearchString = "https://itunes.apple.com/search?media=podcast&entity=podcast&limit=200&lang=ja_jp&term="

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        fragView = inflater.inflate(R.layout.fragment_search, container, false)
        ColorHelper.setSearchButtonIcon(fragView.goSearchButton, activity as Context)
        setListeners()
        var radioView = fragView.findViewById(R.id.search_radio_buttons) as View
        ColorHelper.setSearchRadioBackground(radioView)
        hideAllScreenItems()
        showPreMessageItunes()
        return fragView
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            SearchFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }

    //**************************************
    //* common private methods
    //**************************************

    private fun doSearch() {

        var checked = search_radio_group.checkedRadioButtonId
        var r = search_radio_group.find<RadioButton>(checked)
        var selection = r.getTag().toString()
        when (selection.toLowerCase()) {
            "itunes" -> searchPodcast()
            "rss" -> searchRss()
            "librivox" -> searchLibrivox()
            else -> searchPodcast()
        }
    }

    private fun searchPodcast() {
        hideAllScreenItems()
        var errorMsg = ""
        var result = ""

        if (searchInputBox.text.length < 1) {
            EventBus.getDefault().post(EventToast("Enter a search term."))
            showPreMessageItunes()
            return
        }

        doAsync {
            fragView.hideKeyboard(activity!!.inputMethodManager)
            try {
                result = URL(iTunesSearchString + searchInputBox.text.trim()).readText()
            } catch (e: Exception) {
                errorMsg = e.message.toString()
            }
            uiThread {
                if (errorMsg.length > 0) {
                    EventBus.getDefault().post(EventToast(errorMsg))
                    showPreMessageItunes()
                } else {
                    var searchResult = ItunesSearchResult(result)
                    var podcastInfo = searchResult.getResultsList()
                    if (podcastInfo.size == 0) {
                        hideAllScreenItems()
                        searchEmptyMessageItunes.visibility = View.VISIBLE
                    } else {
                        hideAllScreenItems()
                        frame_search_recycler_view.visibility = View.VISIBLE
                        fillRecyclerView(podcastInfo)
                    }
                }
            }
        }
    }

    private fun searchRss() {
        var errorMsg = ""
        var result = ""

        if (searchInputBox.text.length < 1) {
            EventBus.getDefault().post(EventToast("Enter a link."))
            return
        }

        var searchString = searchInputBox.text.trim().toString()
        fragView.hideKeyboard(activity!!.inputMethodManager)

        doAsync {
            try {
                result = URL(searchString).readText()
            } catch (e: Exception) {
                errorMsg = e.message.toString()
            }
            uiThread {
                if (errorMsg.length > 0) {
                    EventBus.getDefault().post(EventToast(errorMsg))
                } else {
                    var rssResult = RssSearchResult(result, searchString)
                    if (rssResult.errorLoading()) {
                        EventBus.getDefault().post(EventToast("Error fetching info from link."))
                    } else {
                        EventBus.getDefault().post(EventOpenSubscribeFragment(rssResult.getPodcastInfo()))
                    }
                }
            }
        }
    }

    private fun searchLibrivox() {
        var errorMsg = ""
        var result = ""

        if (searchInputBox.text.length < 1) {
            EventBus.getDefault().post(EventToast("Enter a number."))
            return
        }

        var searchString = searchInputBox.text.trim().toString()
        if (searchString.toIntOrNull() == null) {
            EventBus.getDefault().post(EventToast("Enter a number, no letters please."))
            return
        }

        searchString = "https://Librivox.org/rss/" + searchString
        fragView.hideKeyboard(activity!!.inputMethodManager)

        doAsync {
            try {
                result = URL(searchString).readText()
            } catch (e: Exception) {
                errorMsg = e.message.toString()
            }
            uiThread {
                if (errorMsg.length > 0) {
                    toast(errorMsg)
                } else {
                    var rssResult = RssSearchResult(result, searchString)
                    if (rssResult.errorLoading()) {
                        EventBus.getDefault().post(EventToast("Error fetching Librivox info."))
                    } else {
                        EventBus.getDefault().post(EventOpenSubscribeFragment(rssResult.getPodcastInfo()))
                    }
                }
            }
        }
    }

    //****************************************************************

    private fun fillRecyclerView(podcastInfoList: List<PodcastInfo>) {
        while (recycler_view == null) {
            pauseOneSecond()
        }
        recycler_view.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        recycler_view.setHasFixedSize(true)
        recycler_view.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        recycler_view.adapter = SearchFragmentAdapter(activity as Context, podcastInfoList)
    }

    private fun pauseOneSecond() {
        val timer = object: CountDownTimer(100, 100) {
            override fun onTick(millisUntilFinished: Long) {}
            override fun onFinish() {}
        }
        timer.start()
    }

    private fun setListeners() {
        fragView.goSearchButton.onClick {
            doSearch()
        }

        fragView.searchInputBox.onEditorAction() {v, actionId, event ->
            if(actionId == EditorInfo.IME_ACTION_SEARCH){
                doSearch()
                true
            } else {
                false
            }
        }

        //set listener for radio group
        fragView.search_radio_group.setOnCheckedChangeListener(
            RadioGroup.OnCheckedChangeListener { group, checked ->
                var r = fragView.search_radio_group.find<RadioButton>(checked)
                var selection = r.getTag().toString()
                hideAllScreenItems()
                when (selection.toLowerCase()) {
                    "itunes" -> showPreMessageItunes()
                    "rss" -> showPreMessageRss()
                    "librivox" -> showPreMessageLibrivox()
                    else -> showPreMessageItunes()
                }
            })
    }

    private fun hideAllScreenItems() {
        fragView.searchPreMessageItunes.visibility = View.GONE
        fragView.searchEmptyMessageItunes.visibility = View.GONE

        fragView.searchPreMessageRss.visibility = View.GONE
        fragView.searchEmptyMessageRss.visibility = View.GONE

        fragView.searchPreMessageLibrivox.visibility = View.GONE
        fragView.searchEmptyMessageLibrivox.visibility = View.GONE

        fragView.frame_search_recycler_view.visibility = View.GONE
    }

    private fun showPreMessageItunes() {
        fragView.searchPreMessageItunes.visibility = View.VISIBLE
    }

    private fun showPreMessageRss() {
        fragView.searchPreMessageRss.visibility = View.VISIBLE
    }

    private fun showPreMessageLibrivox() {
        fragView.searchPreMessageLibrivox.visibility = View.VISIBLE
    }


}
