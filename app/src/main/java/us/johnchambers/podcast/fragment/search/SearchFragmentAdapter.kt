package us.johnchambers.podcast.fragment.search

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import us.johnchambers.podcast.objects.PodcastInfo
import android.view.LayoutInflater
import android.view.ViewGroup
import kotlinx.android.synthetic.main.row_search.view.*
import org.jetbrains.anko.sdk27.coroutines.onClick
import us.johnchambers.podcast.R
import org.greenrobot.eventbus.EventBus;
import us.johnchambers.podcast.event.EventOpenSubscribeFragment
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import us.johnchambers.podcast.misc.ColorHelper
import us.johnchambers.podcast.misc.GC
import us.johnchambers.podcast.misc.GraphicsHelper
import us.johnchambers.podcast.misc.TapGuard

class SearchFragmentAdapter(val context: Context, val items: List<PodcastInfo>) :
            RecyclerView.Adapter<SearchFragmentAdapter.Holder>() {

    var tapGuard = TapGuard(GC.tapGuard.shortTime)

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(LayoutInflater.from(context).inflate(R.layout.row_search, parent, false))
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.name.text = items[position].name
        ColorHelper.setSearchIcon(holder.button)

        var awu = items.get(position).artworkUrl
        if (holder.graphicHelper.isBadImageLink(awu)) {
            var art = holder.graphicHelper.getSmallPodcastImage(items.get(position).name)
            holder.image.setImageBitmap(art)
        } else {
            Glide.with(context)
                .applyDefaultRequestOptions(
                    RequestOptions()
                        .error(R.drawable.unable_to_get_image)
                )
                .load(items.get(position).artworkUrl)
                .into(holder.image)
        }
        holder.row.onClick {
            if (!tapGuard.tooSoon())
                EventBus.getDefault().post((EventOpenSubscribeFragment(items.get(position))))
        }
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        val name = view.name
        val image = view.image
        var button = view.search_action_button
        val row = view
        val graphicHelper = GraphicsHelper()
    }



}