package us.johnchambers.podcast.fragment.subscribe
import java.io.Serializable
import java.util.Date

class SubscribeEpisodesInfo(
    val name: String = "",
    val date: Date = Date(),
    val episodeUrl: String = "",
    val logoUrl: String = ""
) :  Serializable {
}