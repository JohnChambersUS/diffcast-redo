package us.johnchambers.podcast.fragment.subscribe

import android.content.Context
import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_subscribe.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import us.johnchambers.podcast.R
import us.johnchambers.podcast.fragment.BaseFragment
import us.johnchambers.podcast.misc.GC
import us.johnchambers.podcast.objects.PodcastFeedWrapper
import us.johnchambers.podcast.objects.PodcastInfo
import java.lang.Exception
import java.net.URL
import org.greenrobot.eventbus.EventBus
import us.johnchambers.podcast.event.EventCloseTopFragment
import us.johnchambers.podcast.event.EventToast

class SubscribeFragment : BaseFragment() {

    override val identity = typeOfSubscribe
    override val fragmentType = GC.fragmentType.branch

    lateinit var podcastInfo: PodcastInfo
    lateinit var feedWrapper: PodcastFeedWrapper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        fragView = inflater.inflate(R.layout.fragment_subscribe, container, false)
        return fragView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        getPodcastDetails()
    }

    companion object {
        @JvmStatic
        fun newInstance(podcastInfo: PodcastInfo) =
            SubscribeFragment().apply {
                arguments = Bundle().apply {
                }
                this.podcastInfo = podcastInfo
            }
    }

    private fun getPodcastDetails() {
        doAsync {
            var result = ""
            var errorMsg = ""
            try {
                result = URL(podcastInfo.feedUrl).readText()
            } catch (e: Exception) {
                errorMsg = e.message.toString()
            }
            uiThread {
                //todo handle empty feed
                var feed = PodcastFeedWrapper(result, podcastInfo)
                if (feed.error) {
                    EventBus.getDefault().post(EventToast("Unable to get this podcast's details"))
                    EventBus.getDefault().post(EventCloseTopFragment())
                } else {
                    feedWrapper = feed
                    var episodesInfo = feed.getEpisodeInfo()
                    fillEpisodeRecyclerView(episodesInfo)
                    fillBottomMenu()
                }
            }
        }
    }

    private fun fillEpisodeRecyclerView(items: List<SubscribeEpisodesInfo>) {
        while (fragment_subscribe_episodes_recycler_view == null) {
            pauseOneSecond()
        }
        fragment_subscribe_episodes_recycler_view.layoutManager = LinearLayoutManager(activity,
                                                                        RecyclerView.VERTICAL,
                                                        false)
        fragment_subscribe_episodes_recycler_view.setHasFixedSize(true)
        fragment_subscribe_episodes_recycler_view.addItemDecoration(DividerItemDecoration(activity,
                                                                        DividerItemDecoration.VERTICAL))
        fragment_subscribe_episodes_recycler_view.adapter = SubscribeFragmentEpisodesAdapter(activity as Context,
                                                            items,
                                                            feedWrapper.getImageUrl(), feedWrapper.getTitle())
    }

    private fun fillBottomMenu() {
        var menuList = listOf(feedWrapper, "one")
        while (fragment_subscribe_menu_recycler_view == null) {
            pauseOneSecond()
        }
        fragment_subscribe_menu_recycler_view.layoutManager = LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false)
        fragment_subscribe_menu_recycler_view.setHasFixedSize(true)
        fragment_subscribe_menu_recycler_view.adapter = SubscribeMenuAdapter(activity as Context, menuList)
    }

    private fun pauseOneSecond() {
        val timer = object: CountDownTimer(100, 100) {
            override fun onTick(millisUntilFinished: Long) {}
            override fun onFinish() {}
        }
        timer.start()
    }

}
