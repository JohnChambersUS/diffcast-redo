package us.johnchambers.podcast.fragment.subscribe

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.row_subscribe_episode.view.*
import org.greenrobot.eventbus.EventBus
import org.jetbrains.anko.sdk27.coroutines.onClick
import us.johnchambers.podcast.R
import us.johnchambers.podcast.event.EventOpenPlayer
import us.johnchambers.podcast.misc.*
import us.johnchambers.podcast.objects.docket.DocketSample
import java.util.*
import java.text.DateFormatSymbols

class SubscribeFragmentEpisodesAdapter(val context: Context, val items: List<SubscribeEpisodesInfo>, val logoUrl: String,
                                       val podcastName: String) :
                                        RecyclerView.Adapter<SubscribeFragmentEpisodesAdapter.Holder>() {

    var tapGuard = TapGuard(GC.tapGuard.shortTime)

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubscribeFragmentEpisodesAdapter.Holder {
        return Holder(LayoutInflater.from(context).inflate(R.layout.row_subscribe_episode, parent, false))
    }

    override fun onBindViewHolder(holder: SubscribeFragmentEpisodesAdapter.Holder, position: Int) {
        if (position == 0) {
            if (holder.graphicHelper.isBadImageLink(logoUrl)) {
                var art = holder.graphicHelper.getBigPodcastImage(podcastName)
                holder.headerImage.setImageBitmap(art)
            } else {
                Glide.with(context)
                    .applyDefaultRequestOptions(
                        RequestOptions()
                            .error(R.drawable.unable_to_get_image)
                    )
                    .load(logoUrl)
                    .into(holder.headerImage)
            }
            holder.headerImage.visibility = View.VISIBLE
            holder.guideline.setBeginWithPercentage(0.5f)
        }
        else {
            holder.headerImage.visibility = View.GONE
            holder.guideline.setGuidelineBegin(0)
        }

        holder.name.text = items.get(position).name
        holder.preview.setColorFilter(holder.name.currentTextColor) //set the icon color to light or dark depending on theme
        holder.preview.onClick {
            if (!tapGuard.tooSoon()) {
                EventBus.getDefault().post(EventOpenPlayer(DocketSample(items.get(position), podcastName)))
            }
        }
        ColorHelper.setSubscribePlayButtonIcon(holder.preview)
        var now = Calendar.getInstance()
        var date = Calendar.getInstance()
        date.time = items.get(position).date
        if (now.get(Calendar.YEAR) == date.get(Calendar.YEAR)) {
            try {
                var value = DateFormatSymbols().shortWeekdays[date.get(Calendar.DAY_OF_WEEK)]
                    .append(", ")
                    .append(DateFormatSymbols().shortMonths[date.get(Calendar.MONTH)])
                    .append(" ")
                    .append(date.get(Calendar.DAY_OF_MONTH).toString())
                holder.date.text = value
            } catch (e: Exception) { }
        }
        else {
            try {
                var value = DateFormatSymbols().shortMonths[date.get(Calendar.MONTH)]
                    .append(" ")
                    .append(date.get(Calendar.DAY_OF_MONTH).toString())
                    .append(", ")
                    .append(date.get(Calendar.YEAR).toString())
                holder.date.text = value
            } catch (e: Exception) { }
        }
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        val name = view.row_subscribe_name
        var preview = view.row_subscribe_preview_image
        var date = view.row_subscribe_date
        var headerImage = view.header_image_x
        var guideline = view.episode_bottom_of_header_guideline
        val graphicHelper = GraphicsHelper()
    }


}