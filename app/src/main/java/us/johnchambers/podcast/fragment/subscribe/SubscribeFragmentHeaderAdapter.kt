package us.johnchambers.podcast.fragment.subscribe

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.row_subscribe_header.view.*
import us.johnchambers.podcast.R

class SubscribeFragmentHeaderAdapter(val context: Context, val items: List<SubscribeHeaderInfo>) :
    RecyclerView.Adapter<SubscribeFragmentHeaderAdapter.Holder>() {

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubscribeFragmentHeaderAdapter.Holder {
       return Holder(LayoutInflater.from(context).inflate(R.layout.row_subscribe_header, parent, false))
    }

    override fun onBindViewHolder(holder: SubscribeFragmentHeaderAdapter.Holder, position: Int) {
        if (position == 0) {
            Glide.with(context)
                .applyDefaultRequestOptions(RequestOptions()
                        .error(R.drawable.unable_to_get_image))
                .load(items.get(position).logoUrl)
                .into(holder.image)
            holder.image.visibility = View.VISIBLE
            holder.title.visibility = View.GONE
            holder.desc.visibility = View.GONE
        }
        else {
            holder.title.text = items.get(position).title
            holder.desc.text = items.get(position).desc
            holder.image.visibility = View.GONE
            holder.title.visibility = View.VISIBLE
            holder.desc.visibility = View.VISIBLE
        }
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        val image = view.header_image
        val title = view.title_textview
        val desc = view.desc_textview
    }

}
