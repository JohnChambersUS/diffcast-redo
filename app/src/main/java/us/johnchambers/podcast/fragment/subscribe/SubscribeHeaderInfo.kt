package us.johnchambers.podcast.fragment.subscribe

class SubscribeHeaderInfo() {
    var title: String = ""
    var desc: String = ""
    var logoUrl: String = ""
}