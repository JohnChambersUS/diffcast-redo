package us.johnchambers.podcast.fragment.subscribe

import android.content.Context
import android.content.DialogInterface
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.row_subscribe_fragment_menu.view.*
import org.greenrobot.eventbus.EventBus
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk27.coroutines.onClick
import us.johnchambers.podcast.R
import us.johnchambers.podcast.database.DBHelper
import us.johnchambers.podcast.event.*
import us.johnchambers.podcast.misc.*
import us.johnchambers.podcast.objects.PodcastFeedWrapper

class SubscribeMenuAdapter(val context: Context, val items: List<Any>) :
    RecyclerView.Adapter<SubscribeMenuAdapter.Holder>() {

    var tapGuard = TapGuard(GC.tapGuard.shortTime)

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(LayoutInflater.from(context).inflate(R.layout.row_subscribe_fragment_menu, parent, false))
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        var width = getWidth()
        holder.row.maxWidth = width
        holder.row.minWidth = width
        when (position) {
            0 -> {
                    holder.image.setImageResource(R.drawable.ic_add_white_24dp)
                    holder.desc.text = "SUBSCRIBE"
                    holder.image.onClick {
                    if (!tapGuard.tooSoon()) {
                        subscribe()
                    }
                }
            }
            1 -> {
                    holder.image.setImageResource(R.drawable.ic_menu_info_white_24dp)
                    holder.desc.text = "INFO"
                    //todo update when tags added
                    holder.image.onClick {
                        if (!tapGuard.tooSoon()) {
                            var feed = items.get(0) as PodcastFeedWrapper
                            context.alert(feed.getDesc(), feed.getTitle()) {positiveButton("Continue") {} }.show()
                    }
                }
            }
        }
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        val image = view.subscribe_image
        val desc = view.subscribe_description
        val row = view.row_subscribe_fragment_menu

    }

    private fun getWidth() : Int {
        var displayMetrics = DisplayMetrics()
        context.windowManager.defaultDisplay.getMetrics(displayMetrics)
        var width = displayMetrics.widthPixels
        var returnWidth = (width / 2).toInt()
        return returnWidth
    }

    private fun subscribe() {
        var feed = items.get(0) as PodcastFeedWrapper
        var pid = feed.pid
        var subscribed = DBHelper.podcastExists(pid)
        if (subscribed) {
            context.alert("You are already subscribed to " + feed.getTitle(), "Unable to Subscribe") {positiveButton("Continue") {}}.show()
        } else {
            displaySubscribeModal()
        }
    }

    private fun displaySubscribeModal() {
        //open modal
        var choice = 0
        var builder = androidx.appcompat.app.AlertDialog.Builder(context)
        var choices = arrayOf("Podcast", "Book")
        builder.setTitle("Chose a Listening Mode")

        builder.setSingleChoiceItems(choices, choice, object: DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface, whichButton: Int) {
                choice = whichButton
            }
        })

        builder.setNegativeButton("Cancel", DialogInterface.OnClickListener {
                dialog, id -> dialog.cancel()
        })

        builder.setPositiveButton("OK", DialogInterface.OnClickListener { dialog, id ->
            //todo fill in for visibility
            //subscribe_fab.visibility = View.GONE
            var mode = ""
            var su = SubscriptionUpdater()
            when (choice) {
                0 -> mode = GC.playbackModes.PODCAST
                1 -> mode = GC.playbackModes.BOOK
                else -> mode = GC.playbackModes.PODCAST
            }

            if (mode.equals(GC.playbackModes.BOOK)) {
                context.alert("Is the first episode at the top?") {
                    positiveButton("Yes") {
                        var rv = su.add(items.get(0) as PodcastFeedWrapper, true) //this is the add
                        //EventBus.getDefault().post(EventToast(rv))
                    }
                    negativeButton("No") {
                        var rv = su.add(items.get(0) as PodcastFeedWrapper, false) //this is the add
                        //EventBus.getDefault().post(EventToast(rv))
                    }
                }.show()
            } else {
                var rv = su.add(items.get(0) as PodcastFeedWrapper, mode) //this is the add
                //EventBus.getDefault().post(EventToast(rv))
            }
        })
        var dialog = builder.create()
        dialog.show()
    }

}