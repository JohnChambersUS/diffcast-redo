package us.johnchambers.podcast.fragment.tag.tag_details


import android.content.Context
import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_tag_details.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import us.johnchambers.podcast.R
import us.johnchambers.podcast.database.DBHelper
import us.johnchambers.podcast.database.table_definitions.EpisodeRow
import us.johnchambers.podcast.event.*
import us.johnchambers.podcast.fragment.BaseFragment
import us.johnchambers.podcast.misc.GC
import us.johnchambers.podcast.objects.docket.DocketTag

class TagDetailFragment : BaseFragment() {

    override val identity = typeOfTagDetail
    override val fragmentType = GC.fragmentType.branch
    var tagName = ""
    lateinit var episodeList: MutableList<EpisodeRow>
    var tagFiltered = true
    lateinit var bodyView : RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_tag_details, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        EventBus.getDefault().register(this)
        fillBottomMenu()
        fillEpisodeRows()
    }

    companion object {
        @JvmStatic
        fun newInstance(tag: String) =
            TagDetailFragment().apply {
                arguments = Bundle().apply {
                    //EventBus.getDefault().post(EventToast("newInstance"))
                    tagName = tag
                }
            }
    }

    private fun fillEpisodeRows() {
        if (tagName.equals("")) return
        doAsync {
            //EventBus.getDefault().post(EventToast("async"))
            if (tagFiltered) {
                episodeList = DBHelper.getTopPodcastEpisodesByTag(tagName)
            } else {
                episodeList = DBHelper.getPodcastEpisodesByTag(tagName)
            }
            uiThread {
                //create what's new message

                if (tagName.equals(GC.tags.WHATS_NEW_TEXT)) {
                    tagDetailsEmptyMessage.visibility = View.GONE
                    tagDetailsNoPodcastsMessage.text = tagDetailsNoPodcastsMessage.text.toString() + tagName
                    if (episodeList.size == 0) {
                        tagDetailsNoPodcastsMessage.visibility = View.VISIBLE
                    } else {
                        tagDetailsNoPodcastsMessage.visibility = View.GONE
                    }
                } else {
                    tagDetailsNoPodcastsMessage.visibility = View.GONE
                    tagDetailsEmptyMessage.text = tagDetailsEmptyMessage.text.toString() + tagName
                    if (episodeList.size == 0) {
                        tagDetailsEmptyMessage.visibility = View.VISIBLE
                        //EventBus.getDefault().post(EventToast("onex"))
                    } else {
                        tagDetailsEmptyMessage.visibility = View.GONE
                    }
                }
                //fill adapter
                while (tag_details_episodes_recycler_view == null) {
                    pauseOneSecond()
                }
                tag_details_episodes_recycler_view.layoutManager =
                    LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
                tag_details_episodes_recycler_view.setHasFixedSize(true)
                tag_details_episodes_recycler_view.addItemDecoration(
                    DividerItemDecoration(
                        activity,
                        DividerItemDecoration.VERTICAL
                    )
                )
                tag_details_episodes_recycler_view.adapter =
                    TagDetailsFragmentBodyAdapter(activity as Context, episodeList, tagName)
            }
        }
    }

    private fun refreshBodyRows() {
        episodeList.clear()

        if (tagFiltered) {
            episodeList.addAll(DBHelper.getTopPodcastEpisodesByTag(tagName))
        } else {
            episodeList.addAll(DBHelper.getPodcastEpisodesByTag(tagName))
        }

        waitForViewsToCreate()

        if (tagName.equals(GC.tags.WHATS_NEW_TEXT)) {
            tagDetailsEmptyMessage.visibility = View.GONE
            if (episodeList.size == 0) {
                tagDetailsNoPodcastsMessage.visibility = View.VISIBLE
            } else {
                tagDetailsNoPodcastsMessage.visibility = View.GONE
            }
        } else {
            tagDetailsNoPodcastsMessage.visibility = View.GONE
            if (episodeList.size == 0) {
                tagDetailsEmptyMessage.visibility = View.VISIBLE
                //EventBus.getDefault().post(EventToast("twox"))
            } else {
                tagDetailsEmptyMessage.visibility = View.GONE
            }
        }

        tag_details_episodes_recycler_view.adapter?.notifyDataSetChanged()
    }

    private fun fillBottomMenu() {
        var menuList = mutableListOf(tagName, "one", "two", "three", "four")
        if (!tagName.equals(GC.tags.WHATS_NEW_TEXT)) {
            menuList.add("five")
            menuList.add("six")
        }
        //fill adapter
        waitForViewsToCreate()

        tag_details_menu_recycler_view.layoutManager = LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false)
        tag_details_menu_recycler_view.setHasFixedSize(true)
        ////my_tags_recycler_view.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        tag_details_menu_recycler_view.adapter = TagDetailsFragmentMenuAdapter(activity as Context, menuList)
        tag_details_menu_recycler_view.addOnScrollListener(object: RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                try {
                    if (!tag_details_menu_recycler_view.canScrollHorizontally(1)) {
                        tag_details_right_arrow.visibility = View.GONE
                    } else {
                        tag_details_right_arrow.visibility = View.VISIBLE
                    }
                    if (!tag_details_menu_recycler_view.canScrollHorizontally(-1)) {
                        tag_details_left_arrow.visibility = View.GONE
                    } else {
                        tag_details_left_arrow.visibility = View.VISIBLE
                    }
                } catch(e: Exception) {}

            }
        })
    }

    private fun waitForViewsToCreate() {
        while (tag_details_menu_recycler_view == null) {
            pauseOneSecond()
        }
        while (tagDetailsEmptyMessage == null) {
            pauseOneSecond()
        }
        while (tag_details_episodes_recycler_view == null) {
            pauseOneSecond()
        }
    }

    private fun pauseOneSecond() {
        EventBus.getDefault().post("pause one second tag detail")
        val timer = object: CountDownTimer(100, 100) {
            override fun onTick(millisUntilFinished: Long) {}
            override fun onFinish() {}
        }
        timer.start()
    }

    @Subscribe
    fun onEvent(event: EventUpdateTagDetailsBody) {
        if (event.toggle)
            tagFiltered = event.filter
        refreshBodyRows()
    }

    @Subscribe
    fun onEvent(event: EventPlayTag) {
        var d: DocketTag
        if (event.positon < 0) {
            d = DocketTag(episodeList, episodeList.size - 1, tagName)
        } else {
            d = DocketTag(episodeList, event.positon, tagName)
        }
        EventBus.getDefault().post(EventOpenPlayer(d))
    }

    @Subscribe
    fun onEvent(event: EventPlayerActivityClosed) {
    }

    @Subscribe
    fun onEvent(event: EventUpdateRowInDisplay) {
        refreshBodyRows()
    }

    @Subscribe
    fun onEvent(event: EventScrollTagDetailFragment) {
        if (event.bottom == true) {
            tag_details_episodes_recycler_view.scrollToPosition(episodeList.size - 1)
        } else {
            tag_details_episodes_recycler_view.scrollToPosition(0)
        }
    }

    @Subscribe
    fun onEvent(event: EventMoveTagToManual) {
        var manualQueue = event.manualQueueName
        for (episode in episodeList) {
            DBHelper.addManualPlaylistRow(episode.eid, manualQueue)
        }
        if (episodeList.size > 0) {
            EventBus.getDefault().post(EventToast("All episodes have been added to: " + manualQueue))
        } else {
            EventBus.getDefault().post(EventToast("This tag list is empty, nothng to add to: " + manualQueue))
        }
    }



}
