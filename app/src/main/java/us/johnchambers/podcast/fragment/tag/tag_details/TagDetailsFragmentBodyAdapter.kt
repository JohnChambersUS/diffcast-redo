package us.johnchambers.podcast.fragment.tag.tag_details

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.LayerDrawable
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.row_tag_details_body.view.*
import org.greenrobot.eventbus.EventBus
import org.jetbrains.anko.sdk27.coroutines.onClick
import us.johnchambers.podcast.R
import us.johnchambers.podcast.database.DBHelper
import us.johnchambers.podcast.database.table_definitions.EpisodeRow
import com.bumptech.glide.request.target.CustomTarget
import org.jetbrains.anko.*
import us.johnchambers.podcast.event.EventPlayTag
import us.johnchambers.podcast.misc.*

class TagDetailsFragmentBodyAdapter(val context: Context, val items: MutableList<EpisodeRow>, val tagName: String) :
    RecyclerView.Adapter<TagDetailsFragmentBodyAdapter.Holder>() {

    var tapGuard = TapGuard(GC.tapGuard.shortTime)
    var fetchedImages = mutableMapOf<String, Bitmap>()
    var fetches = 0

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(LayoutInflater.from(context).inflate(R.layout.row_tag_details_body, parent, false))
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        if (position < 1) {
            holder.header.visibility = View.VISIBLE
            holder.header.text = tagName
        } else {
            holder.header.visibility = View.GONE
        }

        holder.name.text = items[position].title
        holder.publishDate.text = items[position].publication_date.formatPodcastDate()
        ColorHelper.setSearchIcon(holder.button)
        var podcast = DBHelper.getPodcastRowByPid(items.get(position).pid)

        if (holder.graphicHelper.isBadImageLink(podcast.logo_url)) {
            var art = holder.graphicHelper.getSmallPodcastImage(podcast.name)
            holder.image.setImageBitmap(art)
        } else {
            if (fetchedImages.containsKey(podcast.logo_url)) {
                holder.image.setImageBitmap(fetchedImages.get(podcast.logo_url))
            } else {
                Glide.with(context)
                    .asBitmap()
                    .error(R.drawable.unable_to_get_image)
                    .load(podcast.logo_url)
                    .into(object : CustomTarget<Bitmap>() {
                        override fun onResourceReady(
                            resource: Bitmap,
                            transition: com.bumptech.glide.request.transition.Transition<in Bitmap>?
                        ) {
                            holder.image.setImageBitmap(resource)
                            fetchedImages.set(podcast.logo_url, resource)
                        }

                        override fun onLoadCleared(placeholder: Drawable?) {
                        }
                    })
            }
        }

        holder.row.onClick {
        }

        holder.button.onClick {
            var actions = listOf("Play this episode", "Skip episode", "Reset to beginning") //removed mark as complete
            val skipped = items.get(position).isSkipped()
            if (skipped) {
                actions = listOf("Play this episode", "Unskip episode", "Reset to beginning")
            }
            context.selector("", actions, { dialogInterface, choice ->
                when(choice) {
                    0 -> {
                        items.get(position).unskipEpisode()
                        if (items.get(position).isComplete()) items.get(position).setToBeginning()
                        DBHelper.insertEpisodeRow(items.get(position))
                        EventBus.getDefault().post(EventPlayTag(position))
                    }
                    1 -> {
                        if (skipped) {
                            items.get(position).unskipEpisode()
                        } else {
                            items.get(position).skipEpisode()
                        }
                        doAsync {
                            DBHelper.insertEpisodeRow(items.get(position))
                            uiThread {
                                setProgress(holder.row, items.get(position))
                            }
                        }
                    }
                    2 -> {
                        doAsync {
                            items.get(position).setToBeginning()
                            DBHelper.insertEpisodeRow(items.get(position))
                            uiThread {
                                setProgress(holder.row, items.get(position))
                            }
                        }
                    }
                    3 -> {
                        doAsync {
                            items.get(position).markAsPlayed()
                            DBHelper.insertEpisodeRow(items.get(position))
                            uiThread {
                                setProgress(holder.row, items.get(position))
                            }
                        }
                    }
                }
            })
        }


        setProgress(holder.row, items.get(position))
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        val header = view.row_tag_detail_tag_name_header
        val image = view.row_tag_detail_logo_image
        val publishDate =  view.row_tag_detail_publish_date
        val name = view.row_tag_detail_episode_name
        var button = view.row_tag_detail_menu_image
        val row = view
        val graphicHelper = GraphicsHelper()
    }

    private fun setProgress(view: View, episode: EpisodeRow) {
        var left = GradientDrawable()
        left.setShape(GradientDrawable.RECTANGLE);

        var v = TypedValue()
        if (episode.isSkipped()) {
            v.data = ColorHelper.getSkippedDarkColor()
        } else {
            v.data = ColorHelper.getProgressColor()
        }
        var cid = v.data
        left.setColor(cid)

        var right = GradientDrawable()
        right.setShape(GradientDrawable.RECTANGLE);

        var v2 = TypedValue()
        if (episode.isSkipped()) {
            v2.data = ColorHelper.getSkippedLightColor()
        } else {
            context.theme.resolveAttribute(android.R.attr.windowBackground, v2, true)
        }

        var cid2 = v2.data
        right.setColor(cid2);

        var ar = arrayOf(left, right)
        var layer = LayerDrawable(ar)

        var displayMetrics = DisplayMetrics()
        context.windowManager.defaultDisplay.getMetrics(displayMetrics)
        var width = displayMetrics.widthPixels

        var twentyWidth = (width * .2).toInt()
        width = width - twentyWidth

        var playPoint = episode.getPlayPointAsLong().toFloat()
        var length = episode.getLengthAsLong().toFloat()

        var ratio = 0f
        if (playPoint >= 0f) {
            if (playPoint >= length) {
                ratio = 1f
            } else {
                ratio = playPoint / length
            }
        }
        var size = Math.round(width * ratio).toInt()
        size = size + twentyWidth
        layer.setLayerInset(1, size, 0, 0, 0);
        view.setBackground(layer);

    } //end of setProgress



}