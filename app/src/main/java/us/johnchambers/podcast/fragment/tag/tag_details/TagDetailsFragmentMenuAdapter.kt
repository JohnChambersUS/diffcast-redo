package us.johnchambers.podcast.fragment.tag.tag_details

import android.app.AlertDialog
import android.content.Context
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.row_tag_details_fragment_menu.view.*
import org.greenrobot.eventbus.EventBus
import org.jetbrains.anko.alert
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.windowManager
import us.johnchambers.podcast.R
import us.johnchambers.podcast.database.DBHelper
import us.johnchambers.podcast.event.*
import us.johnchambers.podcast.misc.GC
import us.johnchambers.podcast.misc.TapGuard


class TagDetailsFragmentMenuAdapter(val context: Context, val items: List<Any>) :
    RecyclerView.Adapter<TagDetailsFragmentMenuAdapter.Holder>() {

    var tapGuard = TapGuard(GC.tapGuard.shortTime)

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(LayoutInflater.from(context).inflate(R.layout.row_tag_details_fragment_menu, parent, false))
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        var width = getWidth()
        holder.row.maxWidth = width
        holder.row.minWidth = width

        when (position) {
            0 -> {
                holder.image.setImageResource(R.drawable.ic_menu_play_arrow_white_24dp)
                holder.desc.text = "PLAY"
                holder.v.onClick {
                    if (!tapGuard.tooSoon()) {
                        EventBus.getDefault().post(EventPlayTag())
                    }
                }
            }
            1 -> {
                holder.desc.text = "UNFILTER"
                holder.image.setImageResource(R.drawable.ic_unfilter_24dp)
                holder.v.onClick {
                    if (!tapGuard.tooSoon()) {
                        if (holder.desc.text.equals("UNFILTER")) {
                            holder.desc.text = "FILTER"
                            holder.image.setImageResource(R.drawable.ic_filter_24dp)
                            EventBus.getDefault().post(EventUpdateTagDetailsBody(true, false))
                        } else {
                            holder.desc.text = "UNFILTER"
                            holder.image.setImageResource(R.drawable.ic_unfilter_24dp)
                            EventBus.getDefault().post(EventUpdateTagDetailsBody(true, true))
                        }
                    }
                }
            }
            2 -> {
                holder.image.setImageResource(R.drawable.ic_menu_bottom_white_24dp)
                holder.desc.text = "BOTTOM"
                holder.v.onClick {
                    if (!tapGuard.tooSoon()) {
                        if (items.size > 0) {
                            EventBus.getDefault().post(EventScrollTagDetailFragment(bottom = true))
                        }
                    }
                }
            }
            3 -> {
                holder.image.setImageResource(R.drawable.ic_menu_top_white_24dp)
                holder.desc.text = "TOP"
                holder.v.onClick {
                    if (!tapGuard.tooSoon()) {
                        if (items.size > 0) {
                            EventBus.getDefault().post(EventScrollTagDetailFragment(bottom = false))
                        }
                    }
                }
            }
            4 -> {
                holder.image.setImageResource(R.drawable.ic_hand_white)
                holder.desc.text = "MANUAL"
                holder.v.onClick {
                    if (!tapGuard.tooSoon()) {
                        val array = arrayOf("One","Two","Three","Four","Five","Six","Seven","Eight","Nine","Ten")
                        val builder = AlertDialog.Builder(context)
                        builder.setTitle("Add to which manual queue?")
                        builder.setItems(array,{_, which ->
                            val selected = array[which]
                            when(which) {
                                0 -> EventBus.getDefault().post(EventMoveTagToManual(GC.playlist.manual.MANUAL_QUEUE_1))
                                1 -> EventBus.getDefault().post(EventMoveTagToManual(GC.playlist.manual.MANUAL_QUEUE_2))
                                2 -> EventBus.getDefault().post(EventMoveTagToManual(GC.playlist.manual.MANUAL_QUEUE_3))
                                3 -> EventBus.getDefault().post(EventMoveTagToManual(GC.playlist.manual.MANUAL_QUEUE_4))
                                4 -> EventBus.getDefault().post(EventMoveTagToManual(GC.playlist.manual.MANUAL_QUEUE_5))
                                5 -> EventBus.getDefault().post(EventMoveTagToManual(GC.playlist.manual.MANUAL_QUEUE_6))
                                6 -> EventBus.getDefault().post(EventMoveTagToManual(GC.playlist.manual.MANUAL_QUEUE_7))
                                7 -> EventBus.getDefault().post(EventMoveTagToManual(GC.playlist.manual.MANUAL_QUEUE_8))
                                8 -> EventBus.getDefault().post(EventMoveTagToManual(GC.playlist.manual.MANUAL_QUEUE_9))
                                9 -> EventBus.getDefault().post(EventMoveTagToManual(GC.playlist.manual.MANUAL_QUEUE_10))
                                else -> EventBus.getDefault().post(EventMoveTagToManual(GC.playlist.manual.MANUAL_QUEUE_1))
                            }
                        })
                        val dialog = builder.create()
                        dialog.show()
                    }
                }
            }
            5 -> {
                holder.image.setImageResource(R.drawable.ic_edit_white_24dp)
                holder.desc.text = "EDIT"
                holder.v.onClick {
                    if (!tapGuard.tooSoon()) {
                        EventBus.getDefault().post(EventOpenTagSelectionFragment(items.get(0).toString()))
                    }
                }
            }
            6 -> {
                holder.image.setImageResource(R.drawable.ic_menu_remove_white_24dp)
                holder.desc.text = "REMOVE"
                holder.v.onClick {
                    if (!tapGuard.tooSoon()) {
                        context.alert("Are you sure you want to remove this tag?") {
                            positiveButton("Yes") {
                                DBHelper.removeTag(items.get(0).toString())

                                EventBus.getDefault().post(EventCloseTopFragment())
                            }
                            negativeButton("No") {}
                        }.show()
                    }
                }
            }
        }
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        val image = view.tdm_image
        val desc = view.tdm_description
        val row = view.row_tag_fragment_menu
        val v = view

    }

    private fun getWidth() : Int {
        var displayMetrics = DisplayMetrics()
        context.windowManager.defaultDisplay.getMetrics(displayMetrics)
        var width = displayMetrics.widthPixels
        var returnWidth = (width / 3).toInt()
        return returnWidth
    }

}