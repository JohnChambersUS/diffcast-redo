package us.johnchambers.podcast.fragment.tag.tag_list

import android.content.Context
import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_tag.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import us.johnchambers.podcast.R
import us.johnchambers.podcast.database.DBHelper
import us.johnchambers.podcast.event.EventTagListHasBeenUpdated
import us.johnchambers.podcast.fragment.BaseFragment
import us.johnchambers.podcast.misc.GC

class TagFragment : BaseFragment() {

    override val identity = typeOfTag
    override val fragmentType = GC.fragmentType.root
    lateinit var tagList : MutableList<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_tag, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        EventBus.getDefault().register(this)
        fillBottomMenu()
        doAsync {
            tagList = DBHelper.getAllTags()
            if (tagList.size > 0) {
                fillBodyRecyclerView(tagList)
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            TagFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }

    private fun fillBodyRecyclerView(items: List<String>) {
        while (fragment_tag_list_recycler_view == null) {
            pauseOneSecond()
        }
        fragment_tag_list_recycler_view.layoutManager = LinearLayoutManager(activity,
            RecyclerView.VERTICAL,
            false)
        fragment_tag_list_recycler_view.setHasFixedSize(true)
        fragment_tag_list_recycler_view.addItemDecoration(
            DividerItemDecoration(activity,
                DividerItemDecoration.VERTICAL)
        )
        fragment_tag_list_recycler_view.adapter =
            TagFragmentListAdapter(activity as Context, items)
    }


    private fun fillBottomMenu() {
        var menuList = listOf("one")
        while (fragment_tag_menu_recycler_view == null) {
            pauseOneSecond()
        }
        fragment_tag_menu_recycler_view.layoutManager = LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false)
        fragment_tag_menu_recycler_view.setHasFixedSize(true)
        fragment_tag_menu_recycler_view.adapter =
            TagFragmentMenuAdapter(activity as Context, menuList)
    }

    private fun pauseOneSecond() {
        val timer = object: CountDownTimer(100, 100) {
            override fun onTick(millisUntilFinished: Long) {}
            override fun onFinish() {}
        }
        timer.start()
    }

    @Subscribe
    fun onEvent(event: EventTagListHasBeenUpdated) {
        doAsync {
            tagList.clear()
            tagList.addAll(DBHelper.getAllTags())
            uiThread {
                if (tagList.size > 0) {
                    fragment_tag_list_recycler_view.adapter?.notifyDataSetChanged()
                }
            }
        }
    }

}
