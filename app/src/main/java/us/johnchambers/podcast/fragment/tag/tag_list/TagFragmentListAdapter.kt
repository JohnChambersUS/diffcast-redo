package us.johnchambers.podcast.fragment.tag.tag_list

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.row_tag_list.view.*
import org.greenrobot.eventbus.EventBus
import org.jetbrains.anko.sdk27.coroutines.onClick
import us.johnchambers.podcast.R
import us.johnchambers.podcast.event.EventOpenTagDetailFragment
import us.johnchambers.podcast.misc.*

class TagFragmentListAdapter(val context: Context, val items: List<String>) :
    RecyclerView.Adapter<TagFragmentListAdapter.Holder>() {

    var tapGuard = TapGuard(GC.tapGuard.shortTime)

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(LayoutInflater.from(context).inflate(R.layout.row_tag_list, parent, false))
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {

        holder.name.text = items.get(position).toString()
        ColorHelper.setSearchIcon(holder.menu)
        holder.row.onClick {
            if (!tapGuard.tooSoon()) {
                EventBus.getDefault().post(EventOpenTagDetailFragment(items.get(position).toString()))
            }
        }
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        val name = view.row_tag_list_name
        var menu = view.row_tag_list_menu_image
        var row = view
    }


}