package us.johnchambers.podcast.fragment.tag.tag_list

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.row_tag_fragment_menu.view.*
import kotlinx.android.synthetic.main.tag_add_new_tag.view.*
import org.greenrobot.eventbus.EventBus
import org.jetbrains.anko.layoutInflater
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.windowManager
import us.johnchambers.podcast.R
import us.johnchambers.podcast.database.DBHelper
import us.johnchambers.podcast.event.EventTagListHasBeenUpdated
import us.johnchambers.podcast.misc.GC
import us.johnchambers.podcast.misc.TapGuard

class TagFragmentMenuAdapter(val context: Context, val items: List<Any>) :
    RecyclerView.Adapter<TagFragmentMenuAdapter.Holder>() {

    var tapGuard = TapGuard(GC.tapGuard.shortTime)

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(LayoutInflater.from(context).inflate(R.layout.row_tag_fragment_menu, parent, false))
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        var width = getWidth()
        holder.row.maxWidth = width
        holder.row.minWidth = width
        when (position) {
            0 -> {
                holder.image.setImageResource(R.drawable.ic_add_white_24dp)
                holder.desc.text = "ADD A TAG"
                holder.row.onClick {
                    if (!tapGuard.tooSoon()) {
                        displayAddTagDialog()
                    }
                }
            }
        }
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        val image = view.tag_image
        val desc = view.tag_description
        val row = view.row_tag_fragment_menu
    }

    private fun getWidth() : Int {
        var displayMetrics = DisplayMetrics()
        context.windowManager.defaultDisplay.getMetrics(displayMetrics)
        var width = displayMetrics.widthPixels
        var returnWidth = (width / 1).toInt()
        return returnWidth
    }

    private fun displayAddTagDialog() {
        val builder = AlertDialog.Builder(context)
        val inflater = context.layoutInflater
        var v = inflater.inflate(R.layout.tag_add_new_tag, null)
        builder.setView(v)
            .setPositiveButton("done",
                DialogInterface.OnClickListener { dialog, id ->
                   var newTag = v.add_a_tag_edit_text.text
                   if (!newTag.equals("")) {
                       DBHelper.insertTag(newTag.toString())
                       EventBus.getDefault().post(EventTagListHasBeenUpdated())
                   }
                })
            .setNegativeButton("cancel",
                DialogInterface.OnClickListener { dialog, id -> })
        builder.create()
        builder.show()
    }

}