package us.johnchambers.podcast.fragment.tag.tag_selection


import android.content.Context
import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_tag_selection.*
import org.greenrobot.eventbus.EventBus
import us.johnchambers.podcast.R
import us.johnchambers.podcast.database.DBHelper
import us.johnchambers.podcast.database.objects.SearchPodcastTag
import us.johnchambers.podcast.event.EventUpdateTagDetailsBody
import us.johnchambers.podcast.fragment.BaseFragment
import us.johnchambers.podcast.misc.GC

class TagSelectionFragment : BaseFragment() {

    override val identity =  typeOfTagSelection
    override val fragmentType = GC.fragmentType.branch
    private lateinit var podcastList: MutableList<SearchPodcastTag>
    private lateinit var tagId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_tag_selection, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        fillBodyRows()
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().post(EventUpdateTagDetailsBody(false))
    }

    companion object {
        @JvmStatic
        fun newInstance(tagName: String) =
            TagSelectionFragment().apply {
                arguments = Bundle().apply {
                    tagId = tagName
                }
            }
    }

    private fun fillBodyRows() {
        while (tag_selection_recycler_view == null) {
            pauseOneSecond()
        }
        podcastList = DBHelper.getPodcastsWithTag2(tagId)

        if (podcastList.size < 1) {
            tag_selection_empty_message.visibility = View.VISIBLE
        } else {
            tag_selection_empty_message.visibility = View.GONE
        }

        tag_selection_recycler_view.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        tag_selection_recycler_view.setHasFixedSize(true)
        tag_selection_recycler_view.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        tag_selection_recycler_view.adapter = TagSelectionFragmentAdapter(activity as Context, podcastList, tagId)
    }

    private fun pauseOneSecond() {
        val timer = object: CountDownTimer(100, 100) {
            override fun onTick(millisUntilFinished: Long) {}
            override fun onFinish() {}
        }
        timer.start()
    }

}
