package us.johnchambers.podcast.fragment.tag.tag_selection

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.row_tag_selection_list.view.*
import org.jetbrains.anko.sdk27.coroutines.onClick
import us.johnchambers.podcast.R
import us.johnchambers.podcast.database.DBHelper
import us.johnchambers.podcast.database.objects.SearchPodcastTag
import us.johnchambers.podcast.database.table_definitions.PodcastTagRow
import us.johnchambers.podcast.misc.ColorHelper
import us.johnchambers.podcast.misc.GC
import us.johnchambers.podcast.misc.GraphicsHelper
import us.johnchambers.podcast.misc.TapGuard

class TagSelectionFragmentAdapter(val context: Context, val items: MutableList<SearchPodcastTag>, val tagName: String) :
    RecyclerView.Adapter<TagSelectionFragmentAdapter.Holder>() {

    var tapGuard = TapGuard(GC.tapGuard.shortTime)

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(LayoutInflater.from(context).inflate(R.layout.row_tag_selection_list, parent, false))
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {

        toggleBackgroundColor(holder, items.get(position))
        holder.name.text = items.get(position).name.toString()
        ColorHelper.setRowButtonColor(holder.action)

        if (holder.graphicHelper.isBadImageLink(items.get(position).logo_url)) {
            var art = holder.graphicHelper.getSmallPodcastImage(items.get(position).name)
            holder.image.setImageBitmap(art)
        } else {
            Glide.with(context)
                .applyDefaultRequestOptions(
                    RequestOptions()
                        .error(R.drawable.unable_to_get_image)
                )
                .load(items.get(position).logo_url)
                .into(holder.image)
        }
        holder.image.onClick {
            if (!tapGuard.tooSoon()) {
                toggleDB(position)
                toggleBackgroundColor(holder, items.get(position))
            }
        }
        holder.name.onClick {
            if (!tapGuard.tooSoon()) {
                toggleDB(position)
                toggleBackgroundColor(holder, items.get(position))
            }
        }
        holder.action.onClick {
            if (!tapGuard.tooSoon()) {
                toggleDB(position)
                toggleBackgroundColor(holder, items.get(position))
            }
        }
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        var image = view.row_tag_selection_image
        val name = view.row_tag_selection_name
        var action = view.row_tag_selection_action_button
        var row = view
        private val background = view.background as ColorDrawable
        var backgroundColor = background.color
        val graphicHelper = GraphicsHelper()
    }

    private fun toggleBackgroundColor(holder: Holder, current: SearchPodcastTag) {

        if (current.tag == null) {
            holder.action.setImageDrawable(context.getDrawable(R.drawable.ic_star_border_black_24dp))
        } else {
            holder.action.setImageDrawable(context.getDrawable(R.drawable.ic_star_black_24dp))
        }
    }

    private fun toggleDB(position: Int) {
        var i = items.get(position)
        if (items.get(position).tag == null) {
            i.tag = tagName
            items.set(position, i)
            var ptr = PodcastTagRow(items.get(position).pid, tagName)
            DBHelper.addPodcastTagRow(ptr)
        } else {
            i.tag = null
            items.set(position, i)
            var ptr = PodcastTagRow(items.get(position).pid, tagName)
            DBHelper.removePodcastTagRow(ptr)
        }
    }

}