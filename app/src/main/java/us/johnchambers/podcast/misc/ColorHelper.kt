package us.johnchambers.podcast.misc

import android.content.Context
import android.graphics.Color
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import kotlinx.android.synthetic.main.activity_main.view.*
import org.jetbrains.anko.image
import us.johnchambers.podcast.R
import us.johnchambers.podcast.database.DBHelper
import us.johnchambers.podcast.database.table_definitions.OptionsGlobalRow

object ColorHelper {

    val greenDark = "forest_style_dark"
    val greenLight = "forest_style_light"

    val redDark = "red_style_dark"
    val redLight = "red_style_light"

    val blueDark = "blue_style_dark"
    val blueLight = "blue_style_light"

    val sunsetDark = "sunset_style_dark"
    val sunsetLight = "sunset_style_light"

    var currTheme = ""
    //var currTheme = greenDark
    //var currTheme = greenLight

    val greenDarkMap = mutableMapOf("skippedDarkColor" to Color.parseColor("#4F4F4F"),
                        "skippedLightColor" to Color.parseColor("#4F4F4F"),
                        "progressColor" to Color.parseColor("#889729"))

    val greenLightMap = mutableMapOf("skippedDarkColor" to Color.parseColor("#cdcdcd"),
                        "skippedLightColor" to Color.parseColor("#cdcdcd"),
                        "progressColor" to Color.parseColor("#E8F0BE"))

    val redDarkMap = mutableMapOf("skippedDarkColor" to Color.parseColor("#4F4F4F"),
                                    "skippedLightColor" to Color.parseColor("#4F4F4F"),
                                    "progressColor" to Color.parseColor("#8E3F27"))

    val redLightMap = mutableMapOf("skippedDarkColor" to Color.parseColor("#cdcdcd"),
                                    "skippedLightColor" to Color.parseColor("#cdcdcd"),
                                    "progressColor" to Color.parseColor("#EFCABA"))

    val blueDarkMap = mutableMapOf("skippedDarkColor" to Color.parseColor("#4F4F4F"),
        "skippedLightColor" to Color.parseColor("#4F4F4F"),
        "progressColor" to Color.parseColor("#3E6B89"))

    val blueLightMap = mutableMapOf("skippedDarkColor" to Color.parseColor("#cdcdcd"),
        "skippedLightColor" to Color.parseColor("#cdcdcd"),
        "progressColor" to Color.parseColor("#C5DBE4"))

    val sunsetDarkMap = mutableMapOf("skippedDarkColor" to Color.parseColor("#4F4F4F"),
        "skippedLightColor" to Color.parseColor("#4F4F4F"),
        "progressColor" to Color.parseColor("#784913"))

    val sunsetLightMap = mutableMapOf("skippedDarkColor" to Color.parseColor("#cdcdcd"),
        "skippedLightColor" to Color.parseColor("#cdcdcd"),
        "progressColor" to Color.parseColor("#F6DDA0"))

    var colorMap = mutableMapOf<String, Int>()

    init {
        getThemeName()
        when(currTheme) {
            greenDark -> colorMap = greenDarkMap
            greenLight -> colorMap = greenLightMap
            redDark -> colorMap = redDarkMap
            redLight -> colorMap = redLightMap
            blueDark -> colorMap = blueDarkMap
            blueLight -> colorMap = blueLightMap
            sunsetDark -> colorMap = sunsetDarkMap
            sunsetLight -> colorMap = sunsetLightMap
            else -> colorMap = greenLightMap
        }
    }

    fun getThemeName() : String {
        if (currTheme.length < 1) {
            currTheme = DBHelper.getCurrentTheme()
            if (currTheme == null) {
                    currTheme = greenLight
            }
            if (currTheme.equals("")) {
                currTheme = greenLight
            }
        }
        return currTheme
    }

    fun getCurrentTheme(): Int {
        when(getThemeName()) {
            greenDark -> return R.style.forest_style_dark
            greenLight -> return R.style.forest_style_light
            redDark -> return R.style.red_style_dark
            redLight -> return R.style.red_style_light
            blueDark -> return R.style.blue_style_dark
            blueLight -> return R.style.blue_style_light
            sunsetDark -> return R.style.sunset_style_dark
            sunsetLight -> return R.style.sunset_style_light
            else -> return R.style.forest_style_light
        }
    }

    private fun isThemeDark(): Boolean {
        if (currTheme.contains("Dark", ignoreCase = true)) {
            return true
        } else {
            return false
        }
    }

    fun setSearchButtonIcon(button: ImageButton, context: Context) {
        button.setImageDrawable(getDrawable(context, R.drawable.ic_search_button_light_background))
        button.alpha = 0.50f
    }

    fun setSubscribePlayButtonIcon(button: ImageView) {
        setAlphaOnView(button)
    }

    fun setSearchIcon(button: ImageView) {
        setAlphaOnView(button)
    }

    private fun setAlphaOnView(button: ImageView) {
        if (isThemeDark()) {
            button.alpha = 1.0f
        } else {
            button.alpha = 0.50f
        }
    }

    fun setRowButtonColor(button: ImageView) {
        if (isThemeDark()) {
            var newColor = Color.WHITE
            button.setColorFilter(newColor)
            button.alpha = 0.5f
        } else {
            var newColor = Color.BLACK
            button.setColorFilter(newColor)
            button.alpha = 0.5f
        }
    }


    fun getSkippedDarkColor() : Int {
        return colorMap.get("skippedDarkColor")!!
    }

    fun getSkippedLightColor() : Int {
        return colorMap.get("skippedLightColor")!!
    }

    fun getProgressColor(): Int {
        return colorMap.get("progressColor")!!
    }

    fun setAppBackground(v: View, context: Context) {

        when(getThemeName()) {
            greenDark -> v.main_background_image.image = getDrawable(context, R.drawable.background_main_activity)
            greenLight -> v.main_background_image.image = getDrawable(context, R.drawable.background_main_activity)
            redDark -> v.main_background_image.image = getDrawable(context, R.drawable.background_main_activity)
            redLight -> v.main_background_image.image = getDrawable(context, R.drawable.background_main_activity)
            blueDark -> v.main_background_image.image = getDrawable(context, R.drawable.background_main_activity)
            blueLight -> v.main_background_image.image = getDrawable(context, R.drawable.background_main_activity)
            sunsetDark -> v.main_background_image.image = getDrawable(context, R.drawable.background_main_activity)
            sunsetLight -> v.main_background_image.image = getDrawable(context, R.drawable.background_main_activity)
            else -> v.main_background_image.image = getDrawable(context, R.drawable.background_main_activity)
        }
    }

    fun updateColor(color:String) {
        currTheme = color
        var row = OptionsGlobalRow()
        row.k = GC.theme.dbKey
        row.v = color
        DBHelper.upsertGlobalOptionsRow(row)
    }

    fun setSearchRadioBackground(v: View) {
        if (isThemeDark()) {
            v.setBackgroundColor(getSkippedDarkColor())
        } else {
            v.setBackgroundColor(getSkippedLightColor())
        }
    }

}