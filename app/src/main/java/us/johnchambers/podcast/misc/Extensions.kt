package us.johnchambers.podcast.misc

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Point
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.constraintlayout.widget.Guideline
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

fun View.hideKeyboard(inputMethodManager: InputMethodManager) {
    inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
}

fun Int.isZero() : Boolean {
    return if (this < 1) true else false
}

fun JSONObject.hasData(name: String) : Boolean {
    if (!this.has(name)) return false
    var v = this.get(name)
    if ((v is String) && ((v.equals("") || (v == null)))) return false
    if ((v is Int) && (v == null)) return false
    return true
}

fun Bitmap.isNull() : Boolean {
    return (this == null)
}

fun View.setHeight(context: Context, percent: Float) {
    val wm = (context).getSystemService(Context.WINDOW_SERVICE) as WindowManager
    val display = wm.defaultDisplay
    val size = Point()
    display.getRealSize(size)
    var height = (size.y * percent ).toInt()

    val params = this.getLayoutParams()
    params.height = height
    this.setLayoutParams(params)
}

fun Guideline.setBeginWithPercentage(percentage: Float) {

    val wm = (context).getSystemService(Context.WINDOW_SERVICE) as WindowManager
    val display = wm.defaultDisplay
    val size = Point()
    display.getRealSize(size)
    var w = size.x * percentage
    var pct = percentage
    while (((w/size.y)*100) > 30 ) {
        pct = pct - 0.05f
        w = size.x * pct
    }
    this.setGuidelineBegin(w.toInt())
}

fun String.formatPodcastDate() : String {
    try {
        var parts = this.toString().split(" ")
        var yearDay = parts.get(0).split("-")
        var cal = Calendar.getInstance()
        var df = SimpleDateFormat("yyyy")
        var currYear = df.format(Calendar.getInstance().time)

        cal.set(Calendar.YEAR, yearDay.get(0).toInt())
        cal.set(Calendar.DAY_OF_YEAR, yearDay.get(1).toInt())
        var d1 = yearDay.get(1)
        var d2 = d1.toInt()
        cal.set(Calendar.DAY_OF_YEAR, d2)

        var rv = "???"
        if (currYear.equals(yearDay.get(0))) {
            rv = SimpleDateFormat("EEE, MMM d").format(cal.time)
        } else {
            rv = SimpleDateFormat("MMM d, yy").format(cal.time)
        }
        return rv
    } catch (e: Exception) {
        return "???"
    }
}

fun String.append(value: String): String {
    return this + value
}

fun Boolean.asInt() : Int {
    return if (this.equals(false)) 0 else 1
}