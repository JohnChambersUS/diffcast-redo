package us.johnchambers.podcast.misc

import kotlinx.android.synthetic.main.content_main.*
import us.johnchambers.podcast.activity.main.MainActivity
import us.johnchambers.podcast.fragment.BaseFragment
import us.johnchambers.podcast.fragment.search.SearchFragment
import us.johnchambers.podcast.fragment.test.TestFragment
import java.util.*
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import us.johnchambers.podcast.event.*
import us.johnchambers.podcast.fragment.manual.ManualQueueFragment
import us.johnchambers.podcast.fragment.my_podcasts.MyPodcastsFragment
import us.johnchambers.podcast.fragment.podcast_details.PodcastDetailsFragment
import us.johnchambers.podcast.fragment.subscribe.SubscribeFragment
import us.johnchambers.podcast.fragment.tag.tag_details.TagDetailFragment
import us.johnchambers.podcast.fragment.tag.tag_list.TagFragment
import us.johnchambers.podcast.fragment.tag.tag_selection.TagSelectionFragment

class FragMgr(private val activity: MainActivity) {

    var fragmentStack = Stack<BaseFragment>()

    init {
        EventBus.getDefault().register(this)
    }

    //*****************************
    //* public non-open functions
    //*****************************

    fun closeTopFragment() {
        if (fragmentStack.empty()) return
        var frag = fragmentStack.pop()
        activity.supportFragmentManager.beginTransaction().remove(frag).commit();
}

    //**********************************
    //* public open fragment functions
    //**********************************

    fun openSearchFragment() {
        removeLeaf()
        if (!fragmentStack.empty())
            if (fragmentStack.peek().isSearchFragment()) return

        addFragment(SearchFragment.newInstance())
    }

    fun openManualQueueFragment(queueName: String) {
        closeAllFragments()
        addFragment(ManualQueueFragment.newInstance(queueName))
    }

    fun openMyPodcastsFragment() {
        removeLeaf()
        if (!fragmentStack.empty())
            if (fragmentStack.peek().isMyPodcastsFragment()) return

        addFragment(MyPodcastsFragment.newInstance())
    }

    fun openTestFragment() {
        removeLeaf()
        if (!fragmentStack.empty())
            if (fragmentStack.peek().isTestFragment()) return

        addFragment(TestFragment.newInstance())
    }

    fun closeAllFragments() {
        while (!fragmentStack.empty()) {
            closeTopFragment()
        }
    }

    fun closePodcastDetailsFragment() {
        if (fragmentStack.empty()) return
        if (fragmentStack.peek().isPodcastDetailsFragment()) {
            closeTopFragment()
        }
    }

    //**************************
    //* local functions
    //**************************

    private fun getNextPlaceholderId(): Int {
        return when(fragmentStack.size + 1) {
            1 -> activity.placeholder_01.id
            2 -> activity.placeholder_02.id
            3 -> activity.placeholder_03.id
            4 -> activity.placeholder_04.id
            5 -> activity.placeholder_05.id
            else -> -1
        }
    }

    private fun addFragment(frag: BaseFragment) {
        if (frag.isRoot()) { //clear stack if frag is root
            while (!fragmentStack.empty()) {
                var fragToDelete = fragmentStack.pop()
                activity.supportFragmentManager.beginTransaction().remove(fragToDelete).commit();
            }
        }
        fragmentStack.add(frag)
            activity.supportFragmentManager.beginTransaction()
                .add(getNextPlaceholderId(), frag)
                .commit()
    }

    private fun removeLeaf() {
        if (fragmentStack.empty()) return
        if (fragmentStack.peek().getType().equals(GC.fragmentType.leaf)) {
            var frag = fragmentStack.pop()
            activity.supportFragmentManager.beginTransaction()
                .remove(frag)
                .commit();
        }
    }

    //*****************************************
    //* Eventbus subscribers
    //*****************************************
    @Subscribe
    fun onEvent(event: EventOpenSubscribeFragment) {
        removeLeaf()
        if (!fragmentStack.empty())
            if (fragmentStack.peek().isSubscribeFragment()) return

        addFragment(SubscribeFragment.newInstance(event.podcastInfo))
    }

    @Subscribe
    fun onEvent(event: EventOpenPodcastDetailsFragment) {
        removeLeaf()
        if (!fragmentStack.empty())
            if (fragmentStack.peek().isPodcastDetailsFragment()) return

        addFragment(PodcastDetailsFragment.newInstance(event.podcastId))
    }

    @Subscribe
    fun onEvent(event: EventOpenTagFragment) {
        removeLeaf()
        if (!fragmentStack.empty())
            if (fragmentStack.peek().isTagFragment()) return

        addFragment(TagFragment.newInstance())
    }


    @Subscribe
    fun onEvent(event: EventOpenTagDetailFragment) {
        removeLeaf()
        if (!fragmentStack.empty())
            if (fragmentStack.peek().isTagDetailFragment()) return
        //EventBus.getDefault().post(EventToast("opening tag details: " + event.tag.toString()))
        if (!event.tag.equals("")) {
            addFragment(TagDetailFragment.newInstance(event.tag))
        }
    }

    @Subscribe
    fun onEvent(event: EventOpenTagSelectionFragment) {
        removeLeaf()
        if (!fragmentStack.empty())
            if (fragmentStack.peek().isTagSelectionFragment()) return

        addFragment(TagSelectionFragment.newInstance(event.tag))
    }

} //end of FragMgr