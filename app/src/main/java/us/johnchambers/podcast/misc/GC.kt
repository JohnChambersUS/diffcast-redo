package us.johnchambers.podcast.misc

//*****************************
//** Global Constants object **
//*****************************
object GC {

    object theme {
        object forest {
            object dark {
                val name = "forest_style_dark"
            }
            object light {
                val name = "forest_style_light"
            }
        }
        var dbKey = "THEME"
        var dark = "Dark"
        var light = "Light"
        var currentDarkLight = ""
    }

    object fragmentType {
        val root = "root"
        val branch = "branch"
        val leaf = "leaf"
    }

    object tapGuard {
        val shortTime = 250
        val longTime = 1000
    }

    object player {
        var REWIND = "Rewind"
        var STOP = "Stop"
        var PLAY = "Play"
        var PAUSE = "Pause"
        var END = "End"
        var FORWARD = "Forward"
        var PLAY_PAUSE = "Play Pause"
    }

    object playbackModes {
        var PODCAST = "Podcast"
        var FORWARD_BOOK = "Forward_Book"
        var REVERSE_BOOK = "Reverse_Book"
        var BOOK = "book_mode"
    }

    object prefix {
        var PID = "PID"
        var EID = "EID"
        var SEPARATOR = "_"
    }

    object update {
        object service {
            var running = false
        }
    }

    object tags {
        val WHATS_NEW_TEXT = "What's New in Podcasts?"
        val tagStrings = listOf(WHATS_NEW_TEXT)
    }

    object playlist {
        object manual {
            val prefix = "Manual"
            val MANUAL_QUEUE_1 = "Manual Queue 1"
            val MANUAL_QUEUE_2 = "Manual Queue 2"
            val MANUAL_QUEUE_3 = "Manual Queue 3"
            val MANUAL_QUEUE_4 = "Manual Queue 4"
            val MANUAL_QUEUE_5 = "Manual Queue 5"
            val MANUAL_QUEUE_6 = "Manual Queue 6"
            val MANUAL_QUEUE_7 = "Manual Queue 7"
            val MANUAL_QUEUE_8 = "Manual Queue 8"
            val MANUAL_QUEUE_9 = "Manual Queue 9"
            val MANUAL_QUEUE_10 = "Manual Queue 10"
        }
    }

    object feedClass {
        val LOYAL = "loyal books 24-1 at top"
        val LIBRI = "libri books 22-1 at top"
        var BOOK = "book has been redated"
        val PODCAST = "normal podcast order, 22-3 at top"
        val ODD = "odd ball book, 24-3 at top"
    }

}