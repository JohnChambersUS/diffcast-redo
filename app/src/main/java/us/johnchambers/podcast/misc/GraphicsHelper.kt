package us.johnchambers.podcast.misc

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.text.Layout
import android.text.StaticLayout
import android.text.TextPaint

class GraphicsHelper {


    fun isBadImageLink(link: String) : Boolean {
        if (link.equals("")) return true
        if (link.equals("https://is4-ssl.mzstatic.com/image/thumb/Features/v4/5f/88/96/5f8896e9-9be5-529a-af14-cc807367568a/mza_1382744875394257421.png/60x60bb.jpg")) return true
        return false
    }

    fun getSmallPodcastImage(name: String) : Bitmap {
        var b = Bitmap.createBitmap(100, 100, Bitmap.Config.ARGB_8888)
        b = colorBitmap(b, name)
        var letter = getSingleLetter(name)
        var paint = Paint(TextPaint.ANTI_ALIAS_FLAG)
        paint.setColor(Color.parseColor("#FFFAFA"))
        paint.setTextSize(60f)
        var c = Canvas(b)
        c.drawText(letter.toUpperCase(), 10f, 55f, paint)

        return b
    }

    fun getBigPodcastImage(name: String) : Bitmap {
        var b = Bitmap.createBitmap(400, 400, Bitmap.Config.ARGB_8888)
        b = colorBitmap(b, name)
        var shortName = reformatLongName(name)
        // canvas object with bitmap image as constructor
        var canvas = Canvas(b)
        var tp = TextPaint();
        tp.setColor(Color.WHITE);
        tp.setTextSize(55.0f);
        tp.setTextAlign(Paint.Align.CENTER);
        tp.setAntiAlias(true);
        var sl = StaticLayout("" + shortName, tp,
            canvas.getWidth(),
            Layout.Alignment.ALIGN_NORMAL,
            1.0f,
            0.0f,
            true);
        canvas.translate(200.0f, 0.0f);
        sl.draw(canvas);
        return b;
    }

    private fun colorBitmap(b: Bitmap, name: String) : Bitmap {
        var c = Canvas(b)
        var paint = Paint()
        var backgroundColor = getBackgroundColor(name)
        paint.setColor(backgroundColor)
        paint.setStyle(Paint.Style.FILL)
        c.drawPaint(paint)

        return b
    }

    private fun reformatLongName(name: String) : String {

        var parts = name.split(" ")
        if (parts.size < 8) {
            return name
        } else {
            var returnString = ""
            for (i in 0..6) {
                returnString = returnString + parts.get(i) + " "
            }
            returnString = returnString.trimEnd(' ')
            returnString = returnString.removeSuffix(" the")
            returnString = returnString.removeSuffix(" THE")
            returnString = returnString.removeSuffix(" The")
            returnString = returnString.trimEnd(',')

            returnString = returnString.trimEnd(' ')
            returnString = returnString.removeSuffix(" by")
            returnString = returnString.removeSuffix(" BY")
            returnString = returnString.removeSuffix(" By")
            returnString = returnString.trimEnd(',')

            returnString = returnString.trimEnd(' ')
            returnString = returnString.removeSuffix(" and")
            returnString = returnString.removeSuffix(" AND")
            returnString = returnString.removeSuffix(" And")
            returnString = returnString.trimEnd(',')

            returnString = returnString.removeSuffix(" a")
            returnString = returnString.removeSuffix(" A")
            returnString = returnString.trimEnd(',')

            returnString = returnString.removeSuffix(" -")
            returnString = returnString.removeSuffix(" :")
            returnString = returnString.removeSuffix(":")
            returnString = returnString.removeSuffix(";")
            returnString = returnString.trimEnd(',')
            return returnString
        }


    }

    private fun getSingleLetter(name: String) : String {
        if (name.length < 1) return "*"
        var badWords = listOf("a", "the")
        var words = name.split(" ")
        if (words.get(0) in badWords) {
            if (words.size < 1)
                return "#"
            else
                return words.get(1).first().toString()

        } else {
            return words.get(0).first().toString()
        }
    }

    private fun getBackgroundColor(name: String) : Int {

        var hash = name.hashCode().toString()
        if (name.length > 5) {
            var end = "#" + hash.takeLast(6)
            return Color.parseColor(end)
        } else {
            var num = hash.takeLast(1).toInt()
            when(num) {
                0 -> return Color.parseColor("#009933")
                1 -> return Color.parseColor("#006699")
                2 -> return Color.parseColor("#3333ff")
                3 -> return Color.parseColor("#cc00cc")
                4 -> return Color.parseColor("#cc0000")
                5 -> return Color.parseColor("#cc9900")
                6 -> return Color.parseColor("#999966")
                7 -> return Color.parseColor("#660033")
                8 -> return Color.parseColor("#66ccff")
                9 -> return Color.parseColor("#ff99cc")
            }
            return Color.parseColor("#99ff66")
        }
    }

}