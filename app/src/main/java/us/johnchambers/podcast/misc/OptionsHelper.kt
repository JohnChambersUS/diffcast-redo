package us.johnchambers.podcast.misc

import us.johnchambers.podcast.database.DBHelper
import us.johnchambers.podcast.database.table_definitions.OptionsGlobalRow

class OptionsHelper {

    var WIFI_ONLY = "WIFI_ONLY"

    fun getPlaybackSpeed(pid: String): Float {
        var speed = DBHelper.getSpeedForPodcastAsString(pid)
        if ((speed == null) || (speed.equals(""))) return 1.0f
        try {
            return speed.toFloat()
        } catch (e: Exception) {
            return 1.0f
        }
    }

    fun getRewindMinutes(eid: String): Int {
        return 1
    }

    fun getRewindMilliSeconds(eid: String): Int {
        return getRewindMinutes(eid) * 60 * 1000
    }

    fun getFastForwardMinutes(eid: String): Int {
        return 10
    }

    fun getFastForwardMilliSeconds(eid: String): Int {
        return getFastForwardMinutes(eid) * 60 * 1000
    }

    fun getSpeedForPodcastAsString(pid: String): String {
        var speed = DBHelper.getSpeedForPodcastAsString(pid)
        if (speed.equals("global")) {
            speed = "1.0"
        }
        return speed
    }

    fun setSpeedForPodcast(pid: String, speed: String) {
        DBHelper.setSpeedForPodcastFromString(pid, speed)
    }

    //********************************************
    //* global options setup
    //********************************************

    fun getPlaybackLimit(): Int {
        var limit = DBHelper.getGlobalOptionPlaybackLimit()
        try {
            return limit.toInt()
        } catch (e:java.lang.Exception) {
            return 10
        }
    }

    fun getPlaybackLimitAsString(): String {
        return DBHelper.getGlobalOptionPlaybackLimit()
    }

    fun setPlaybackLimit(limit: String) {
        var row = OptionsGlobalRow()
        row.k = "playback_limit"
        row.v = limit
        DBHelper.upsertGlobalOptionsRow(row)
    }

    fun setWifiOnly(only: Boolean) {
        var row = OptionsGlobalRow()
        row.k = WIFI_ONLY
        if (only == true) {
            row.v = "true"
        } else {
            row.v = "false"
        }
        DBHelper.upsertGlobalOptionsRow(row)
    }

    fun getWifiOnly() : Boolean {
        var row = DBHelper.getGlobalOptionsRow(WIFI_ONLY)
        if (row == null) return false
        if (row.k == "") return false
        return (row.v.equals("true"))
    }

}