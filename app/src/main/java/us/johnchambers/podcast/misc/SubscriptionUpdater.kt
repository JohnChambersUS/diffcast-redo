package us.johnchambers.podcast.misc

import org.greenrobot.eventbus.EventBus
import us.johnchambers.podcast.database.DBHelper
import us.johnchambers.podcast.event.EventToast
import us.johnchambers.podcast.objects.PodcastFeedWrapper
import java.lang.Exception

class SubscriptionUpdater {

    lateinit var podcast: PodcastFeedWrapper
    lateinit var mode: String
    var atTop = false
    var newEpisodes = HashMap<String, Boolean>()

    fun add(podcastInfo: PodcastFeedWrapper, mode: String) : String {
        this.podcast = podcastInfo
        this.mode = mode
        if (DBHelper.podcastExists(podcast.pid)) {
        }
        var rc = addHeader(mode)
        if (rc.length > 0) return rc
        addRows()
        return "Added: " + podcast.getTitle()
    }

    fun add(podcastInfo: PodcastFeedWrapper, atTop: Boolean) : String {
        this.podcast = podcastInfo
        this.mode = GC.playbackModes.BOOK
        this.atTop = atTop
        podcast.setBookSort(atTop)
        if (DBHelper.podcastExists(podcast.pid)) {
        }
        var rc = addHeader(mode)
        if (rc.length > 0) return rc
        addRows()
        return "Added: " + podcast.getTitle()
    }

    fun update(podcastInfo: PodcastFeedWrapper) {
        this.podcast = podcastInfo
        var headerUpdated = updateHeader()
        if (!headerUpdated) return
        try {
            addRows()
            removeDeadEpisodes()
        } catch (e: Exception) {
            EventBus.getDefault().post(EventToast(podcast.getTitle() +
                    " Subscription Updater Failure: " + e.message))
        }
    }

    private fun updateHeader(): Boolean{
        try {
            var pRow = this.podcast.getPodcastRow()
            var dbRow = DBHelper.getPodcastRowByPid(pRow.pid)
            if (!dbRow.isEmpty()) {
                pRow.mode = dbRow.mode
                pRow.category = dbRow.category
                pRow.setAsUpdatable(dbRow.isUpdatable())
            }
            var rv = DBHelper.updatePodcastRow(pRow)
        } catch (e: Exception){
            return false
        }
        return true
    }

    private fun addHeader(mode: String) : String {
        try {
            var pr = podcast.getPodcastRow()
            pr.mode = mode
            if (!DBHelper.insertPodcastRow(pr)) {
                return "Failed to add podcast to database"
            } else {
                return ""
            }
        } catch(e: Exception) {
            return e.message.toString()
        }
    }

    private fun addRows() {

        podcast.iterateOverEpisodes()
        while (podcast.moreEpisodes) {
            var newRow = podcast.getCurrentEpisodeRow()
            var dbRow = DBHelper.getEpisodeRowByEpisodeId(newRow.eid)
            if (!dbRow.isEmpty()) {
                newRow.play_point = dbRow.play_point
                newRow.length = dbRow.length
                newRow.skip = dbRow.skip
            }
            var rv = DBHelper.insertEpisodeRow(newRow)
            newEpisodes.set(newRow.eid, true)
            podcast.next()
        }
    }

    private fun removeDeadEpisodes() {
        var oldEpisodes = DBHelper.getEpisodeRowsByPid(podcast.pid)
        for (oe in oldEpisodes) {
            if (!newEpisodes.containsKey(oe.eid)) {
                DBHelper.deleteEpisodeByRow(oe)
            }
        }
    }

}