package us.johnchambers.podcast.misc

import java.util.*

class TapGuard(val timeLimit: Int) {

    var lastTapTime = Date()

    public fun tooSoon() : Boolean {
        var diff = Date().time - lastTapTime.time
        if (diff < timeLimit) {
            return true
        }
        lastTapTime = Date()
        return false;
    }
}