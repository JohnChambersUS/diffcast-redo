package us.johnchambers.podcast.objects

import com.rometools.rome.feed.synd.SyndEntry
import com.rometools.rome.feed.synd.SyndFeed
import java.io.StringReader
import com.rometools.rome.io.SyndFeedInput
import com.rometools.rome.io.impl.XmlFixerReader
import us.johnchambers.podcast.database.table_definitions.EpisodeRow
import us.johnchambers.podcast.database.table_definitions.PodcastRow
import us.johnchambers.podcast.fragment.subscribe.SubscribeEpisodesInfo
import us.johnchambers.podcast.fragment.subscribe.SubscribeHeaderInfo
import us.johnchambers.podcast.misc.GC
import java.security.MessageDigest
import java.text.SimpleDateFormat
import java.util.*

class PodcastFeedWrapper(var feedXml: String = "",
                         var podcastInfo: PodcastInfo = PodcastInfo(),
                         val passedPodcastRow: PodcastRow = PodcastRow()
) {

    var error = false
    var errorMsg: String? = null
    lateinit var feed: SyndFeed
    var moreEpisodes = false
    var currRow = 0
    var pid = ""
    var updatable = true

    init {
        loadFeedData()
    }

    fun iterateOverEpisodes() {
        currRow = feed.entries.size - 1
        moreEpisodes = (feed.entries.size > 0)
    }

    fun next() {
        currRow--
        moreEpisodes = !(currRow < 0)         //(currRow < feed.entries.size)
    }

    private fun loadFeedData() {
        val stringReader = StringReader(feedXml)
        val input = SyndFeedInput()

        try {
            feed = input.build(XmlFixerReader(stringReader))
        } catch (e: Exception) {
            error = true
            errorMsg = e.message
        }
        //todo wrap in try catch, notify if error, handle error
        try {
            if ((feed == null) && (error == false)) {
                error = true
                errorMsg = "Bad data file, unable to load podcast."
            }
        } catch (e: Exception) {
            error = true
            errorMsg = e.message
        }
    }

    fun getHeaderInfoList(): List<SubscribeHeaderInfo> {
        var list = mutableListOf<SubscribeHeaderInfo>()
        var headerInfo = SubscribeHeaderInfo()
        try {
            headerInfo.title = getTitle()
            headerInfo.desc = getDesc()
            headerInfo.logoUrl = getImageUrl()
            list.add(headerInfo)
        }
        catch (e: java.lang.Exception) {
            println(e.message)
        }

        list.add(headerInfo)
        return list
    }

    //used by subscribe
    fun getEpisodeInfo(): List<SubscribeEpisodesInfo> {
        var list = mutableListOf<SubscribeEpisodesInfo>()
        feed.entries.forEach() {
            var name = getEpisodeName(it)
            var date = getEpisodeDate(it)
            var url = getEpisodeUrl(it)
            var imageUrl = getImageUrl()

            list.add(SubscribeEpisodesInfo(name, date, url, imageUrl))
        }
        return list
    }


    //*****************************************
    //* add getters to safety check values
    //* because can't be sure what is in JSON
    //*****************************************

    fun getTitle() : String {
        var returnValue: String
        try {
            returnValue = feed.title
            if ((returnValue == null) || (returnValue == ""))
                returnValue = removeXmlTags(if (podcastInfo.isEmpty()) passedPodcastRow.name else  podcastInfo.name)
        } catch (e: Exception) {
            try {
                returnValue = removeXmlTags(if (podcastInfo.isEmpty()) passedPodcastRow.name else  podcastInfo.name)
            } catch(e: Exception) {
                returnValue = "Unknown Podcast Name"
            }
        }
        return returnValue
    }

    fun getDesc() : String {
        var returnValue: String
        try {
            returnValue = feed.description
            if (returnValue == null)
                returnValue = removeXmlTags(if (podcastInfo.isEmpty()) passedPodcastRow.desc else podcastInfo.desc)
        } catch (e: Exception) {
            returnValue = removeXmlTags(if (podcastInfo.isEmpty()) passedPodcastRow.desc else podcastInfo.desc)
        }
        return removeXmlTags(returnValue)
    }

    fun getImageUrl() : String {
        var returnValue: String
        try {
            returnValue = feed.image.url
            if (returnValue == null)
                returnValue = if (podcastInfo.isEmpty()) passedPodcastRow.logo_url else podcastInfo.artworkUrl
        } catch (e: Exception) {
            returnValue = if (podcastInfo.isEmpty()) passedPodcastRow.logo_url else podcastInfo.artworkUrl
        }
        return removeXmlTags(returnValue)
    }

    fun getFeedUrl() : String {
        var returnValue: String
        try {
            returnValue = if (podcastInfo.isEmpty()) passedPodcastRow.feed_url else podcastInfo.feedUrl
            if (returnValue == null) returnValue = ""
        } catch (e: Exception) {
            returnValue = ""
        }
        return removeXmlTags(returnValue)
    }

    fun getPodcastId() : String {
        if (pid == "") {
            var url = getFeedUrl()
            if ((url == null) || (url.equals(""))) url = System.currentTimeMillis().toString()
            pid = GC.prefix.PID + GC.prefix.SEPARATOR + hashString(url)
        }
        return pid
    }

    //*************************************
    //* safety getters for episode items
    //*************************************

    private fun getEpisodeName(item: SyndEntry) : String {
        var name = ""
        try {
            name = item.title.trim()
            if (name == null) name = ""
        } catch (e: Exception) {
            name = ""
        }
        return removeXmlTags(name)
    }

    private fun getEpisodeDate(item: SyndEntry) : Date {
        var rv: Date
        try {
            rv = item.publishedDate
            if (rv == null) rv = Date()
        } catch (e: Exception) {
            rv = Date()
        }
        return rv
    }

    private fun getEpisodeDateAsString(item: SyndEntry) : String {
        var d = getEpisodeDate(item)
        var sdf = SimpleDateFormat("yyyy-DDD HH:mm:ss:SS");
        return sdf.format(d)
    }

    private fun getEpisodeUrl(item: SyndEntry) : String {
        var rv = ""
        try {
            rv = item.enclosures.first().url
            if (rv == null) rv = ""
        } catch (e: Exception) {
            rv = ""
        }
        return removeXmlTags(rv)
    }

    private fun getEpisodeTitle(item: SyndEntry) : String {
        return item.title
    }

    private fun getEpisodeDescription(item: SyndEntry) : String {
        try {
            return item.description.toString()
        } catch (e: java.lang.Exception) {
            return ""
        }
    }

    //*******************************************************
    //* public wrappers for getting row info
    //*******************************************************

    //*** get row ****
    fun getCurrentEpisodeRow() : EpisodeRow {
        var row = EpisodeRow()
        try { //unable to get row, so return empty row
            var currEpisode = feed.entries.get(currRow)
            row.audio_url = getEpisodeUrl(currEpisode)
            row.publication_date = getEpisodeDateAsString(currEpisode)
            row.eid = GC.prefix.EID + GC.prefix.SEPARATOR + hashString(row.audio_url + row.publication_date)
            row.pid = getPodcastId()
            row.title = getEpisodeTitle(currEpisode)
            row.summary = getEpisodeDescription(currEpisode)
            return row
        } catch (e: java.lang.Exception) {
            return EpisodeRow()
        }
    }

    fun getPodcastRow() : PodcastRow {
        var pr = PodcastRow()
        pr.feed_url = getFeedUrl()
        if (pr.feed_url == "") {
            throw Exception("Subscription attempt failed, unable to get feed url")
        }
        pr.pid = getPodcastId()
        pr.name = getTitle()
        pr.sort_name = getSortName()
        pr.desc = getDesc()
        pr.mode = ""
        pr.category = ""
        pr.stored_logo_path = ""
        pr.logo_url = getImageUrl()
        pr.setAsUpdatable(updatable)
        return pr
    }

    private fun getSortName() : String {
        var wordList = getTitle().split(" ") as MutableList<String>
        if (wordList.size == 0) return "No Name Found"
        var firstWord = wordList.get(0).toLowerCase()
        if (firstWord.equals("the") ) {
            wordList[0] = ""
        }
        var rw =  wordList.joinToString(separator = " ")
        return rw.trim()
    }

    fun setBookSort(atTop: Boolean) {
        if (feed.entries.size < 1) return
        var feedClass = getFeedClass(atTop)

        if (feedClass.equals(GC.feedClass.BOOK)) {
            feed.entries.reverse()
            reDate()
            updatable = false
        }
        if (feedClass.equals(GC.feedClass.PODCAST)) {
            //DO NOTHING
            updatable = true
        }
    }

    private fun reDate() {

        var d = Date()
        var workingTime = d.time
        var increment = (1 * 48 * 60 * 60 * 1000)
        for (i in 0..(feed.entries.size - 1)) {
            workingTime = workingTime - increment
            var workingDate = Date(workingTime)
            feed.entries[i].publishedDate = workingDate
        }
    }

    private fun getFeedClass(atTop: Boolean = false) : String {
        if (atTop) {
            //is loyal or libre
            return GC.feedClass.BOOK
        } else { //at bottom, is podcast or odd
            return GC.feedClass.PODCAST
        }
    }

    //**************************************
    //* utility functions
    //**************************************

    private fun removeXmlTags(value: String): String {
        val r0 = "^\n(\r)?".toRegex()
        var rv = r0.replace(value, "")

        val r00 = "\n(\r)?$".toRegex()
        rv = r00.replace(rv, "")

        val r1 = "<(\\/)?[Pp]>".toRegex()
        rv = r1.replace(rv, "\n\r")

        val r2 = "<[^>]+>".toRegex()
        rv = r2.replace(rv, " ")

        val r3 = "&#[0-9]*;".toRegex()
        rv = r3.replace(rv, " ")

        val r4 = "  ".toRegex()
        rv = r4.replace(rv, " ")

        val r5 = "(\\s*)?\n\r".toRegex()
        rv = r5.replace(rv, "\n\r")

        val r6 = "^(\\s*)?\n\r".toRegex()
        rv = r6.replace(rv, "")

        val r7 = "\n\r([\n\r]*)?".toRegex()
        rv = r7.replace(rv, "\n\r\n\r")


        return rv
    }

    private fun hashString(input: String): String {
        val HEX_CHARS = "0123456789ABCDEF"
        val bytes = MessageDigest
            .getInstance("MD5")
            .digest(input.toByteArray())
        val result = StringBuilder(bytes.size * 2)

        bytes.forEach {
            val i = it.toInt()
            result.append(HEX_CHARS[i shr 4 and 0x0f])
            result.append(HEX_CHARS[i and 0x0f])
        }

        return result.toString()
    }

}

