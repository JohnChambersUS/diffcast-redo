package us.johnchambers.podcast.objects

//*****************************************
//** This contains basic info from search
//*****************************************
class PodcastInfo() {
    var name: String = ""
    var desc: String = ""
    var feedUrl: String = ""
    var artworkUrl: String = ""
    var artworkUrl600: String = ""
    var episodeCount: Int = 0

    fun isEmpty() : Boolean {
        return name.equals("")
    }
}