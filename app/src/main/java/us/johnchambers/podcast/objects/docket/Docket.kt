package us.johnchambers.podcast.objects.docket

import java.io.Serializable

abstract class Docket : Serializable{

    var TypeOfPodcast = false
    var TypeOfTag = false
    var TypeOfManual = false
    var TypeOfSample = false
    var TypeOfEmpty = false
    var TypeOfNowPlaying = false
    var TypeOfPlaylist = false

    var playlistId = ""
    var nextEpisode = ""

}