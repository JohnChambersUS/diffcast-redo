package us.johnchambers.podcast.objects.docket

class DocketNowPlaying : Docket() {
    init {
        TypeOfNowPlaying = true
    }
}