package us.johnchambers.podcast.objects.docket

class DocketPlaylist(var playlistName: String) : Docket() {

    var nextEpisodeEid = ""
    var nextEpisodeIndex = -1
    var playAll = false
    var useEid = false

    constructor(playlistName: String, episodeToPlay: String) : this(playlistName) {
        nextEpisodeEid = episodeToPlay
        useEid = true
    }

    constructor(playlistName: String, playFromBeginning: Boolean) : this(playlistName) {
        playAll = true
    }

    init{
        TypeOfPlaylist = true
    }

}