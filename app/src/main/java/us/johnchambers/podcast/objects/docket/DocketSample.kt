package us.johnchambers.podcast.objects.docket

import us.johnchambers.podcast.fragment.subscribe.SubscribeEpisodesInfo

class DocketSample(val info: SubscribeEpisodesInfo, val podcastName: String) : Docket() {

    init {
        TypeOfSample = true
    }
}