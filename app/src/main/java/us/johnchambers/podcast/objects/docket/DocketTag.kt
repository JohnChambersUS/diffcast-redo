package us.johnchambers.podcast.objects.docket

import us.johnchambers.podcast.database.table_definitions.EpisodeRow

class DocketTag(var episodes: MutableList<EpisodeRow>, var episodeToPlay: Int, var tag: String) : Docket() {

    init{
        TypeOfTag = true
    }

}