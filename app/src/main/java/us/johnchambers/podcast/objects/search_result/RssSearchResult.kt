package us.johnchambers.podcast.objects.search_result

import com.rometools.rome.feed.synd.SyndFeed
import com.rometools.rome.io.SyndFeedInput
import com.rometools.rome.io.impl.XmlFixerReader
import us.johnchambers.podcast.objects.PodcastInfo
import java.io.StringReader

class RssSearchResult(val response: String, val link: String) {

    lateinit var feed: SyndFeed
    var error = false
    var errorMsg: String? = null

    init {
        loadFeedData()
    }

    private fun loadFeedData() {
        val stringReader = StringReader(response)
        val input = SyndFeedInput()

        try {
            feed = input.build(XmlFixerReader(stringReader))
        } catch (e: Exception) {
            error = true
            errorMsg = e.message
        }
        //todo wrap in try catch, notify if error, handle error
        try {
            if ((feed == null) && (error == false)) {
                error = true
                errorMsg = "Bad data file, unable to load podcast."
            }
        } catch (e: Exception) {
            error = true
            errorMsg = e.message
        }
    }

    fun errorLoading() : Boolean {
        return error
    }

    public fun getPodcastInfo() : PodcastInfo {
        var podcastInfo = PodcastInfo()

        try {podcastInfo.name = feed.title} catch(e:Exception) {podcastInfo.name = "Bad Title"}
        try {podcastInfo.desc = feed.description} catch(e:Exception) {podcastInfo.desc = "Bad Description"}
        try {podcastInfo.feedUrl = link} catch(e:Exception) {podcastInfo.feedUrl = ""}
        try {podcastInfo.artworkUrl = feed.image.url} catch (e: Exception) {podcastInfo.artworkUrl = ""}
        try {podcastInfo.artworkUrl600 = feed.image.url} catch (e: Exception) {podcastInfo.artworkUrl600 = ""}
        try {podcastInfo.episodeCount = feed.entries.size} catch(e:Exception) {podcastInfo.episodeCount = 0}

        return podcastInfo
    }

}