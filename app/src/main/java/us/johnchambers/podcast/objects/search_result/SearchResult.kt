package us.johnchambers.podcast.objects.search_result

import org.json.JSONArray
import org.json.JSONObject
import us.johnchambers.podcast.misc.hasData
import us.johnchambers.podcast.objects.PodcastInfo

abstract class SearchResult(val responseString: String) {

    private var response = JSONObject()
    private var resultCount = 0
    private var results = JSONArray()
    private var parseFailed = false

    init {
        try {
            response = JSONObject(responseString)
            resultCount = response.getInt("resultCount")
            results = response.getJSONArray("results");
        }
        catch (e:Exception) {parseFailed = true}
    }

    fun getResultsList() : List<PodcastInfo> {
        var list = mutableListOf<PodcastInfo>()
        var artworkPriority = listOf<String>("artworkUrl100", "artworkUrl60", "artworkUrl30", "artworkUrl600")
        var i = -1;

        while (++i < results.length()) {
            try { //put try inside while, so if one fails, they don't all fail
                var pi = PodcastInfo()
                var cr = results.get(i) as JSONObject

                if (cr.has("collectionName")) pi.name = cr.getString("collectionName") else continue
                if (cr.hasData("feedUrl")) pi.feedUrl = cr.getString("feedUrl") else continue
                if (cr.has("trackCensoredName")) pi.desc = cr.getString("trackCensoredName")
                if (cr.has("trackCount")) pi.episodeCount = cr.getInt("trackCount")
                //set artwork url
                if (cr.hasData(cr.getString(artworkPriority.get(0)))) pi.artworkUrl =
                        cr.getString(artworkPriority.get(0))
                else if (cr.hasData(artworkPriority.get(1))) pi.artworkUrl = cr.getString(artworkPriority.get(1))
                else if (cr.hasData(artworkPriority.get(2))) pi.artworkUrl = cr.getString(artworkPriority.get(2))
                else if (cr.hasData(artworkPriority.get(3))) pi.artworkUrl = cr.getString(artworkPriority.get(3))
                //get 600 artwork as backup for subscribe if artwork on details fails
                if (cr.hasData("artworkUrl600")) pi.artworkUrl600 = cr.getString("artworkUrl600")

                list.add(pi)
            } catch (e: Exception) {
            }
        }
        return list;
    }

}