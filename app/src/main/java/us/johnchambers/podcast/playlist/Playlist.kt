package us.johnchambers.podcast.playlist

import us.johnchambers.podcast.database.DBHelper
import java.io.Serializable

abstract class Playlist : Serializable{

    var TypeOfPodcast = false
    var TypeOfForwardBook = false
    var TypeOfReverseBook = false
    var TypeOfTag = false
    var TypeOfManual = false
    var TypeOfSample = false
    var TypeOfEmpty = false
    var TypeOfPlaylist = false


    abstract open fun next() : Boolean

    abstract open fun more() : Boolean

    abstract fun getCurrEpisodeUrl() : String

    abstract open fun getCurrEpisodeEid(): String

    abstract open fun getLogoUrl(): String

    abstract open fun getPodcastName(): String

    abstract open fun updateEpisodeTime(playPoint: Long, duration: Long, eid: String)

    abstract open fun getPlayPointAsLong(): Long

    abstract open fun getSpeed(): Float

    abstract open fun getEpisodeName(): String

    abstract open fun setNowPlaying()


    open fun clearNowPlaying() {
        DBHelper.clearNowPlaying()
    }



}