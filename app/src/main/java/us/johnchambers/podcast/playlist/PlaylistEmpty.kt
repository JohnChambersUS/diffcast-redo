package us.johnchambers.podcast.playlist

import java.io.Serializable

class PlaylistEmpty : Playlist(), Serializable {

    init {
        this.TypeOfEmpty = true
    }

    override fun getCurrEpisodeUrl(): String {
        return ""
    }

    override fun getCurrEpisodeEid(): String {
        return ""
    }

    override fun getLogoUrl(): String {
        return ""
    }

    override fun getPodcastName(): String {
        return ""
    }

    override fun next() : Boolean {
        return false
    }

    override fun more(): Boolean {
        return false;
    }

    override fun updateEpisodeTime(playPoint: Long, duration: Long, eid: String) {
    }

    override fun getPlayPointAsLong(): Long {
        return (0).toLong()
    }

    override fun getSpeed(): Float {
        return 1.0f
    }

    override fun getEpisodeName(): String {
        return ""
    }

    override fun setNowPlaying() {
    }
}