package us.johnchambers.podcast.playlist

import us.johnchambers.podcast.database.DBHelper
import us.johnchambers.podcast.misc.GC
import us.johnchambers.podcast.objects.docket.*

object PlaylistFactory {

    fun getPlaylist(docket: Docket) : Playlist {

        var workingDocket = docket

        if (docket.TypeOfNowPlaying) { //create a TypeOfPodcast docket for later
            workingDocket = processNowPlayingDocket()
        }

        if (workingDocket.TypeOfPodcast) {
            var playlist : Playlist
            var d = workingDocket as DocketPodcast
            var podcastRow = DBHelper.getPodcastRowByPid(d.pid)
            when(podcastRow.mode) {
                GC.playbackModes.PODCAST -> playlist = PlaylistPodcast(d.pid, d.nextEpisode)
                GC.playbackModes.FORWARD_BOOK -> playlist = PlaylistForwardBook(d.pid, d.nextEpisode)
                GC.playbackModes.BOOK -> playlist = PlaylistForwardBook(d.pid, d.nextEpisode)
                GC.playbackModes.REVERSE_BOOK -> playlist = PlaylistReverseBook(d.pid, d.nextEpisode)
                else -> playlist = PlaylistPodcast(d.pid, d.nextEpisode)
            }
            return playlist
        }

        if (workingDocket.TypeOfSample) {
            var d = workingDocket as DocketSample
            var info = d.info
            var playlist = PlaylistSample(info.episodeUrl, info.logoUrl, d.podcastName)
            return playlist
        }

        if (workingDocket.TypeOfTag) {
            var d = workingDocket as DocketTag
            var playlist = PlaylistTag(d.episodes, d.episodeToPlay, d.tag)
            return playlist
        }

        if (workingDocket.TypeOfPlaylist) {
            var d = workingDocket as DocketPlaylist
            var playlistName = d.playlistName
            var playlist: Playlist
            if (d.useEid) {
                playlist = PlaylistPlaylist(playlistName, d.nextEpisodeEid)
            } else {
                playlist = PlaylistPlaylist(playlistName, true)
            }
            return playlist
        }
        return PlaylistEmpty()
    }

    private fun processNowPlayingDocket() : Docket {
        var row = DBHelper.getNowPlayingRow()
        if (row.isEmpty()) {
            return DocketEmpty()
        }
        var playlistType = row.name.split(GC.prefix.SEPARATOR).get(0)
        if (playlistType.equals(GC.prefix.PID)) {
            var returnDocket = DocketPodcast(row.name, row.eid)
            return returnDocket
        }
        return DocketEmpty()
    }
}