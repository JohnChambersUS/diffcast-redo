package us.johnchambers.podcast.playlist

import org.greenrobot.eventbus.EventBus
import us.johnchambers.podcast.database.DBHelper
import us.johnchambers.podcast.database.table_definitions.EpisodeRow
import us.johnchambers.podcast.database.table_definitions.PlaylistRow
import us.johnchambers.podcast.database.table_definitions.PodcastRow
import us.johnchambers.podcast.event.EventUpdateRowInDisplay
import us.johnchambers.podcast.misc.OptionsHelper
import java.io.Serializable
import java.lang.Exception

class PlaylistPlaylist(var playlistName: String) : Playlist(), Serializable {

    lateinit var episodes: MutableList<PlaylistRow>
    var currentlyPlayingEpisode = 0
    var firstIteration = true
    var nextEpisodeEid = ""

    constructor(playlistName: String, nextEpisodeId: String) : this(playlistName) {
        nextEpisodeEid = nextEpisodeId
        loadEpisodes()
        setOpeningEpisode()
    }

    constructor(playlistName: String, playAll: Boolean) : this(playlistName) {
        loadEpisodes()
        findInitialPlayableEpisode()
    }

    init {
        TypeOfPlaylist = true
    }

    override fun next() : Boolean{
        if (firstIteration) {
            firstIteration = false
        } else {
            findNextEpisode()
        }
        return more()
    }

    override fun more(): Boolean {
        return !(currentlyPlayingEpisode < 0)
    }

    override fun getCurrEpisodeUrl(): String {
        return getEpisodeInfo(currentlyPlayingEpisode).audio_url
    }

    override fun getCurrEpisodeEid(): String {
        return episodes.get(currentlyPlayingEpisode).eid
    }

    override fun getLogoUrl(): String {
        return getPodcastInfo(currentlyPlayingEpisode).logo_url
    }

    override fun getPodcastName(): String {
        return getPodcastInfo(currentlyPlayingEpisode).name
    }

    override fun updateEpisodeTime(playPoint: Long, duration: Long, eid: String) {
        var eidToUse = episodes.get(currentlyPlayingEpisode).eid
        var episodeIndex = currentlyPlayingEpisode
        var durationValidated = duration
        var playPointValidated = playPoint
        if (duration < 0) {
            durationValidated = 0
            playPointValidated = -1
        }
        if (!episodes.get(currentlyPlayingEpisode).eid.equals(eid)) {
            for (i in 0..(episodes.size -1)) {
                if (episodes.get(i).eid.equals(eid)) {
                    episodeIndex = i
                    eidToUse = episodes.get(i).eid
                    break
                }
            }
        }
        DBHelper.updateTimerOnEpisodeRow(playPointValidated, durationValidated, eidToUse)
        EventBus.getDefault().post(EventUpdateRowInDisplay(episodeIndex))
    }

    override fun getPlayPointAsLong(): Long {
        try {
            return getEpisodeInfo(currentlyPlayingEpisode).getPlayPointAsLong()
        } catch (e: Exception) {
            return -1
        }
    }

    override fun getSpeed(): Float {
        var options = OptionsHelper()
        var pc = getPodcastInfo(currentlyPlayingEpisode)
        return options.getPlaybackSpeed(pc.pid)
    }

    override fun getEpisodeName(): String {
        return getEpisodeInfo(currentlyPlayingEpisode).title
    }

    override fun setNowPlaying() {
        var episode = getEpisodeInfo(currentlyPlayingEpisode)
        DBHelper.setNowPlaying(episode.pid, episode.eid)
    }

    private fun loadEpisodes() {
        try {
            episodes = DBHelper.getPlaylistByName(playlistName)
        } catch(e: Exception) {
            episodes = mutableListOf(PlaylistRow())
        }
    }

    //*** find next episode while playlist is playing
    private fun findNextEpisode() {
        --currentlyPlayingEpisode
        var foundIt = false
        while ((!foundIt) && (currentlyPlayingEpisode > -1)) {
            var episodeInfo = getEpisodeInfo(currentlyPlayingEpisode)
            if (episodeInfo.isSkipped()) {
                currentlyPlayingEpisode--
                continue
            }
            if (episodeInfo.getPlayPointAsLong() < episodeInfo.getLengthAsLong()) {
                foundIt = true
            } else {
                currentlyPlayingEpisode--
            }
        }
    }

    private fun findInitialPlayableEpisode() {
        currentlyPlayingEpisode = episodes.size - 1
        var foundIt = false
        while ((!foundIt) && (currentlyPlayingEpisode > -1)) {
            var episodeInfo = getEpisodeInfo(currentlyPlayingEpisode)
            if (episodeInfo.isSkipped()) {
                currentlyPlayingEpisode--
                continue
            }
            if (episodeInfo.getPlayPointAsLong() < episodeInfo.getLengthAsLong()) {
                foundIt = true
            } else {
                currentlyPlayingEpisode--
            }

        }
    }

    private fun getEpisodeInfo(eid: String) : EpisodeRow {
        return DBHelper.getEpisodeRowByEpisodeId(eid)
    }

    private fun getEpisodeInfo(pos: Int) : EpisodeRow {
        return getEpisodeInfo(episodes.get(pos).eid)
    }

    private fun getPodcastInfo(eid: String) : PodcastRow {
        var episode = getEpisodeInfo(eid)
        return DBHelper.getPodcastRowByPid(episode.pid)
    }

    private fun getPodcastInfo(pos: Int) : PodcastRow {
        var episode = getEpisodeInfo(pos)
        return DBHelper.getPodcastRowByPid(episode.pid)
    }

     //******************************************************
    //* start section to determine opening podcast to play
    //******************************************************
    private fun setOpeningEpisode() {
        if (nextEpisodeEid.equals("")) {
            findOpeningEpisodeWhenNonePassed()
        } else {
            findOpeningEpisodeWhenValuePassed()
        }
    }

    private fun findOpeningEpisodeWhenValuePassed() {
        var pos = 0
        var foundIt = false
        while ((pos < episodes.size) && (!foundIt)) {
            var episodeInfo = getEpisodeInfo(pos)
            if (nextEpisodeEid.equals(episodeInfo.eid)) {
                foundIt = true
                currentlyPlayingEpisode = pos
                episodeInfo.unskipEpisode()
                if (episodeInfo.isComplete()) episodeInfo.setToBeginning()
                DBHelper.insertEpisodeRow(episodeInfo)
            }
            pos++
        }
        if (!foundIt) {
            findOpeningEpisodeWhenNonePassed()
        }
    }

    private fun findOpeningEpisodeWhenNonePassed() {
        if (episodes.size == 0) {
            currentlyPlayingEpisode = -1
            return
        }
        //** find first touched episode **
        var pos = findFirstTouchedEpisodePosition()

        if (pos >= episodes.size) { //ran through all episodes without finding a touched episode
            //go through list from top and find a playable one
            currentlyPlayingEpisode = findFirstPlayableEpisodePosition()
        } else { //found a touched episode
            //is found touched episode in progress, then play it
            if (getEpisodeInfo(pos).isInProgressEligable()) { currentlyPlayingEpisode = pos }
            else {
                //else touched episode is complete, so look for next episode that is eligable
                currentlyPlayingEpisode = findNextPlayableEpisodeAfterPosition(pos)
            }
        }
    }

    private fun findFirstTouchedEpisodePosition() : Int {
        var pos = -1
        while(++pos < episodes.size) {
            if (getEpisodeInfo(pos).touchedPlayable()) break
        }
        return pos
    }

    private fun findFirstPlayableEpisodePosition() : Int {
        var pos = -1
        while(++pos < episodes.size) {
            if (getEpisodeInfo(pos).isPlayable()) break
        }
        return pos
    }

    private fun findNextPlayableEpisodeAfterPosition(startPos: Int) : Int {
        var pos = startPos
        while (--pos > -1) {
            if (getEpisodeInfo(pos).isPlayable()) break
        }
        return pos
    }


}