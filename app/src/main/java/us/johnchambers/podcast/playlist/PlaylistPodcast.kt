package us.johnchambers.podcast.playlist

import org.greenrobot.eventbus.EventBus
import us.johnchambers.podcast.database.DBHelper
import us.johnchambers.podcast.database.table_definitions.EpisodeRow
import us.johnchambers.podcast.database.table_definitions.PodcastRow
import us.johnchambers.podcast.event.EventUpdateRowInDisplay
import us.johnchambers.podcast.misc.OptionsHelper
import java.io.Serializable
import java.lang.Exception

class PlaylistPodcast(var pid: String, var nextEpisodeId: String) : Playlist(), Serializable {

    lateinit var episodes: MutableList<EpisodeRow>
    lateinit var podcastInfo : PodcastRow
    var currentlyPlayingEpisode = 0
    var firstIteration = true

    init {
        TypeOfPodcast = true
        loadEpisodes()
        setOpeningEpisode()
    }

    override fun next() : Boolean{
        if (firstIteration) {
            firstIteration = false
        } else {
            findNextEpisode()
        }
        return more()
    }

    override fun more(): Boolean {
        return !(currentlyPlayingEpisode < 0)
    }

    override fun getCurrEpisodeUrl(): String {
        return episodes.get(currentlyPlayingEpisode).audio_url
    }

    override fun getCurrEpisodeEid(): String {
        return episodes.get(currentlyPlayingEpisode).eid
    }

    override fun getLogoUrl(): String {
        return podcastInfo.logo_url
    }

    override fun getPodcastName(): String {
        return podcastInfo.name
    }

    override fun updateEpisodeTime(playPoint: Long, duration: Long, eid: String) {
        var eidToUse = episodes.get(currentlyPlayingEpisode).eid
        var episodeIndex = currentlyPlayingEpisode
        var durationValidated = duration
        var playPointValidated = playPoint
        if (duration < 0) {
            durationValidated = 0
            playPointValidated = -1
        }
        if (!episodes.get(currentlyPlayingEpisode).eid.equals(eid)) {
            for (i in 0..(episodes.size -1)) {
                if (episodes.get(i).eid.equals(eid)) {
                    episodeIndex = i
                    eidToUse = episodes.get(i).eid
                    break
                }
            }
        }
        DBHelper.updateTimerOnEpisodeRow(playPointValidated, durationValidated, eidToUse)
        EventBus.getDefault().post(EventUpdateRowInDisplay(episodeIndex))
    }

    override fun getPlayPointAsLong(): Long {
        try {
            return episodes.get(currentlyPlayingEpisode).getPlayPointAsLong()
        } catch (e: Exception) {
            return -1
        }
    }

    override fun getSpeed(): Float {
        var options = OptionsHelper()
        return options.getPlaybackSpeed(pid)
    }

    override fun getEpisodeName(): String {
        return episodes.get(currentlyPlayingEpisode).title
    }

    override fun setNowPlaying() {
        DBHelper.setNowPlaying(podcastInfo.pid, episodes.get(currentlyPlayingEpisode).eid)
    }

    private fun loadEpisodes() {
        try {
            podcastInfo = DBHelper.getPodcastRowByPid(pid)
            episodes = DBHelper.getEpisodeRowsByPid(pid)
        } catch(e: Exception) {
            podcastInfo = PodcastRow()
            episodes = mutableListOf(EpisodeRow())
        }
    }

    //*** find next episode while playlist is playing
    private fun findNextEpisode() {
        --currentlyPlayingEpisode
        var foundIt = false
        while ((!foundIt) && (currentlyPlayingEpisode > -1)) {
            refreshEpisodeInfo(currentlyPlayingEpisode)
            if (episodes.get(currentlyPlayingEpisode).isSkipped()) {
               currentlyPlayingEpisode--
                continue
            }
            if (episodes.get(currentlyPlayingEpisode).getPlayPointAsLong() < episodes.get(currentlyPlayingEpisode).getLengthAsLong()) {
                foundIt = true
            } else {
                currentlyPlayingEpisode--
            }
        }
    }

    private fun refreshEpisodeInfo(pos: Int) {
        episodes.set(pos, DBHelper.getEpisodeRowByEpisodeId(episodes.get(pos).eid))
    }

    //******************************************************
    //* start section to determine opening podcast to play
    //******************************************************
    private fun setOpeningEpisode() {
        if (nextEpisodeId.equals("")) {
            findOpeningEpisodeWhenNonePassed()
        } else {
            findOpeningEpisodeWhenValuePassed()
        }
    }

    private fun findOpeningEpisodeWhenValuePassed() {
        var pos = 0
        var foundIt = false
        while ((pos < episodes.size) && (!foundIt)) {
            if (nextEpisodeId.equals(episodes.get(pos).eid)) {
                foundIt = true
                currentlyPlayingEpisode = pos
                episodes.get(pos).unskipEpisode()
                if (episodes.get(pos).isComplete()) episodes.get(pos).setToBeginning()
                DBHelper.insertEpisodeRow(episodes.get(pos))
            }
            pos++
        }
        if (!foundIt) {
            findOpeningEpisodeWhenNonePassed()
        }
    }

    private fun findOpeningEpisodeWhenNonePassed() {
        if (episodes.size == 0) {
            currentlyPlayingEpisode = -1
            return
        }
        //** find first touched episode **
        var pos = findFirstTouchedEpisodePosition()

        if (pos >= episodes.size) { //ran through all episodes without finding a touched episode
            //go through list from top and find a playable one
            currentlyPlayingEpisode = findFirstPlayableEpisodePosition()
        } else { //found a touched episode
            //is found touched episode in progress, then play it
            if (episodes.get(pos).isInProgressEligable()) { currentlyPlayingEpisode = pos }
            else {
                //else touched episode is complete, so look for next episode that is eligable
                currentlyPlayingEpisode = findNextPlayableEpisodeAfterPosition(pos)
            }
        }
    }

    private fun findFirstTouchedEpisodePosition() : Int {
        var pos = -1
        while(++pos < episodes.size) {
            if (episodes.get(pos).touchedPlayable()) break
        }
        return pos
    }

    private fun findFirstPlayableEpisodePosition() : Int {
        var pos = -1
        while(++pos < episodes.size) {
            if (episodes.get(pos).isPlayable()) break
        }
        return pos
    }

    private fun findNextPlayableEpisodeAfterPosition(startPos: Int) : Int {
        var pos = startPos
        while (--pos > -1) {
            if (episodes.get(pos).isPlayable()) break
        }
        return pos
    }


}