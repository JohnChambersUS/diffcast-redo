package us.johnchambers.podcast.playlist

import org.greenrobot.eventbus.EventBus
import us.johnchambers.podcast.database.DBHelper
import us.johnchambers.podcast.database.table_definitions.EpisodeRow
import us.johnchambers.podcast.database.table_definitions.PodcastRow
import us.johnchambers.podcast.event.EventUpdateRowInDisplay
import us.johnchambers.podcast.misc.OptionsHelper
import java.lang.Exception

class PlaylistReverseBook(var pid: String, var nextEpisodeId: String) : Playlist() {

    lateinit var episodes: MutableList<EpisodeRow>
    lateinit var podcastInfo : PodcastRow
    var currentlyPlayingEpisode = 0
    private var firstIteration = true

    init {
        TypeOfReverseBook = true
        loadEpisodes()
        setOpeningEpisode()
    }

    override fun getCurrEpisodeUrl(): String {
        return episodes.get(currentlyPlayingEpisode).audio_url
    }

    override fun getCurrEpisodeEid(): String {
        return episodes.get(currentlyPlayingEpisode).eid
    }

    override fun getLogoUrl(): String {
        return podcastInfo.logo_url
    }

    override fun getPodcastName(): String {
        return podcastInfo.name
    }

    override fun more(): Boolean {
        return (currentlyPlayingEpisode < episodes.size)
    }

    override fun next(): Boolean {
        if (firstIteration) {
            firstIteration = false
        } else {
            //currentlyPlayingEpisode++
            findNextEpisode()
        }
        return more()
    }

    override fun updateEpisodeTime(playPoint: Long, duration: Long, eid: String) {
        var eidToUse = episodes.get(currentlyPlayingEpisode).eid
        var episodeIndex = currentlyPlayingEpisode
        var durationValidated = duration
        var playPointValidated = playPoint
        if (duration < 0) {
            durationValidated = 0
            playPointValidated = -1
        }
        //get row with eid
        // if current row is eid use it
        // else find row with eid
        if (!episodes.get(currentlyPlayingEpisode).eid.equals(eid)) {
            for (i in 0..(episodes.size -1)) {
                if (episodes.get(i).eid.equals(eid)) {
                    episodeIndex = i
                    eidToUse = episodes.get(i).eid
                    break
                }
            }
        }
        DBHelper.updateTimerOnEpisodeRow(playPointValidated, durationValidated, eidToUse)
        EventBus.getDefault().post(EventUpdateRowInDisplay(episodeIndex))
    }

    override fun getPlayPointAsLong(): Long {
        try {
            return episodes.get(currentlyPlayingEpisode).getPlayPointAsLong()
        } catch (e: Exception) {
            return -1
        }
    }

    override fun getSpeed(): Float {
        var options = OptionsHelper()
        return options.getPlaybackSpeed(pid)
    }

    override fun getEpisodeName(): String {
        return episodes.get(currentlyPlayingEpisode).title
    }

    override fun setNowPlaying() {
        DBHelper.setNowPlaying(podcastInfo.pid, episodes.get(currentlyPlayingEpisode).eid)
    }

    private fun loadEpisodes() {
        try {
            podcastInfo = DBHelper.getPodcastRowByPid(pid)
            episodes = DBHelper.getEpisodeRowsByPid(pid)
        } catch(e: Exception) {
            podcastInfo = PodcastRow()
            episodes = mutableListOf()
        }
    }

    private fun findNextEpisodeX() {
        ++currentlyPlayingEpisode
        var foundIt = false
        while ((!foundIt) && (currentlyPlayingEpisode < episodes.size)) {
            refreshEpisodeInfo(currentlyPlayingEpisode)
            if (episodes.get(currentlyPlayingEpisode).isSkipped()) {
                currentlyPlayingEpisode++
                continue
            }
            if (episodes.get(currentlyPlayingEpisode).isPlayable()) {
                foundIt = true
            } else {
                currentlyPlayingEpisode++
            }
        }
    }

    private fun findNextEpisode() {
        while (++currentlyPlayingEpisode < episodes.size) {
            refreshEpisodeInfo(currentlyPlayingEpisode)
            if (episodes.get(currentlyPlayingEpisode).isPlayable()) break
        }
    }

    private fun refreshEpisodeInfo(pos: Int) {
        episodes.set(pos, DBHelper.getEpisodeRowByEpisodeId(episodes.get(pos).eid))
    }

    //*************************
    //* playlist setup
    //*************************
    private fun setOpeningEpisode() {
        if (nextEpisodeId.equals("")) {
            findOpeningEpisodeWhenNonePassed()
        } else {
            findOpeningEpisodeWhenValuePassed()
        }
    }

    private fun findOpeningEpisodeWhenValuePassed() {
        var pos = 0
        var foundIt = false
        while ((pos < episodes.size) && (!foundIt)) {
            if (nextEpisodeId.equals(episodes.get(pos).eid)) {
                foundIt = true
                currentlyPlayingEpisode = pos
                episodes.get(pos).unskipEpisode()
                if (episodes.get(pos).isComplete()) episodes.get(pos).setToBeginning()
                DBHelper.insertEpisodeRow(episodes.get(pos))
            }
            pos++
        }
        if (!foundIt) {
            findOpeningEpisodeWhenNonePassed()
        }
    }

    private fun findOpeningEpisodeWhenNonePassed() {
        //** empty playlist **
        if (episodes.size == 0) {
            currentlyPlayingEpisode = episodes.size + 1
            return
        }
        //** find first touched episode **
        var pos = findFirstTouchedEpisodePosition()
        if (pos < 0) { //ran through all episodes without finding a touched episode
            //go through list from top and find a playable one
            currentlyPlayingEpisode = findFirstPlayableEpisodePosition()
        } else { //found a touched episode
            //is found touched episode in progress, then play it
            if (episodes.get(pos).isInProgressEligable()) { currentlyPlayingEpisode = pos }
            else {
                //else touched episode is complete, so look for next episode that is eligable
                currentlyPlayingEpisode = findNextPlayableEpisodeAfterPosition(pos)
            }
        }
    }

    private fun findNextPlayableEpisodeAfterPosition(startPos: Int) : Int {
        var pos = startPos
        while (++pos < episodes.size) {
            if (episodes.get(pos).isPlayable()) break
        }
        return pos
    }

    private fun findFirstPlayableEpisodePosition() : Int {
        var pos = -1
        while(++pos < episodes.size) {
            if (episodes.get(pos).isPlayable()) break
        }
        return pos
    }

    private fun findFirstTouchedEpisodePosition() : Int {
        var pos = episodes.size
        var foundIt = false
        while(--pos > -1) {
            if (episodes.get(pos).touchedPlayable()) break
        }
        return pos
    }
    //***************************
    //***************************
    //go backwards looking for a complete or partially played episode
    private fun findLastPlayedEpisode() : Int {
        var pos = episodes.size - 1
        var foundIt = false
        while ((pos > -1) && (!foundIt)) {
            if (episodes.get(pos).isSkipped()) {
                pos--
                continue
            }
            if (episodes.get(pos).getPlayPointAsLong() >= episodes.get(pos).getLengthAsLong()) {
                foundIt = true
            }
            if (episodes.get(pos).getPlayPointAsLong() > 0) {
                foundIt = true
            }
        }
        return pos
    }




}