package us.johnchambers.podcast.playlist

class PlaylistSample(val playbackUrl: String, val passedLogoUrl: String, val passedPodcastName: String) : Playlist() {

    private var firstEpisode = true

    override fun getCurrEpisodeUrl(): String {
        return playbackUrl
    }

    override fun getLogoUrl(): String {
        return passedLogoUrl
    }

    override fun getCurrEpisodeEid(): String {
        return ""
    }

    override fun getPodcastName(): String {
        return passedPodcastName
    }

    override fun getPlayPointAsLong(): Long {
        return (0).toLong()
    }

    override fun more(): Boolean {
        if (firstEpisode) {
            firstEpisode = false
            return true
        } else {
            return false
        }
    }

    override fun next(): Boolean {
        return more()
    }

    override fun updateEpisodeTime(playPoint: Long, duration: Long, eid: String) {
        //do nothing as nothing in db, jus playing temporary file
    }

    override fun getSpeed(): Float {
        return 1.0f
    }

    override fun getEpisodeName(): String {
        return ""
    }

    override fun setNowPlaying() {

    }

}