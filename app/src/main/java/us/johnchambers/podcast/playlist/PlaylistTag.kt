package us.johnchambers.podcast.playlist


import org.greenrobot.eventbus.EventBus
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import us.johnchambers.podcast.database.DBHelper
import us.johnchambers.podcast.database.table_definitions.EpisodeRow
import us.johnchambers.podcast.database.table_definitions.PodcastRow
import us.johnchambers.podcast.event.EventUpdateRowInDisplay
import us.johnchambers.podcast.misc.OptionsHelper
import java.io.Serializable
import java.lang.Exception

class PlaylistTag(var episodes: MutableList<EpisodeRow>, var nextEpisodeId: Int, var tagName: String) : Playlist(), Serializable {

    var currentlyPlayingEpisode = 0
    var firstIteration = true

    init {
        TypeOfTag = true
        currentlyPlayingEpisode = nextEpisodeId
        if (episodes.get(currentlyPlayingEpisode).isPlayable()) {
            //do nothing, current is set and will show in more
        } else {
            findNextEpisode() //sets new current
        }
    }

    override fun next() : Boolean{
        if (firstIteration) {
            firstIteration = false
        } else {
            findNextEpisode()
        }
        return more()
    }

    override fun more(): Boolean {
        return !(currentlyPlayingEpisode < 0)
    }

    override fun getCurrEpisodeUrl(): String {
        return episodes.get(currentlyPlayingEpisode).audio_url
    }

    override fun getCurrEpisodeEid(): String {
        return episodes.get(currentlyPlayingEpisode).eid
    }

    override fun getLogoUrl(): String {
        var pid = episodes.get(currentlyPlayingEpisode).pid
        var podcast = DBHelper.getPodcastRowByPid(pid)
        return podcast.logo_url
    }

    override fun getPodcastName(): String {
        var pid = episodes.get(currentlyPlayingEpisode).pid
        var podcast = DBHelper.getPodcastRowByPid(pid)
        return podcast.name
    }

    override fun updateEpisodeTime(playPoint: Long, duration: Long, eid: String) {
        var eidToUse = episodes.get(currentlyPlayingEpisode).eid
        var episodeIndex = currentlyPlayingEpisode
        var durationValidated = duration
        var playPointValidated = playPoint
        if (duration < 0) {
            durationValidated = 0
            playPointValidated = -1
        }
        //get row with eid
        // if current row is eid use it
        // else find row with eid
        if (!episodes.get(currentlyPlayingEpisode).eid.equals(eid)) {
            for (i in 0..(episodes.size -1)) {
                if (episodes.get(i).eid.equals(eid)) {
                    episodeIndex = i
                    eidToUse = episodes.get(i).eid
                    break
                }
            }
        }
        DBHelper.updateTimerOnEpisodeRow(playPointValidated, durationValidated, eidToUse)
        EventBus.getDefault().post(EventUpdateRowInDisplay(episodeIndex))
    }

    override fun getPlayPointAsLong(): Long {
        try {
            return episodes.get(currentlyPlayingEpisode).getPlayPointAsLong()
        } catch (e: Exception) {
            return -1
        }
    }

    override fun getSpeed(): Float {
        var options = OptionsHelper()
        return options.getPlaybackSpeed(episodes.get(currentlyPlayingEpisode).pid)
    }

    override fun getEpisodeName(): String {
        return episodes.get(currentlyPlayingEpisode).title
    }

    override fun setNowPlaying() {
        DBHelper.setNowPlaying(tagName, episodes.get(currentlyPlayingEpisode).eid)
    }

    //*** find next episode while playlist is playing
    private fun findNextEpisode() {
        --currentlyPlayingEpisode
        var foundIt = false
        while ((!foundIt) && (currentlyPlayingEpisode > -1)) {
            refreshEpisodeInfo(currentlyPlayingEpisode)
            if (episodes.get(currentlyPlayingEpisode).isSkipped()) {
                currentlyPlayingEpisode--
                continue
            }
            if (episodes.get(currentlyPlayingEpisode).getPlayPointAsLong() < episodes.get(currentlyPlayingEpisode).getLengthAsLong()) {
                foundIt = true
            } else {
                currentlyPlayingEpisode--
            }
        }
    }

    private fun refreshEpisodeInfo(pos: Int) {
        episodes.set(pos, DBHelper.getEpisodeRowByEpisodeId(episodes.get(pos).eid))
    }

}