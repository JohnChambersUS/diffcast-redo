package us.johnchambers.podcast.updater

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import org.greenrobot.eventbus.EventBus
import us.johnchambers.podcast.event.EventToast
import kotlin.concurrent.thread

class PodcastUpdateBroadcastReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        //EventBus.getDefault().post(EventToast("receiver entered"))
        //Log.d("~~~~~", "Broadcast Reciever entered, starting updater")
        ///val theIntent = Intent(context.applicationContext,
            ///us.johnchambers.podcast.updater.PodcastUpdateService::class.java)
        ///context.startService(theIntent)
        //Log.d("~~~~~", "Broadcast Reciever ran, started updater")

        Thread {
            val theIntent = Intent(context.applicationContext,
                us.johnchambers.podcast.updater.PodcastUpdateService::class.java)
            context.startService(theIntent)
        } .start()
    }
}