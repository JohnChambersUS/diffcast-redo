package us.johnchambers.podcast.updater

import android.app.*
import android.content.Intent
import android.util.Log
import org.greenrobot.eventbus.EventBus
import us.johnchambers.podcast.database.DBHelper
import us.johnchambers.podcast.event.EventToast
import us.johnchambers.podcast.misc.SubscriptionUpdater
import us.johnchambers.podcast.objects.PodcastFeedWrapper
import java.net.URL

class PodcastUpdateService : IntentService("PodcastUpdateService") {

    var _intent : Intent? = null
    var _notificationId = 2133427
    var _notificationChannelId = "us.johnchambers.player.updater"

    override fun onHandleIntent(intent: Intent?) {
        _intent = intent
        if (intent != null) {
            try {
                //Log.d("~~~~~", "Update Starting")
                //EventBus.getDefault().post(EventToast("receiver entered"))
                updatePodcasts()
                //Log.d("~~~~~", "Update Complete")
            } catch (e: Exception) {
                EventBus.getDefault().post(EventToast("Updater Failed: " + e.message))
            }
        }
    }

    override fun onDestroy() {
        //Log.d("~~~~~","Updater ended")
        super.onDestroy()
    }

    private fun updatePodcasts() {

        //return

        var podcasts = DBHelper.getPodcasts()
        for (podcast in podcasts) {
            try {
              if (podcast.isUpdatable()) {
                    var updater = SubscriptionUpdater()
                    var aapcname = podcast.name
                    var result = URL(podcast.feed_url).readText()
                    var aathesize = result.length
                    var feed = PodcastFeedWrapper(feedXml = result, passedPodcastRow = podcast)
                    updater.update(feed)
                    //Log.d("~~~~~", "Updating: " + podcast.name)
                } else {
                    //Log.d("~~~~~", podcast.name + " not updateable")
                }
            } catch (e: Exception) {
                //Log.d("~~~~~", "could not update a podcast not updateable")
            }
        }
    }

    private fun updateable(feed: String) : Boolean {
        if (feed.contains("loyalbooks", true)) return false
        if (feed.contains("librivox", true)) return false
        return true
    }

}